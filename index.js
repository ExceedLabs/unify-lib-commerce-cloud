const CustomerModel = require('./models/customer-model');
const ProductModel = require('./models/product-model');
const CategoryModel = require('./models/category-model');
const CatalogModel = require('./models/catalog-model');
const OrderModel = require('./models/order-model');
const BasketModel = require('./models/basket-model');
const WishlistModel = require('./models/wishlist-model');
const WishlistItemModel = require('./models/wishlist-item-model');
const BotModel = require('./models/bot-model');
const AuthModel = require('./models/auth-model');
const StoreModel = require('./models/store-model');

// CUSTOMERS
exports.listCustomer = CustomerModel.listCustomer;
exports.listAllCustomers = CustomerModel.listAllCustomers;
exports.createCustomer = CustomerModel.createCustomer;
exports.updateCustomer = CustomerModel.updateCustomer;
exports.createCustomerAddresses = CustomerModel.createCustomerAddress;
exports.createCustomerPaymentInstruments = CustomerModel.createCustomerPaymentInstruments;
exports.createCustomerWishlists = CustomerModel.createCustomerWishlists;
exports.createCustomerWishlistItems = CustomerModel.createCustomerWishlistItems;
exports.createCustomerPurchases = CustomerModel.createCustomerPurchases;
exports.listCustomerAddresses = CustomerModel.listCustomerAddresses;
exports.listCustomerAddressesById = CustomerModel.listCustomerAddressesById;
exports.listCustomerBaskets = CustomerModel.listCustomerBaskets;
exports.listCustomerOrders = CustomerModel.listCustomerOrders;
exports.listCustomerPaymentInstruments = CustomerModel.listCustomerPaymentInstruments;
exports.listCustomerPurchases = CustomerModel.listCustomerPurchases;
exports.listCustomerWishlists = CustomerModel.listCustomerWishlists;
exports.listCustomerWishlistsById = CustomerModel.listCustomerWishlistsById;
exports.listCustomerWishlistItems = CustomerModel.listCustomerWishlistItems;
exports.listCustomerAbandonedCart = CustomerModel.listCustomerAbandonedCart;
exports.deleteCustomerAddressById = CustomerModel.deleteCustomerAddressById;
exports.deleteCustomerWishlistsById = CustomerModel.deleteCustomerWishlistsById;
exports.deleteCustomerPaymentInstruments = CustomerModel.deleteCustomerPaymentInstruments;
exports.deleteCustomerWishlistItemById = CustomerModel.deleteCustomerWishlistItemById;
exports.listCustomerWishlistItemsById = CustomerModel.listCustomerWishlistItemsById;
exports.updateCustomerWishlistById = CustomerModel.updateCustomerWishlistById;
exports.updateCustomerWishlistItemsById = CustomerModel.updateCustomerWishlistItemsById;
exports.updateCustomerAddressById = CustomerModel.updateCustomerAddressById;
exports.getAbandonedCarts = CustomerModel.getAbandonedCarts;

// PRODUCTS
exports.searchProduct = ProductModel.searchProduct;
exports.listProduct = ProductModel.listProduct;
exports.getProductInventory = ProductModel.getProductInventory;

// CATEGORIES
exports.listCategoryProducts = CategoryModel.listCategoryProducts;
exports.searchCategory = CategoryModel.searchCategory;
exports.listCategories = CategoryModel.listCategories;
exports.listCategoriesById = CategoryModel.listCategoriesById;

// CATALOG
exports.searchCatalog = CatalogModel.searchCatalog;
exports.searchCatalogProduct = CatalogModel.searchCatalogProduct;

// ORDER
exports.searchOrder = OrderModel.searchOrder;
exports.createOrder = OrderModel.createOrder;
exports.getOrderByNumber = OrderModel.getOrderByNumber;
exports.getPaymentMethod = OrderModel.getPaymentMethod;
exports.getPaymentInstrument = OrderModel.getPaymentInstrument;
exports.createPaymentInstrument = OrderModel.createPaymentInstrument;
exports.listProductByOrder = OrderModel.listProductByOrder;
exports.listBillingAddressByOrder = OrderModel.listBillingAddressByOrder;
exports.listShipmentAddressByOrder = OrderModel.listShipmentAddressByOrder;
exports.updateOrder = OrderModel.updateOrder;

// BASKET
exports.createBasket = BasketModel.createBasket;
exports.listBasket = BasketModel.listBasket;
exports.listBasketById = BasketModel.listBasketById;
exports.listBasketShipments = BasketModel.listBasketShipments;
exports.listBasketProductItems = BasketModel.listBasketProductItems;
exports.listBasketShippingItems = BasketModel.listBasketShippingItems;
exports.listBasketPaymentInstruments = BasketModel.listBasketPaymentInstruments;
exports.listBasketBillingAddress = BasketModel.listBasketBillingAddress;
exports.listBasketShippingMethods = BasketModel.listBasketShippingMethods;
exports.updateBasketBillingAddress = BasketModel.updateBasketBillingAddress;
exports.createBasketProductItem = BasketModel.createBasketProductItem;
exports.createBasketPaymentInstrument = BasketModel.createBasketPaymentInstrument;
exports.createBasketShipment = BasketModel.createBasketShipment;
exports.getBasketPaymentMethod = BasketModel.getBasketPaymentMethod;
exports.putBasketShipmentMethodToShipment = BasketModel.putBasketShipmentMethodToShipment;
exports.putBasketShipmentAddressToShipment = BasketModel.putBasketShipmentAddressToShipment;
exports.putBasketCustomer = BasketModel.putBasketCustomer;
exports.putBasketShipment = BasketModel.putBasketShipment;
exports.putBasketProductItem = BasketModel.putBasketProductItem;
exports.putBasketPaymentInstrumentById = BasketModel.putBasketPaymentInstrumentById;
exports.removeBasket = BasketModel.removeBasket;
exports.removeBasketShipment = BasketModel.removeBasketShipment;
exports.removeBasketItemById = BasketModel.removeBasketItemById;
exports.removeBasketPaymentInstrumentById = BasketModel.removeBasketPaymentInstrumentById;
exports.refreshBasketId = BasketModel.refreshBasketId;

// WISHLIST
exports.listWishlist = WishlistModel.listWishlist;
exports.listWishlistById = WishlistModel.listWishlistById;

// WISHLIST ITEMS
exports.listWishlistItem = WishlistItemModel.listWishlistItem;
exports.listWishlistItemById = WishlistItemModel.listWishlistItemById;

// BOT
exports.getOrderBotById = BotModel.getOrderBotById;

// AUTHENTICATION
exports.doLogin = AuthModel.doLogin;

//STORES
exports.getStores = StoreModel.getStores;

Object.byString = function(o, s) {
  s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
  s = s.replace(/^\./, '');           // strip a leading dot
  var a = s.split('.');
  for (var i = 0, n = a.length; i < n; ++i) {
      var k = a[i];
      if (k in o) {
          o = o[k];
      } else {
          return;
      }
  }
  return o;
}

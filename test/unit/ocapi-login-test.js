require('../load-env');
require('chai').should();

const OcapiLoginHelper = require('../../services/helpers/ocapi-helpers/ocapi-login-transactions-helper');

describe('## OCAPI LOGIN TRANSACTION ##', () => {
  it('#01 checking error to perform performDemandwareBmLogin()', (done) => {
    OcapiLoginHelper
      .performDemandwareBmLogin()
      .then((result) => {
        result.message.should.equal('required.configuration.not.found');
        done();
      })
      .catch(() => {
        done();
      });
  });

  it('#02 getting base64 performDemandwareBmLogin()', (done) => {
    const infoOrganization = {
      credentials: {
        userId: 'user-id',
        userPassword: 'user-password',
        clientId: 'client-id',
        clientPassword: 'client-password',
        baseUrl: 'base-url',
      },
    };
    OcapiLoginHelper
      .performDemandwareBmLogin(infoOrganization)
      .then((result) => {
        result.authorizationHeader.should.equal('dXNlci1pZDp1c2VyLXBhc3N3b3JkOmNsaWVudC1wYXNzd29yZA==');
        result.finalUrl.should.equal('base-url/dw/oauth2/access_token?client_id=client-id');
        result.demandwareBmLoginInfo.URL.should.equal('dw/oauth2/access_token?client_id');
        result.demandwareBmLoginInfo.METHOD.should.equal('POST');
        result.demandwareBmLoginInfo.GRANT_TYPE.should.equal('urn:demandware:params:oauth:grant');
        result.demandwareBmLoginInfo.CONTENT_TYPE.should.equal('application/x-www-form-urlencoded');
        done();
      })
      .catch(() => {
        done();
      });
  });

  it('#03 getting tokenBearer()', () => {
    const result = OcapiLoginHelper.getTokenBearer({
      token_type: 'Bearer',
      access_token: '<something_encoded_here>',
    });
    result.should.equal('Bearer <something_encoded_here>');
  });
});

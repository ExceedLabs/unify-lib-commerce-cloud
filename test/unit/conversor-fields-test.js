require('../load-env');
require('chai').should();

const ConversorFieldsService = require('../../services/customer/customer-converter');

describe('## CONVERSOR FIELDS SERVICE ##', () => {
  it('#01 replacing Customer', () => {
    const data = [{"_type":"customer","birthday":"1993-06-24","creation_date":"2018-11-30T18:31:22.000Z","credentials":{"_type":"credentials","enabled":true,"locked":false,"login":"xxx2@osf-final-test.com"},"customer_id":"cdcaw7sUKblwOoCdXhyBMllD2i","customer_no":"00025501","email":"xxx2@osf-final-test.com","first_name":"HI Updated2","gender":0,"job_title":"Analyst","last_modified":"2018-11-30T18:52:19.000Z","last_name":"HI Test Updated2","phone_business":"5585999999999","phone_home":"5585999999998","phone_mobile":"5585999999997","salutation":"Mr.","title":"HI Test FT updated2"}];

    const obj = {
      libObjId: 'customer',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            birthday: 'birthday',
            createdAt: 'createdAt',
            customerId: 'customerId',
            customerNumber: 'customerNumber',
            name: 'name',
            gender: 'gender',
            phoneBusiness: 'phoneBusiness',
            phoneHome: 'phoneHome',
            phoneMobile: 'phoneMobile'
          }
        }
      }
    };

    const result = ConversorFieldsService.getDataMapToCore(obj, data);
    result.map.list.should.equal('customer');
    result.map.item.createdAt.should.equal('creation_date');
    result.data.customer[0].creation_date.should.equal('2018-11-30T18:31:22.000Z');
  });

  it('#02 replacing Customer', () => {
    const data = [];

    const obj = {
      libObjId: 'customer',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            birthday: 'birthday',
            createdAt: 'createdAt',
            customerId: 'customerId',
            customerNumber: 'customerNumber',
            name: 'name',
            gender: 'gender',
            phoneBusiness: 'phoneBusiness',
            phoneHome: 'phoneHome',
            phoneMobile: 'phoneMobile'
          }
        }
      }
    };

    const result = ConversorFieldsService.getDataMapToCore(obj, data);
    result.map.list.should.equal('customer');
    result.data.customer.length.should.equal(0);
  });

  it('#03 getting error on try replacing Customer', () => {
    try {
      const data = [];
      const obj = { libObjId: 'customer' };
      const result = ConversorFieldsService.getDataMapToCore(obj, data);
      result.map.list.should.equal('customer');
    } catch (error) {
      error.message.should.equal('error.replace.customer.fields');
    }
  });
});

require('../../load-env');
require('chai').should();

const CustomerMapping = require('../../../services/customer/customer-mapping');
const CustomerConstant = require('../../../services/customer/customer-constant');
const FIELDS_CUSTOMER = CustomerConstant.fields;

describe('## CUSTOMER MAPPINGS ##', () => {
  const fieldsCore = {
    login: 'login',
    salutation: 'salutation',
    email: 'email',
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    customerId: 'customerId',
    customerNumber: 'customerNumber',
    gender: 'gender',
    birthday: 'birthday',
    firstName: 'firstName',
    lastName: 'lastName',
    phoneBusiness: 'phoneBusiness',
    phoneHome: 'phoneHome',
    phoneMobile: 'phoneMobile',
  };
  it('#01 getting mappings from getCoreFields()', () => {
    const result = CustomerMapping.getCoreFields(fieldsCore);
    result.gender.should.equal(FIELDS_CUSTOMER.gender);
    result.salutation.should.equal(FIELDS_CUSTOMER.salutation);
    result.email.should.equal(FIELDS_CUSTOMER.email);
    result.birthday.should.equal(FIELDS_CUSTOMER.birthday);
    result.createdAt.should.equal(FIELDS_CUSTOMER.creation_date);
    result.customerId.should.equal(FIELDS_CUSTOMER.customer_id);
    result.customerNumber.should.equal(FIELDS_CUSTOMER.customer_no);
    result.firstName.should.equal(FIELDS_CUSTOMER.first_name);
    result.lastName.should.equal(FIELDS_CUSTOMER.last_name);
    result.phoneBusiness.should.equal(FIELDS_CUSTOMER.phone_business);
    result.phoneHome.should.equal(FIELDS_CUSTOMER.phone_home);
    result.phoneMobile.should.equal(FIELDS_CUSTOMER.phone_mobile);
  });

  it('#02 getting mappings from getCustomerFields()', () => {
    const result = CustomerMapping.getCustomerFields(fieldsCore);
    result.gender.should.equal(fieldsCore.gender);
    result.salutation.should.equal(fieldsCore.salutation);
    result.email.should.equal(fieldsCore.email);
    result.birthday.should.equal(fieldsCore.birthday);
    result.creation_date.should.equal(fieldsCore.createdAt);
    result.customer_id.should.equal(fieldsCore.customerId);
    result.customer_no.should.equal(fieldsCore.customerNumber);
    result.first_name.should.equal(fieldsCore.firstName);
    result.last_name.should.equal(fieldsCore.lastName);
    result.phone_business.should.equal(fieldsCore.phoneBusiness);
    result.phone_home.should.equal(fieldsCore.phoneHome);
    result.phone_mobile.should.equal(fieldsCore.phoneMobile);
  });
});

const DataTransform = require('node-json-transform').transform;
require('../../load-env');
require('chai').should();
const { expect } = require('chai');

const CustomerConverter = require('../../../services/customer/customer-converter');

describe('## CUSTOMER CONVERTER ##', () => {

  const obj = {
    libObjId: 'customerBasicInfo',
    privateLibraries: {
      Converters: {
        fieldsCore: {
          salutation: 'salutation',
          email: 'email',
          birthday: 'birthday',
          createdAt: 'createdAt',
          customerId: 'customerId',
          customerNumber: 'customerNumber',
          firstName: 'firstName',
          lastName: 'lastName',
          gender: 'gender',
          phoneBusiness: 'phoneBusiness',
          phoneHome: 'phoneHome',
          phoneMobile: 'phoneMobile',
        },
        replaceFields: (data, baseMap) => (DataTransform(data, baseMap).transform()),
      },
    },
  };

  const data = [{
    salutation: 'Mrs.',
    email: 'email@domain.com',
    birthday: 'birthday',
    createdAt: '2018-01-10T23:00:00',
    firstName: 'Test',
    lastName: 'Test 2',
    customerId: 'kjhkjhhUIIYjhjhg7676',
    customerNumber: '76765',
    phoneMobile: '85989898989',
  }];

  it('#01 getting URL to POST Customer()', () => {
    const result = CustomerConverter.getDataMapToCore(obj, data);
    result.map.list.should.equal('customerBasicInfo');
    expect(result.data.customerBasicInfo).to.be.an('array');
  });
});

require('../../load-env');
require('chai').should();

const CustomerEndpoint = require('../../../services/customer/customer-endpoint');

describe('## CUSTOMER ENDPOINTs ##', () => {
  it('#01 getting URL to POST Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'POST', '123', {});
    result.should.equal('/shop/v18_8/customers');
  });

  it('#02 getting URL to GET Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET', '123');
    result.should.equal('/shop/v18_8/customers/123');
  });

  it('#03 getting URL to PATCH Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'PATCH', '123');
    result.should.equal('/shop/v18_8/customers/123');
  });

  it('#04 getting URL to POST_ADDRESS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'POST_ADDRESS', '123');
    result.should.equal('/shop/v18_8/customers/123/addresses');
  });

  it('#05 getting URL to GET_ADDRESS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_ADDRESS', '123');
    result.should.equal('/shop/v18_8/customers/123/addresses');
  });

  it('#06 getting URL to GET_BASKETS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_BASKETS', '123');
    result.should.equal('/shop/v18_8/customers/123/baskets');
  });

  it('#07 getting URL to GET_ORDERS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_ORDERS', '123');
    result.should.equal('/shop/v18_8/customers/123/orders');
  });

  it('#08 getting URL to GET_PAYMENT_INSTRUMENTS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_PAYMENT_INSTRUMENTS', '123');
    result.should.equal('/shop/v18_8/customers/123/payment_instruments');
  });

  it('#09 getting URL to POST_PAYMENT_INSTRUMENTS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'POST_PAYMENT_INSTRUMENTS', '123');
    result.should.equal('/shop/v18_8/customers/123/payment_instruments');
  });

  it('#10 getting URL to GET_PURCHASES Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_PURCHASES', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items/xpto/purchases');
  });

  it('#11 getting URL to GET_WISHLISTS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_WISHLISTS', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists?expand=product,images,availability');
  });

  it('#12 getting URL to GET_WISHLISTS_BY_ID Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_WISHLISTS_BY_ID', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc?expand=product,images,availability');
  });

  it('#13 getting URL to GET_WISHLIST_ITEMS Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_WISHLIST_ITEMS', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items?expand=product,images,availability');
  });

  it('#14 getting URL to POST_WISHLIST Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'POST_WISHLIST', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists');
  });

  it('#15 getting URL to POST_WISHLIST_ITEM Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'POST_WISHLIST_ITEM', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items');
  });

  it('#16 getting URL to DELETE_WISHLIST_ITEM_BY_ID Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'DELETE_WISHLIST_ITEM_BY_ID', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items/xpto');
  });

  it('#17 getting URL to DELETE_PAYMENT_INSTRUMENT Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'DELETE_PAYMENT_INSTRUMENT', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/payment_instruments/abc');
  });

  it('#18 getting URL to DELETE_WISHLIST_BY_ID Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'DELETE_WISHLIST_BY_ID', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc');
  });

  it('#19 getting URL to GET_WISHLIST_ITEMS_BY_ID Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'GET_WISHLIST_ITEMS_BY_ID', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items/xpto?expand=product,images,availability');
  });

  it('#20 getting URL to UPDATE_WISHLIST_BY_ID Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'UPDATE_WISHLIST_BY_ID', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc');
  });

  it('#21 getting URL to UPDATE_WISHLIST_ITEM_BY_ID Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'UPDATE_WISHLIST_ITEM_BY_ID', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items/xpto');
  });

  it('#22 getting URL to POST_WISHLIST_PURCHASE Customer()', () => {
    const result = CustomerEndpoint.retrieveEndpoint('v18_8', 'POST_WISHLIST_PURCHASE', '123', { id: 'abc', itemId: 'xpto' });
    result.should.equal('/shop/v18_8/customers/123/product_lists/abc/items/xpto/purchases');
  });

  it('#LAST getting error()', () => {
    try {
      const result = CustomerEndpoint.retrieveEndpoint();
      result.should.equal('method.not.allowed.error');
    } catch (error) {
      error.message.should.equal('method.not.allowed.error');
    }
  });
});

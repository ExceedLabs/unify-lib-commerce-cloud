var DataTransform = require("node-json-transform").DataTransform

var data = {
    items : [
        {
            "shippingMethod": {
                "id" : "BaseShippingMethod"
            },
            "id": "DefaultShipping",
            "shippingAddress": {
                "firstName": "Breno",
                "lastName": "Monteiro",
                "street" : "Vautrin Avenue",
                "city" : "Holtsville",
                "country" : "US",
                "phone" : "111111111111",
                "zipCode" : "00501",
                "state": "NY"
            }
        }
    ]
};

var map = {
    list: 'items',
    item: {
        "shippingMethod": {
            "id" : "shippingMethod.id"
        },
        "id": "id",
        "shippingAddress": {
            "firstName": "shippingAddress.firstName"
        }
    },
    defaults: {
        "missingData": true
    }
};

var result = DataTransform(data, map).transform();

console.log(result[0]);
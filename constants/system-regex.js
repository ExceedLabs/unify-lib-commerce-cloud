'use strict';

module.exports = {
    dateRegex: {
        engISO: {
            regexFormat: /(\d{4})-(\d{2})-(\d{2})/,
            isoStringFormat: 'YYYY-MM-DDTHH:mm:ss.sssZ'
        },
        engDot: {
            regexFormat: /(\d{4})\.(\d{2})\.(\d{2})/,
            isoStringFormat: 'YYYY.MM.DDTHH:mm:ss.sssZ'
        },
        eng: {
            regexFormat: /(\d{4})\/(\d{2})\/(\d{2})/,
            isoStringFormat: 'YYYY/MM/DDTHH:mm:ss.sssZ'
        },
        ptISO: {
            regexFormat: /(\d{2})-(\d{2})-(\d{4})/,
            isoStringFormat: 'DD-MM-YYYYTHH:mm:ss.sssZ'
        },
        ptDot: {
            regexFormat: /(\d{2})\.(\d{2})\.(\d{4})/,
            isoStringFormat: 'DD.MM.YYYYTHH:mm:ss.sssZ'
        },
        pt: {
            regexFormat: /(\d{2})\/(\d{2})\/(\d{4})/,
            isoStringFormat: 'DD/MM/YYYYTHH:mm:ss.sssZ'
        }
    }
};

'use strict';

module.exports = {
    DEMANDWARE_BM_LOGIN: {
        URL : 'dw/oauth2/access_token?client_id',
        CONTENT_TYPE : `application/x-www-form-urlencoded`,
        GRANT_TYPE : 'urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken',
        METHOD : 'POST',
        DATA_URL: 'https://account.demandware.com/dw/oauth2/access_token',
        DATA_GRANT_TYPE: 'client_credentials',
    },
    ALLOWED_METHODS_ARRAY: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'MACRO_POST'],
    ALLOWED_OPERATIONS_METHODS: {
        GET: 'GET',
        POST: 'POST',
        PUT: 'PUT',
        PATCH: 'PATCH',
        DELETE: 'DELETE',
        MACRO_POST: 'MACRO_POST'
    },
    ALLOWED_OBJECTS_ARRAY: ['basket', 'basket_item', 'basket_coupon', 'basket_billing', 'basket_shipping', 'basket_payment', 'order', 'customer', 'coupon'],
    ALLOWED_OBJECTS_CONFIGURATIONS : {
        BASKET: {
            name: 'basket',
            allowedMethods: ['GET', 'POST'],
            hasParams               : {
                value       : true,
                onMethods   : ['GET', 'POST']
            },
            hasPayload              : {
                value       : false
            },
            hasAdditionConfig       : {
                value       : false
            }
        },
        BASKET_ITEM         : {
            name: 'basket_item',
            allowedMethods: ['POST', 'PATCH', 'DELETE'],
            hasParams               : {
                value       : true,
                onMethods   : ['POST', 'PATCH', 'DELETE']
            },
            hasPayload              : {
                value       : true,
                onMethods   : ['POST', 'PATCH']
            },
            hasAdditionConfig       : {
                value       : false
            }
        },
        BASKET_COUPON       : {
            name: 'basket_coupon',
            allowedMethods: ['POST', 'DELETE'],
            hasParams               : {
                value       : true,
                onMethods   : ['POST', 'DELETE']
            },
            hasPayload              : {
                value       : true,
                onMethods   : []
            },
            hasAdditionConfig       : {
                value       : false
            }
        },
        BASKET_BILLING      : {
            name: 'basket_billing',
            allowedMethods: ['PUT'],
            hasParams               : {
                value       : true,
                onMethods   : ['PUT']
            },
            hasPayload              : {
                value       : true,
                onMethods   : ['PUT']
            },
            hasAdditionConfig       : {
                value       : true,
                onMethods   : ['PUT']
            }
        },
        BASKET_SHIPPING     : {
            name: 'basket_shipping',
            allowedMethods: ['GET', 'POST', 'PATCH', 'PUT', 'DELETE'],
            hasParams               : {
                value       : true,
                onMethods   : ['GET', 'POST', 'PATCH', 'PUT', 'DELETE']
            },
            hasPayload              : {
                value       : true,
                onMethods   : ['POST', 'PATCH', 'PUT']
            },
            hasAdditionConfig       : {
                value       : false
            }
        },
        BASKET_PAYMENT      : {
            name: 'basket_payment',
            allowedMethods: ['GET', 'POST', 'PATCH', 'DELETE'],
            hasParams               : {
                value       : true,
                onMethods   : ['GET', 'POST', 'PATCH', 'DELETE']
            },
            hasPayload              : {
                value       : true,
                onMethods   : ['POST', 'PATCH']
            },
            hasAdditionConfig       : {
                 value       : false
            }
        },
        ORDER               : {
            name: 'order',
            allowedMethods: ['POST', 'MACRO_POST'],
            hasParams               : {
                value       : true,
                onMethods   : ['POST', 'MACRO_POST']
            },
            hasPayload              : {
                value       : true,
                onMethods   : ['POST', 'MACRO_POST']
            },
            hasAdditionConfig       : {
                 value       : false
            }
        },
        CUSTOMER            :{
            name: 'customer',
            allowedMethods: ['GET', 'POST', 'PATCH'],
            hasParams               : {
                value       : true,
                onMethods   : ['GET', 'PATCH']
            },
            hasPayload              : {
                value       : true,
                onMethods   : ['POST', 'PATCH']
            },
            hasAdditionConfig       : {
                 value       : false
            }
        },
        COUPON              : {
            name: 'coupon',
            allowedMethods: ['GET'],
            hasParams               : {
                value       : true,
                onMethods   : ['GET']
            },
            hasPayload              : {
                value       : false
            },
            hasAdditionConfig       : {
                 value       : false
            }
        }
    },
    COMMERCE_OBJECTS: {
        BASKET: {
            basket          : 'basket',
            basket_item     : 'basket_item',
            basket_coupon   : 'basket_coupon',
            basket_billing  : 'basket_billing',
            basket_shipping : 'basket_shipping',
            basket_payment  : 'basket_payment'
        },
        ORDER: {
            order           : 'order'
        },
        CUSTOMER: {
            customer        : 'customer'
        },
        COUPON: {
            coupon          : 'coupon'
        }
    },
    COMMERCE_OBJECTS_SCHEMA: {
        OCAPI: {
            ORDER: {
                'billing_address': {
                    'type': 'object',
                    'valuesToRetrieve': [
                        {
                            'inputFieldName': 'address1',
                            'type': 'plain',
                            'outputFieldName': 'billing_street'
                        },
                        {
                            'inputFieldName': 'city',
                            'type': 'plain',
                            'outputFieldName': 'billing_city'
                        },
                        {
                            'inputFieldName': 'country_code',
                            'type': 'plain',
                            'outputFieldName': 'billing_country_code'
                        },
                        {
                            'inputFieldName': 'phone',
                            'type': 'plain',
                            'outputFieldName': 'billing_phone'
                        },
                        {
                            'inputFieldName': 'postal_code',
                            'type': 'plain',
                            'outputFieldName': 'billing_postal_code'
                        },
                        {
                            'inputFieldName': 'state_code',
                            'type': 'plain',
                            'outputFieldName': 'billing_state_code'
                        }
                    ]
                },
                'creation_date': {
                    'type': 'plain',
                    'outputFieldName': 'creation_date'
                },
                'currency': {
                    'type': 'plain',
                    'outputFieldName': 'currency_code'
                },
                'order_no': {
                    'type': 'plain',
                    'outputFieldName': 'order_number'
                },
                'status': {
                    'type': 'plain',
                    'outputFieldName': 'status'
                },
                'origin': {
                    'type': 'plain',
                    'outputFieldName': 'source_integration'
                },
                'store_id': {
                    'type': 'plain',
                    'outputFieldName': 'store_id'
                },
                'shipments': {
                    'type': 'objectsArray',
                    'valuesToRetrieve': [
                        {
                            'shipping_address' : {
                                'type': 'object',
                                'valuesToRetrieve': [
                                    {
                                        'inputFieldName': 'address1',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_street'
                                    },
                                    {
                                        'inputFieldName': 'city',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_city'
                                    },
                                    {
                                        'inputFieldName': 'country_code',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_country_code'
                                    },
                                    {
                                        'inputFieldName': 'phone',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_phone'
                                    },
                                    {
                                        'inputFieldName': 'postal_code',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_postal_code'
                                    },
                                    {
                                        'inputFieldName': 'state_code',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_state_code'
                                    }
                                ]
                            },
                        },
                        {
                            'shipping_method': {
                                'type': 'object',
                                'valuesToRetrieve': [
                                    {
                                        'inputFieldName': 'id',
                                        'type': 'plain',
                                        'outputFieldName': 'shipping_method'
                                    }
                                ]
                            },
                        }
                    ]
                },
                'order_total': {
                    'type': 'plain',
                    'outputFieldName': 'order_total'
                }/*,
                'site_id': {
                    'type': 'plain',
                    'outputFieldName': 'sfcc_site_id'
                }*/,
                'account_id': {
                    'type': 'plain',
                    'outputFieldName': 'account_id'
                },
                'order_token': {
                    'type': 'plain',
                    'outputFieldName': 'order_id'
                },
                'payment_instruments': {
                    'type': 'objectsArray',
                    'valuesToRetrieve': [
                        {
                            'payment_method_id': {
                                'type': 'plain',
                                'outputFieldName': 'payment_method'
                            },
                        }
                    ]
                }
            },
            ORDER_ITEM: {
                'product_id': {
                    'type': 'plain',
                    'outputFieldName': 'pricebook_id'
                },
                'base_price': {
                    'type': 'plain',
                    'outputFieldName': 'unit_price'
                },
                'quantity': {
                    'type': 'plain',
                    'outputFieldName': 'quantity'
                },
                'order_id': {
                    'type': 'plain',
                    'outputFieldName': 'order_id'
                },
                'bonus_product_line_item': {
                    'type': 'plain',
                    'outputFieldName': 'is_bonus_product'
                },
                'tax': {
                    'type': 'plain',
                    'outputFieldName': 'tax_price'
                },
                'item_text': {
                    'type': 'plain',
                    'outputFieldName': 'description'
                },
                'order_item_id': {
                    'type': 'plain',
                    'outputFieldName': 'order_item_id'
                }
            },
            ACCOUNT: {
                'dw_id__c': {
                    'type': 'plain',
                    'outputFieldName': 'dw_id__c'
                },
                'recordtypeid': {
                    'type': 'plain',
                    'outputFieldName': 'recordtypeid'
                },
                'modified_by_dw__c': {
                    'type': 'plain',
                    'outputFieldName': 'modified_by_dw__c'
                },
                'first_name': {
                    'type': 'plain',
                    'outputFieldName': 'firstname'
                },
                'last_name': {
                    'type': 'plain',
                    'outputFieldName': 'lastname'
                },
                'email': {
                    'type': 'plain',
                    'outputFieldName': 'personemail'
                },
                'salutation': {
                    'type': 'plain',
                    'outputFieldName': 'salutation'
                },
                'job_title': {
                    'type': 'plain',
                    'outputFieldName': 'persontitle'
                },
                'birthday': {
                    'type': 'plain',
                    'outputFieldName': 'personbirthdate'
                },
                'phone_mobile': {
                    'type': 'plain',
                    'outputFieldName': 'personmobilephone'
                },
                'fax': {
                    'type': 'plain',
                    'outputFieldName': 'fax'
                },
                'phone': {
                    'type': 'plain',
                    'outputFieldName': 'phone'
                },
                'creation_date': {
                    'type': 'plain',
                    'outputFieldName': 'createddate'
                },
                'site_id__c': {
                    'type': 'plain',
                    'outputFieldName': 'site_id__c'
                },
                'ispersonaccount': {
                    'type': 'plain',
                    'outputFieldName': 'ispersonaccount'
                },
                'customer_id': {
                    'type': 'plain',
                    'outputFieldName': 'sfcc_customer_uuid__c'
                },
                'addresses': {
                    'type': 'objectsArray',
                    'valuesToRetrieve': [
                        {
                            'address1': {
                                'type': 'plain',
                                'outputFieldName': 'billingstreet'
                            },
                            'city': {
                                'type': 'plain',
                                'outputFieldName': 'billingcity'
                            },
                            'postal_code': {
                                'type': 'plain',
                                'outputFieldName': 'billingpostalcode'
                            }
                        }
                    ]
                }
            }
        }

    },
    COMMERCE_OBJECTS_EXCLUSION_FIELDS: {
        OCAPI: {
            ACCOUNT: ['dw_id__c', 'recordtypeid', 'name']
        }
    },
    COMMERCE_OBJECTS_MANDATORY_FIELDS: {
        OCAPI: {
            ACCOUNT         : ['dw_id__c', 'recordtypeid', 'name'],
            BASKET_BILLING  : ['first_name', 'last_name', 'address1', 'city']
        }
    }
};

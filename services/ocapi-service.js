

const clientelingConstants = require('../constants/clienteling-constants.js');
const basketTransactionHelper = require('./helpers/ocapi-helpers/ocapi-basket-transactions-helper.js');
const basketItemTransactionHelper = require('./helpers/ocapi-helpers/ocapi-items-transactions-helper.js');
const basketCouponTransactionHelper = require('./helpers/ocapi-helpers/ocapi-basket-coupon-transactions-helper.js');
const basketBillingTransactionHelper = require('./helpers/ocapi-helpers/ocapi-billing-transaction-helper.js');
const basketShippingTransactionHelper = require('./helpers/ocapi-helpers/ocapi-shipping-transaction-helper.js');
const basketPaymentTransactionHelper = require('./helpers/ocapi-helpers/ocapi-payment-transactions-helper.js');
const orderTransactionHelper = require('./helpers/ocapi-helpers/ocapi-order-transactions-helper.js');
const customerTransactionHelper = require('./helpers/ocapi-helpers/ocapi-customer-transactions-helper.js');
const couponTransactionHelper = require('./helpers/ocapi-helpers/ocapi-coupon-transactions-helper.js');
const performLogins = require('./helpers/ocapi-helpers/ocapi-login-helper');

exports.performCommerceOperations = async (method, object, params, payload,
  apiSettings, _additionalConfiguration, req) => {
  const commerceObjects = clientelingConstants.COMMERCE_OBJECTS;
  const loginToken = await performLogins(apiSettings, params, object);

  if (!loginToken.error) {
    switch (object) {
      case (commerceObjects.BASKET.basket): {
        const returnedBasketObject = await basketTransactionHelper.performBasketOperations(
          apiSettings,
          method,
          loginToken.loginOnBehalfToken,
          loginToken.customerNumber,
          loginToken.customerEmail,
          params,
        ).catch((error) => {
          console.error('Error on perfoming Basket Operations!');
          return ({ status: 500, error, message: 'basket.operations.error' });
        });
        return (returnedBasketObject);
      }

      case (commerceObjects.BASKET.basket_item): {
        const returnedBasketItemObject = await basketItemTransactionHelper.performsItemsOperations(
          apiSettings,
          method,
          loginToken.loginOnBehalfToken,
          params,
          payload,
        ).catch((error) => {
          console.error('Error on performing Basket Item Operations');
          throw ({ status: 500, error, message: 'basket.item.operations.error' });
        });
        return ({ status: 200, payload: returnedBasketItemObject });
      }

      case (commerceObjects.BASKET.basket_coupon): {
        const returnedBasketCouponObject = await basketCouponTransactionHelper
          .performBasketCouponOperations(
            apiSettings,
            method,
            loginToken.loginOnBehalfToken,
            params,
            payload,
          ).catch((error) => {
            console.error('Error on performing Basket Coupon Operations');
            throw ({ status: 500, error, message: 'basket.coupon.operations.error' });
          });
        return ({ status: 200, payload: returnedBasketCouponObject });
      }

      case (commerceObjects.BASKET.basket_billing): {
        const returnedBasketBillingObject = await basketBillingTransactionHelper
          .performBillingOperations(
            apiSettings,
            method,
            loginToken.loginOnBehalfToken,
            params,
            payload,
            _additionalConfiguration,
          ).catch((error) => {
            console.error('Error on performing Basket Billing Operations');
            throw ({ status: 500, error, message: 'basket.billing.operations.error' });
          });
        return ({ status: 200, payload: returnedBasketBillingObject });
      }

      case (commerceObjects.BASKET.basket_shipping): {
        const returnedBasketShippingObject = await basketShippingTransactionHelper
          .performShippingOperations(
            apiSettings,
            method,
            loginToken.loginOnBehalfToken,
            params,
            payload,
          ).catch((error) => {
            console.error('Error on performing Basket Shipping Operations');
            throw ({ status: 500, error, message: 'basket.shipping.operations.error' });
          });
        return ({ status: 200, payload: returnedBasketShippingObject });
      }

      case (commerceObjects.BASKET.basket_payment): {
        const returnedBasketPaymentObject = await basketPaymentTransactionHelper
          .performPaymentOperations(
            apiSettings,
            method,
            loginToken.loginOnBehalfToken,
            params,
            payload,
          ).catch((error) => {
            console.error('Error on performing Basket Payment Operations');
            throw ({ status: 500, error, message: 'basket.payment.operations.error' });
          });
        return ({ status: 200, payload: returnedBasketPaymentObject });
      }

      case (commerceObjects.ORDER.order): {
        const returnedOrderResult = await orderTransactionHelper.performOrderOperations(
          apiSettings,
          method,
          loginToken.loginOnBehalfToken,
          params,
          payload,
          req,
        ).catch((error) => {
          console.error('Error on performing Order Operations');
          throw ({ status: 500, error, message: 'order.operations.error' });
        });
        return (returnedOrderResult);
      }

      case (commerceObjects.CUSTOMER.customer): {
        const returnedCustomer = await customerTransactionHelper.performCustomerOperations(
          apiSettings,
          method,
          loginToken,
          params,
          payload,
          req,
        ).catch((error) => {
          console.error('Error on performing Customer Operations');
          throw ({ status: 500, error, message: 'customer.operations.error' });
        });
        return ({ status: 200, payload: returnedCustomer });
      }

      case (commerceObjects.COUPON.coupon): {
        const retunedCouponsObject = await couponTransactionHelper.performCouponOperations(
          apiSettings,
          method,
          loginToken,
          params,
          payload,
        ).catch((error) => {
          console.error('Error on performing Coupon Operations');
          throw ({ status: 500, error, message: 'coupon.operations.error' });
        });
        return ({ status: 200, payload: retunedCouponsObject });
      }

      default: {
        return ({ status: 400, error: `Token not recognized ${object}`, message: 'object.not.recognized.error' });
      }
    }
  } else {
    return ({ status: 401, error: loginToken, message: 'ocapi.login.error' });
  }
};

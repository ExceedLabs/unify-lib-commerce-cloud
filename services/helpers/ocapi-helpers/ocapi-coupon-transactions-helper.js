const axios = require('axios');
const commerceApiRouterService = require('../../commerce-api-router-service.js');


const performGetOrDeleteRequests = (apiSettings, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = apiSettings.baseUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.couponsEndpoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

exports.performCouponOperations = async (apiSettings, method, loginToken, params, payload) => {
  switch (method) {
    case 'GET':
      const retrievedCoupons = await performGetOrDeleteRequests(
        apiSettings,
        method,
        loginToken,
        params,
      ).catch((error) => {
        console.error('Error on getting coupons! Details: ');
        console.error(error);
        throw new Error('get.customer.error');
      });
      return retrievedCoupons.data;
  }
};

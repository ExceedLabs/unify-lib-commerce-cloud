

const axios = require('axios');
const _ = require('lodash');
const commerceApiRouterService = require('../../commerce-api-router-service.js');
const ocapiUnifyObjectExport = require('../ocapi-helpers/ocapi-unify-object-export-helper.js');
const basketBillingHelper = require('./ocapi-billing-transaction-helper.js');
const basketShippingHelper = require('./ocapi-shipping-transaction-helper.js');
const basketPaymentHelper = require('./ocapi-payment-transactions-helper.js');

const performAddOrUpdateRequests = (apiSettings, method, loginOnBehalfToken, params, payload) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.orderEndpoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

const performBillingInfoMacroRequest = async (apiSettings, loginOnBehalfToken, params, billingInfo) => {
  const addBillingInfo = await basketBillingHelper.performBillingOperations(
    apiSettings,
    'PUT',
    loginOnBehalfToken,
    params,
    billingInfo.payload,
    billingInfo._additionalConfiguration,
  ).catch((error) => {
    console.error('Error on adding billing info on Order Macro!');
    console.error(error);
    throw (
      {
        status: 500,
        error: 'add.basket.billing.order.macro.error',
        message: 'Error on adding basket billing info using Order Macro! Please check your informations or contact the system administrator',
      }
    );
  });
  if (addBillingInfo.status >= 200 && addBillingInfo.status <= 304) {
    return (
      {
        status: 200,
        message: 'OK',
        data: addBillingInfo.data,
      }
    );
  }
  return (
    {
      status: addBillingInfo.status,
      message: 'Error on adding billing info request',
      error: addBillingInfo.data,
    }
  );
};

const performShippingInfoMacroRequest = async (apiSettings, loginOnBehalfToken, params, shippingInfo) => {
  const addShippingInfo = await basketShippingHelper.performShippingOperations(
    apiSettings,
    'PATCH',
    loginOnBehalfToken,
    params,
    shippingInfo.payload,
  ).catch((error) => {
    console.error('Error on adding shipping info on Order Macro!');
    console.error(error);
    throw (
      {
        status: 500,
        error: 'add.basket.shipping.order.macro.error',
        message: 'Error on adding basket shipping info using Order Macro! Please check your informations or contact the system administrator',
      }
    );
  });
  if (addShippingInfo.status >= 200 && addShippingInfo.status <= 304) {
    return (
      {
        status: 200,
        message: 'OK',
        data: addShippingInfo.data,
      }
    );
  }
  return (
    {
      status: addShippingInfo.status,
      message: 'Error on adding shipping info request',
      error: addShippingInfo.data,
    }
  );
};

const performPaymentInfoMacroRequest = async (apiSettings, loginOnBehalfToken, params, paymentInfo) => {
  const addpaymentInfo = await basketPaymentHelper.performPaymentOperations(
    apiSettings,
    'POST',
    loginOnBehalfToken,
    params,
    paymentInfo.payload,
  ).catch((error) => {
    console.error('Error on adding payment info on Order Macro!');
    console.error(error);
    throw (
      {
        status: 500,
        error: 'add.basket.payment.order.macro.error',
        message: 'Error on adding basket payment info using Order Macro! Please check your informations or contact the system administrator',
      }
    );
  });
  if (addpaymentInfo.status >= 200 && addpaymentInfo.status <= 304) {
    return (
      {
        status: 200,
        message: 'OK',
        data: addpaymentInfo.data,
      }
    );
  }
  return (
    {
      status: addpaymentInfo.status,
      message: 'Error on adding payment info request',
      error: addpaymentInfo.data,
    }
  );
};

const performOrderMacro = async (apiSettings, loginOnBehalfToken, params, billingInfo, shippingInfo, paymentInfo) => {
  const addBillingInfoResponse = await performBillingInfoMacroRequest(apiSettings, loginOnBehalfToken, params, billingInfo);

  if (addBillingInfoResponse.status == 200) {
    const addShippingInfoReponse = await performShippingInfoMacroRequest(apiSettings, loginOnBehalfToken, params, shippingInfo);

    if (addShippingInfoReponse.status == 200) {
      const addPaymentInfoResponse = await performPaymentInfoMacroRequest(apiSettings, loginOnBehalfToken, params, paymentInfo);

      if (addPaymentInfoResponse.status == 200) {
        const placeOrderReturn = await performAddOrUpdateRequests(
          apiSettings,
          'POST',
          loginOnBehalfToken,
          params,
          { basket_id: params.basket_id },
        ).catch((error) => {
          console.error('Error on adding Customer Order! Details: ');
          console.error(error);
          return (
            {
              status: 500,
              error: 'place.customer.order.error',
              message: 'Error on placing order to Customer!',
            }
          );
        });
        if (placeOrderReturn.status >= 200 && placeOrderReturn.status <= 304) {
          return (
            {
              status: 200,
              data: placeOrderReturn.data,
              message: 'OK',
            }
          );
        }

        return (
          {
            status: placeOrderReturn.status,
            error: placeOrderReturn.data,
            message: 'Error on placing order',
          }
        );
      }

      return addPaymentInfoResponse;
    }

    return addShippingInfoReponse;
  }

  return addBillingInfoResponse;
};


exports.performOrderOperations = async (apiSettings, method, loginOnBehalfToken, params, payload, req) => {
  const convertParams = _.merge(
    params,
    {
      basePriceBook: apiSettings.basePriceBookValue,
      store_id: apiSettings.storeName,
    },
  );

  switch (method) {
    case 'POST':

      const returnedOrder = await performAddOrUpdateRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
        payload,
      ).catch((error) => {
        console.error('Error on adding Customer Order! Details: ');
        console.error(error);
        throw new Error('add.order.method.error');
      });
      if (returnedOrder.status >= 200 && returnedOrder.status <= 304) {
        const orderObjects = ocapiUnifyObjectExport.exportObjectsToDb(
          'order',
          returnedOrder.data,
          convertParams,
        );

        return (
          {
            status: 200,
            data: _.merge(orderObjects.order, { orderItems: orderObjects.order_items }),
            message: 'OK',
          }
        );
      }
      return (
        {
          status: returnedOrder.status,
          error: returnedOrder.data,
          message: 'Error on Adding billing Address to Basket',
        }
      );

    case 'MACRO_POST':

      const billingInfo = payload.basket_billing;
      const shippingInfo = payload.basket_shipping;
      const paymentInfo = payload.basket_payment;
      const returnedOrderObj = await performOrderMacro(
        apiSettings,
        loginOnBehalfToken,
        params,
        billingInfo,
        shippingInfo,
        paymentInfo,
      );

      if (returnedOrderObj.status == 200) {
        const macroOrderObjects = ocapiUnifyObjectExport.exportObjectsToDb(
          'order',
          returnedOrderObj.data,
          convertParams,
        );
        saveOrderObjectIntoUnifyDb(macroOrderObjects, req);
        return (
          {
            status: 200,
            data: _.merge(macroOrderObjects.order, { orderItems: macroOrderObjects.order_items }),
            message: 'OK',
          }
        );
      }
      throw new Error('add.order.method.error');

    default:
      throw new Error('not.implement.method.error');
  }
};



const axios = require('axios');
const commerceApiRouterService = require('../../commerce-api-router-service.js');

const performGetOrDeleteRequests = (apiSettings, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketShippingEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

const performAddOrUpdateRequests = (apiSettings, method, loginOnBehalfToken, params, payload) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketShippingEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

exports.performShippingOperations = async (apiSettings, method, loginOnBehalfToken, params, payload) => {
  switch (method) {
    case 'GET':
      const shippingMethodsObjects = await performGetOrDeleteRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
      ).catch((error) => {
        console.error('Error on getting shipping methods! Details: ');
        console.error(error);
        throw new Error('get.shipping.methods.error');
      });
      return shippingMethodsObjects.data;
    case 'POST':
      const newShippingBasketObject = await performAddOrUpdateRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
        payload,
      ).catch((error) => {
        console.error('Error adding shippiment info into Customer basket! Details: ');
        console.error(error);
        throw new Error('add.shipping.info.into.basket.error');
      });
      return newShippingBasketObject.data;
    case 'PATCH':
      const updatedShippingBasketObject = await performAddOrUpdateRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
        payload,
      ).catch((error) => {
        console.error('Error updating shippiment info into Customer basket! Details: ');
        console.error(error);
        throw new Error('update.shipping.info.into.basket.error');
      });
      if (updatedShippingBasketObject.status >= 200 && updatedShippingBasketObject.status <= 304) {
        return (
          {
            status: 200,
            data: updatedShippingBasketObject.data,
            message: 'OK',
          }
        );
      }
      return (
        {
          status: updatedShippingBasketObject.status,
          error: updatedShippingBasketObject.data,
          message: 'Error on Adding billing Address to Basket',
        }
      );

    case 'DELETE':
      const deletionUpdatedBasketObject = await performGetOrDeleteRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
      ).catch((error) => {
        console.error('Error deleting shipping info from Customer basket! Details: ');
        console.error(error);
        throw new Error('delete.shipment.info.from.basket.error');
      });
      return deletionUpdatedBasketObject.data;
    default:
      throw new Error('not.implement.method.error');
  }
};



const _ = require('lodash');

exports.retrieveLoginEndpoint = (version, params) => `/shop/${version}/customers/${params.customer_sfcc_uuid}/auth`;

exports.retrieveBasketEndpoints = (version, method, params) => {
  switch (method) {
    case 'GET':
      return `/shop/${version}/baskets/${params.basket_id}`;
    case 'POST':
      return `/shop/${version}/baskets`;
    case 'PUT':
      return `/shop/${version}/baskets/${params.basket_id}/customer`;
    case 'DELETE':
      return `/shop/${version}/baskets/${params.basket_id}`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveBasketItemEndpoints = (version, method, params) => {
  switch (method) {
    case 'POST':
      return `/shop/${version}/baskets/${params.basket_id}/items`;
    case 'PATCH':
      return `/shop/${version}/baskets/${params.basket_id}/items/${params.item_id}`;
    case 'DELETE':
      return `/shop/${version}/baskets/${params.basket_id}/items/${params.item_id}`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveBasketCouponEndpoints = (version, method, params) => {
  switch (method) {
    case 'POST':
      return `/shop/${version}/baskets/${params.basket_id}/coupons`;
    case 'DELETE':
      return `/shop/${version}/baskets/${params.basket_id}/coupons/${params.coupon_item_id}`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveBasketBillingEndpoints = (version, method, params, additionalInfo) => {
  switch (method) {
    case 'PUT':
      const baseUrl = `/shop/${version}/baskets/${params.basket_id}/billing_address`;
      if (_.isEmpty(additionalInfo)) {
        return baseUrl;
      }
      const options = Object.keys(additionalInfo);
      const lastUrlPart = options.map(elem => `${elem}=${additionalInfo[elem]}`).reduce((acc, cur) => `${acc}&${cur}`);
      return `${baseUrl}?${lastUrlPart}`;

    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveBasketShippingEndpoints = (version, method, params) => {
  switch (method) {
    case 'GET':
      return `/shop/${version}/baskets/${params.basket_id}/shipments/${params.shipment_id}/shipping_methods`;
    case 'POST':
      return `/shop/${version}/baskets/${params.basket_id}/shipments`;
    case 'PATCH':
      return `/shop/${version}/baskets/${params.basket_id}/shipments/${params.shipment_id}`;
    case 'PUT':
      return `/shop/${version}/baskets/${params.basket_id}/shipping_method`;
    case 'DELETE':
      return `/shop/${version}/baskets/${params.basket_id}/shipments/${params.shipment_id}`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrievePaymentEndpoints = (version, method, params) => {
  switch (method) {
    case 'GET':
      return `/shop/${version}/baskets/${params.basket_id}/payment_methods`;
    case 'POST':
      return `/shop/${version}/baskets/${params.basket_id}/payment_instruments`;
    case 'PATCH':
      return `/shop/${version}/baskets/${params.basket_id}/payment_instruments/${params.payment_id}`;
    case 'DELETE':
      return `/shop/${version}/baskets/${params.basket_id}/payment_instruments/${params.payment_id}`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveOrderEndpoints = (version, method, params) => {
  switch (method) {
    case 'POST':
      return `/shop/${version}/orders`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveCustomerBasketsEndpoint = (version, method, params) => {
  switch (method) {
    case 'GET':
      return `/shop/${version}/customers/${params.customer_sfcc_uuid}/baskets`;
    default:
      throw new Error('method.not.allowed.error');
  }
};

exports.retrieveCouponEndpoints = (version, method, params, additionalInfo) => {
  switch (method) {
    case 'GET':
      const baseUrl = `/dw/data/${version}/sites/${params.site_id}/coupons`;
      if (_.isEmpty(additionalInfo)) {
        return baseUrl;
      }
      const options = Object.keys(additionalInfo);
      const lastUrlPart = options.map(elem => `${elem}=${additionalInfo[elem]}`).reduce((acc, cur) => `${acc}&${cur}`);
      return `${baseUrl}?${lastUrlPart}`;

    default:
      throw new Error('method.not.allowed.error');
  }
};

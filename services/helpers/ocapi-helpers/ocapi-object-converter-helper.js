

const _ = require('lodash');
const moment = require('moment');
const clientelingConstants = require('../../../constants/clienteling-constants.js');

const extractObjectValues = (objectKeys, objectSchema, payload, fieldsList = []) => {
  if (objectKeys.length == 0) {
    return fieldsList;
  }
  const key = _.head(objectKeys);
  const schema = objectSchema[key];
  switch (schema.type) {
    case 'object':
      const innerPayloadValue = payload[key];
      if (!_.isEmpty(innerPayloadValue)) {
        const innerObjects = schema.valuesToRetrieve.map((value) => {
          const payloadFieldvalue = innerPayloadValue[value.inputFieldName];
          const outputFieldName = value.outputFieldName;
          return { [outputFieldName]: payloadFieldvalue };
        });
        return extractObjectValues(_.tail(objectKeys), objectSchema, payload, fieldsList.concat(innerObjects));
      }
      return extractObjectValues(_.tail(objectKeys), objectSchema, payload, fieldsList);

    case 'plain':
      const value = payload[key];
      const outputField = { [schema.outputFieldName]: value };
      return extractObjectValues(_.tail(objectKeys), objectSchema, payload, fieldsList.concat(outputField));
    case 'objectsArray':
      if (!_.isEmpty(payload[key]) && payload[key].length > 0) {
        const innerArrayObjects = _.flatten(
          schema.valuesToRetrieve.map((value) => {
            const innerKey = Object.keys(value);
            const innerPayload = payload[key][0];
            return extractObjectValues(
              innerKey,
              value,
              innerPayload,
              [],
            );
          }),
        );
        return extractObjectValues(_.tail(objectKeys), objectSchema, payload, fieldsList.concat(innerArrayObjects));
      }
      return extractObjectValues(_.tail(objectKeys), objectSchema, payload, fieldsList);
  }
};

const retrieveRemapedObject = (objectSchema, payload) => {
  const keysToRetrieve = Object.keys(objectSchema);
  const scattedObject = extractObjectValues(
    keysToRetrieve,
    objectSchema,
    payload,
    [],
  );
  const totalFlatObject = _.flattenDeep(scattedObject);
  const mergedObject = totalFlatObject.reduce((acc, cur) => _.merge(acc, cur));
  return mergedObject;
};

const mountFinalOrderObject = (orderObj, account_id, storeId) => {
  const orderSchema = clientelingConstants.COMMERCE_OBJECTS_SCHEMA.OCAPI.ORDER;
  return _.merge(orderObj,
    {
      [orderSchema.account_id.outputFieldName]: account_id,
      [orderSchema.origin.outputFieldName]: 'CLIENTELING',
      [orderSchema.store_id.outputFieldName]: storeId,
    });
};

const mountFinalOrderItemObject = (orderItemObj, orderId, basePriceBook, currencyCode) => {
  const orderItemSchema = clientelingConstants.COMMERCE_OBJECTS_SCHEMA.OCAPI.ORDER_ITEM;
  const pricebookId = `${basePriceBook}_${orderItemObj[orderItemSchema.product_id.outputFieldName]}_${currencyCode}`;
  const externalIdBaseString = `${orderId}_${orderItemObj[orderItemSchema.product_id.outputFieldName]}`;
  const externalId = Buffer.from(externalIdBaseString).toString('base64');
  return _.merge(orderItemObj,
    {
      [orderItemSchema.product_id.outputFieldName]: pricebookId,
      [orderItemSchema.order_item_id.outputFieldName]: externalId,
      [orderItemSchema.order_id.outputFieldName]: orderId,
    });
};

const mountFinalAccountObject = (accountObj, accountRecordType, baseSiteUrl) => {
  const accountSchema = clientelingConstants.COMMERCE_OBJECTS_SCHEMA.OCAPI.ACCOUNT;
  const externalIdBaseString = `${accountObj[accountSchema.customer_id.outputFieldName]}_${accountObj[accountSchema.email.outputFieldName]}`;
  const externalId = Buffer.from(externalIdBaseString).toString('base64');
  return _.merge(accountObj,
    {
      [accountSchema.dw_id__c.outputFieldName]: externalId,
      [accountSchema.recordtypeid.outputFieldName]: accountRecordType,
      [accountSchema.ispersonaccount.outputFieldName]: true,
      [accountSchema.modified_by_dw__c.outputFieldName]: true,
      [accountSchema.birthday.outputFieldName]: moment(accountObj[accountSchema.birthday.outputFieldName]).format('llll'),
      [accountSchema.site_id__c.outputFieldName]: baseSiteUrl, /* ,
            [accountSchema.name.outputFieldName] : `${accountObj[accountSchema.first_name.outputFieldName]} ${accountObj[accountSchema.last_name.outputFieldName]}` */
    });
};

exports.convertOCAPIObjectOnUnifyObjects = (object, payload, params) => {
  const ocapiObjects = clientelingConstants.COMMERCE_OBJECTS_SCHEMA.OCAPI;
  switch (object) {
    case 'order':
      const account_id = params.account_id;
      const storeId = params.store_id;
      const unifyOrderObject = retrieveRemapedObject(
        ocapiObjects.ORDER,
        payload,
      );
      return mountFinalOrderObject(unifyOrderObject, account_id, storeId);
    case 'order_item':
      if (payload.length) {
        const orderId = params.order_id;
        const basePriceBook = params.basePriceBook;
        const currencyCode = params.currencyCode;
        const unifyOrderItemObjects = payload.map((ocapiOrderItem) => {
          const unifyOrderItem = retrieveRemapedObject(
            ocapiObjects.ORDER_ITEM,
            ocapiOrderItem,
          );
          return mountFinalOrderItemObject(unifyOrderItem, orderId, basePriceBook, currencyCode);
        });
        return unifyOrderItemObjects;
      }
    case 'customer':
      const accountRecordType = params.accountRecordType;
      const baseSiteUrl = params.baseSiteUrl;
      const unifyAccountObject = retrieveRemapedObject(
        ocapiObjects.ACCOUNT,
        payload,
      );
      return mountFinalAccountObject(unifyAccountObject, accountRecordType, baseSiteUrl);
  }
};

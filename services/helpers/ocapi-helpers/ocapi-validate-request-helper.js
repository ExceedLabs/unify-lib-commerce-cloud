const _ = require('lodash');
const clientelingConstants = require('../../../constants/clienteling-constants.js');


exports.validateObjectsConditions = (object, method, params, payload, additionalConfig) => {
  const configurations = clientelingConstants.ALLOWED_OBJECTS_CONFIGURATIONS;
  const configurationsObject = configurations[object.toUpperCase()];

  if (_.isEmpty(configurationsObject)) {
    return (
      {
        status: 404,
        message: 'No object configuration found! Please check the object spelling or contact the administrator',
        error: 'object.configuration.not.found.error',
      }
    );
  }
  if (configurationsObject.allowedMethods.includes(method)) {
    if (configurationsObject.hasParams.value && configurationsObject.hasParams.onMethods.includes(method)) {
      if (_.isEmpty(params)) {
        return (
          {
            status: 400,
            message: 'No params object found! Please check your http body or contact the administrator',
            error: 'required.param.object.missing',
          }
        );
      }
    }
    if (configurationsObject.hasPayload.value && configurationsObject.hasPayload.onMethods.includes(method)) {
      if (_.isEmpty(payload)) {
        return (
          {
            status: 400,
            message: 'No payload object found! Please check your http body or contact the administrator',
            error: 'required.param.object.missing',
          }
        );
      }
    }
    if (configurationsObject.hasAdditionConfig.value && configurationsObject.hasAdditionConfig.onMethods.includes(method)) {
      if (_.isEmpty(additionalConfig)) {
        console.warn(`The object: ${object} using the method ${method} allows the use of Additional configurations, 
                        but none was given into the payload. If you don't want to provide any additional configuration, just ignore this
                        warning.`);
      }
    }
  } else {
    return (
      {
        status: 405,
        message: 'Method not allowed for this object! Please check the required method or contact the administrator',
        error: 'method.not.allowed.error',
      }
    );
  }


  console.log('Object validation successful');
  return (
    {
      status: 200,
      message: 'OK',
    }
  );
};

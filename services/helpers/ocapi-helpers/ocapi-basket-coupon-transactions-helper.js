

const axios = require('axios');
const commerceApiRouterService = require('../../commerce-api-router-service.js');

const performGetOrDeleteRequests = (apiSettings, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketCouponEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

const performAddOrUpdateRequests = (apiSettings, method, loginOnBehalfToken, params, payload) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketCouponEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

exports.performBasketCouponOperations = async (apiSettings, method, loginToken, params, payload) => {
  switch (method) {
    case 'DELETE':
      const deletedCouponCodeBasket = await performGetOrDeleteRequests(
        apiSettings,
        method,
        loginToken,
        params,
      ).catch((error) => {
        console.error('Error on deleting coupon from basket! Details: ');
        console.error(error);
        throw new Error('delete.basket.coupon.error');
      });
      return deletedCouponCodeBasket.data;
    case 'POST':

      const updatedBasketCouponObj = await performAddOrUpdateRequests(
        apiSettings,
        method,
        loginToken,
        params,
        payload,
      ).catch((error) => {
        console.error('Error on adding coupon into basket! Details: ');
        console.error(error);
        throw new Error('add.basket.coupon.error');
      });
      return updatedBasketCouponObj.data;
  }
};

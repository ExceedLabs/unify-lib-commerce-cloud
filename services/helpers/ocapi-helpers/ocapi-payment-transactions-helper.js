

const axios = require('axios');
const commerceApiRouterService = require('../../commerce-api-router-service.js');

const performGetOrDeleteRequests = (apiSettings, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketPaymentEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

const performAddOrUpdateRequests = (apiSettings, method, loginOnBehalfToken, params, payload) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketPaymentEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

exports.performPaymentOperations = async (apiSettings, method, loginOnBehalfToken, params, payload) => {
  switch (method) {
    case 'GET':

      const paymentMethodsObjects = await performGetOrDeleteRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
      ).catch((error) => {
        console.error('Error on getting payment methods! Details: ');
        console.error(error);
        throw new Error('get.payment.methods.error');
      });
      return paymentMethodsObjects.data;
    case 'POST':

      const addPaymentMethodBasketObject = await performAddOrUpdateRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
        payload,
      ).catch((error) => {
        console.error('Error on adding payment method to Basket! Details: ');
        console.error(error);
        throw new Error('add.payment.method.error');
      });
      if (addPaymentMethodBasketObject.status >= 200 && addPaymentMethodBasketObject.status <= 304) {
        return (
          {
            status: 200,
            data: addPaymentMethodBasketObject.data,
            message: 'OK',
          }
        );
      }
      return (
        {
          status: addPaymentMethodBasketObject.status,
          error: addPaymentMethodBasketObject.data,
          message: 'Error on Adding billing Address to Basket',
        }
      );

    case 'PATCH':

      const updatePaymentMethodBasketObject = await performAddOrUpdateRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
        payload,
      ).catch((error) => {
        console.error('Error on updating payment method to Basket! Details: ');
        console.error(error);
        throw new Error('update.payment.method.error');
      });
      return updatePaymentMethodBasketObject.data;
    case 'DELETE':

      const deletepaymentMethodBasketObject = await performGetOrDeleteRequests(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
      ).catch((error) => {
        console.error('Error on deleting payment from Basket! Details: ');
        console.error(error);
        throw new Error('delete.payment.method.error');
      });
      return deletepaymentMethodBasketObject.data;
  }
};

const axios = require('axios');
const _ = require('lodash');


exports.getCustomer = async (currentLib, tokenBM, params) => {
  const siteBaseUrl = currentLib.credentials.baseUrl;
  const version = currentLib.credentials.version;
  const siteId = currentLib.credentials.siteId;
  const URL = `${siteBaseUrl}/s/-/dw/data/${version}/customer_lists/${siteId}/customer_search`;
  return axios({
    method: 'POST',
    url: URL,
    data: {
      query: {
        text_query: {
          fields: ['email', 'first_name', 'last_name', 'phone_home', 'phone_business', 'phone_mobile'],
          search_phrase: params.email || params.firstName || params.phone,
        },
      },
      select: '(**)',
    },
    headers: {
      Authorization: `${tokenBM}`,
      'Content-Type': 'application/json',
    },
  });
};

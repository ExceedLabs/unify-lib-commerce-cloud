

const axios = require('axios');
const commerceApiRouterService = require('../../commerce-api-router-service.js');

const addOrUpdateItemOnBasket = (apiSettings, method, loginOnBehalfToken, params, payload) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketItemEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

const deleteItemOnBasket = (apiSettings, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketItemEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

exports.performsItemsOperations = async (apiSettings, method, loginOnBehalfToken, params, payload) => {
  if (method == 'POST' || method == 'PATCH') {
    const updatedBasket = await addOrUpdateItemOnBasket(
      apiSettings,
      method,
      loginOnBehalfToken,
      params,
      payload,
    ).catch((error) => {
      console.error('Error on adding items into Customer basket! Details: ');
      console.error(error);
      throw new Error('add.or.update.item.into.basket.error');
    });
    return updatedBasket.data;
  } if (method == 'DELETE') {
    const updatedBasket = await deleteItemOnBasket(
      apiSettings,
      method,
      loginOnBehalfToken,
      params,
    ).catch((error) => {
      console.error('Error on adding items into Customer basket! Details: ');
      console.error(error);
      throw new Error('remove.item.from.basket.error');
    });
    return updatedBasket.data;
  }
  throw new Error('not.implement.method.error');
};



const _ = require('lodash');
const axios = require('axios');
const qs = require('qs');
const clientelingConstants = require('../../../constants/clienteling-constants.js');
const OcapiRequestHelper = require('./ocapi-request-helper');

/**
 * Example Output
 * {
 *  finalUrl - https://osfglobal19-alliance-prtnr-eu05-dw.demandware.net/dw/oauth2/access_token?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
 *  authorizationHeader- Y2xpZW50ZWxsaW5nOjlyRyxHUjRzMTphYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=
 *  method - POST
 *  grantType - urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken
 *  contentType - application/x-www-form-urlencoded
 * }
*/
const getBMToken = (finalUrl, authorizationHeader, method, grantType, contentType) => axios({
  method,
  url: finalUrl,
  data: qs.stringify({ grant_type: `${grantType}` }),
  headers: {
    Authorization: `Basic ${authorizationHeader}`,
    'Content-Type': contentType,
  },
});

const saveBMTokenOnRedis = async (key, value, RedisService) => {
  const redisResponse = await RedisService
    .set(key, value, 'EX', 720)
    .catch((err) => {
      console.error(`Error on saving BM Token on Redis! Details: ${err}`);
    });
  if (!_.isEmpty(redisResponse)) {
    console.log('BM Token successfully saved on Redis.');
  }
};

/**
 * Try to recover the information token from Redis DB, if not found it try
 * to get from Demandware API
 * @param {string} authorizationHeader - key to recover information from redis
 * @param {string} finalUrl - Complete URL to Demandware Instance
 * @param {object} demandwareBmLoginInfo
*/
exports.retrieveAndManageBMToken = async (obj, configBMTokenJson) => {
  const { authorizationHeader, finalUrl, demandwareBmLoginInfo } = configBMTokenJson;
  const { RedisService } = obj.privateLibraries;

  const reply = await RedisService
    .get(authorizationHeader)
    .catch(async (err) => {
      console.error(`An error occurred on retrieving object from Redis. Details: ${err}
            Skipping this step and retrieving a fresh token from DW`);
      const response = await getBMToken(
        finalUrl,
        authorizationHeader,
        demandwareBmLoginInfo.METHOD,
        demandwareBmLoginInfo.GRANT_TYPE,
        demandwareBmLoginInfo.CONTENT_TYPE,
      );
      if (response.status >= 200 && response.status <= 304) {
        saveBMTokenOnRedis(authorizationHeader, response.data);
        return response.data;
      }
      throw new Error('retrieve.bm.token.error');
    });

  if (!_.isEmpty(reply) && !_.isEmpty(reply.data) && reply.data.length > 0) {
    return JSON.parse(reply.data);
  }
  const response = await getBMToken(
    finalUrl,
    authorizationHeader,
    demandwareBmLoginInfo.METHOD,
    demandwareBmLoginInfo.GRANT_TYPE,
    demandwareBmLoginInfo.CONTENT_TYPE,
  );
  if (response.status >= 200 && response.status <= 304) {
    saveBMTokenOnRedis(authorizationHeader, response.data, RedisService);
    return response.data;
  }
  throw new Error('retrieve.bm.token.error');
  // TODO - DEAL WITH HTTP ERRORS
};

const saveLoginOnBehalfTokenOnRedis = async (customerKey, token, RedisService) => {
  const redisAnswer = await RedisService.set(customerKey, token, 'EX', 719).catch((error) => {
    console.error(`Redis insertion error! Details ${error}`);
    return undefined;
  });

  return new Promise((resolve, reject) => {
    if (_.isEmpty(redisAnswer)) {
      console.error('Error trying to insert value on Redis!');
      reject('redis.insertion.error');
    } else {
      console.log(`Successful Customer Key ${customerKey} login on behalf redis insertion`);
      return resolve(redisAnswer);
    }
  });
};

exports.performDemandwareBmLogin = async (infoOrganization) => {
  try {
    const userId = infoOrganization.credentials.userId;
    const userPassword = infoOrganization.credentials.userPassword;
    const clientId = infoOrganization.credentials.clientId;
    const clientPassword = infoOrganization.credentials.clientPassword;
    const baseUrl = infoOrganization.credentials.baseUrl;
    const demandwareBmLoginInfo = clientelingConstants.DEMANDWARE_BM_LOGIN;
    const finalUrl = `${baseUrl}/${demandwareBmLoginInfo.URL}=${clientId}`;

    if (userId && userPassword && clientId && clientPassword && baseUrl) {
      const BASE64_STRING = `${userId}:${userPassword}:${clientPassword}`;
      const authorizationHeader = Buffer.from(BASE64_STRING).toString('base64');
      return {
        authorizationHeader,
        finalUrl,
        demandwareBmLoginInfo,
      };
    }
    throw new Error('required.configuration.not.found');
  } catch (error) {
    throw new Error('required.configuration.not.found');
  }
};

exports.performDemandwareLoginOnBehalf = async (obj) => {
  const { customerId, tokenBearer, currentLib } = obj;

  if (customerId) {
    const baseUrl = currentLib.credentials.siteUrl;
    const version = currentLib.credentials.version;
    const finalUrl = baseUrl + OcapiRequestHelper.loginCustomerEndPoints(version, customerId);
    const response = await axios({
      method: 'POST',
      url: finalUrl,
      headers: {
        Authorization: `${tokenBearer}`,
        'Content-Type': 'application/json',
      },
    }).catch((error) => {
      console.error(`Unable to perform OCAPI request! Details ${error}`);
      throw({ stack: error.response.data.fault, message: error.message, status: error.response.status });
    });

    if (response.status >= 200 && response.status <= 304) {
      return {
        customerNumber: response.data.customer_no,
        customerEmail: response.data.email,
        token: response.headers.authorization,
        customerId: response.data.customer_id,
      };
    }
    console.error(`Unable to perform Login on Behalf Request! Error Details:
                Status - ${response.status} | Data - ${response.data}`);
    if (response.status == 401) {
      return response;
    } else { // TODO - DEAL WITH HTTP ERRORS
      throw(response);
    }
  }
  return { token: currentLib.credentials.BMToken || '' };
};

exports.getTokenBearer = BMTokenJson => `${BMTokenJson.token_type} ${BMTokenJson.access_token}`;

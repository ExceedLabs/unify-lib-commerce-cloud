const clientelingConstants = require('../../../constants/clienteling-constants');
const loginTransactionHelper = require('./ocapi-login-transactions-helper');

const isValidLoginOnBehalfToken = (loginOnBehalfTokenObject) => {
  if (!loginOnBehalfTokenObject) return false;
  return loginOnBehalfTokenObject && loginOnBehalfTokenObject.customerEmail
  && loginOnBehalfTokenObject.customerNumber && loginOnBehalfTokenObject.loginOnBehalfToken;
};

exports.getTokenBearer = async (BMTokenJson, object) => {
  try {
    const commerceObjects = clientelingConstants.COMMERCE_OBJECTS;

    if (BMTokenJson && BMTokenJson.access_token) {
      const bmToken = BMTokenJson.access_token;
      if (object != commerceObjects.CUSTOMER.customer && object != commerceObjects.COUPON.coupon) {
        const loginOnBehalfTokenObject = await loginTransactionHelper.performDemandwareLoginOnBehalf(infoOrganization, params, bmToken);
        if (isValidLoginOnBehalfToken(loginOnBehalfTokenObject)) {
          return loginOnBehalfTokenObject;
        }
        throw new Error('no.login.on.behalf.error');
      } else {
        return `Bearer ${bmToken}`;
      }
    } else {
      throw new Error('no.bm.token.error');
    }
  } catch (e) {
    console.error('Error on performLogins function! Error details: ');
    console.error(e);
    return ({ status: 500, error: e });
  }
};

/**
 * Perform login to Demandware and returns a token Bearer
 * @param {object} infoOrganization -
 * @param {object} params -
 * @param {object} object -
 * @returns {string} Bearer <token>
 */
exports.performLogins = async (infoOrganization, params, object) => {
  try {
    const commerceObjects = clientelingConstants.COMMERCE_OBJECTS;
    const getBMTokenJson = await loginTransactionHelper.performDemandwareBmLogin(infoOrganization);

    if (getBMTokenJson && getBMTokenJson.access_token) {
      const bmToken = getBMTokenJson.access_token;
      if (object != commerceObjects.CUSTOMER.customer && object != commerceObjects.COUPON.coupon) {
        const loginOnBehalfTokenObject = await loginTransactionHelper.performDemandwareLoginOnBehalf(infoOrganization, params, bmToken);
        if (isValidLoginOnBehalfToken(loginOnBehalfTokenObject)) {
          return loginOnBehalfTokenObject;
        }
        throw new Error('no.login.on.behalf.error');
      } else {
        return `Bearer ${bmToken}`;
      }
    } else {
      throw new Error('no.bm.token.error');
    }
  } catch (e) {
    console.error('Error on performLogins function! Error details: ');
    console.error(e);
    return ({ status: 500, error: e });
  }
};

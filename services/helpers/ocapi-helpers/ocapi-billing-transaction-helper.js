

const axios = require('axios');
const _ = require('lodash');
const commerceApiRouterService = require('../../commerce-api-router-service.js');
const commerceObjectConstants = require('../../../constants/clienteling-constants.js');

const validateBillingAddressRequest = (params, payload) => {
  const mandatoryPayloadFields = commerceObjectConstants.COMMERCE_OBJECTS_MANDATORY_FIELDS.OCAPI.BASKET_BILLING;
  if (params.basket_id && params.basket_id.length > 0) {
    if (params.customer_sfcc_uuid && params.customer_sfcc_uuid.length > 0) {
      const payloadKeys = Object.keys(payload);
      const payloadKeysValidationResult = mandatoryPayloadFields
        .map(key => payloadKeys.includes(key))
        .reduce((acc, cur) => acc && cur);
      if (payloadKeysValidationResult) {
        return ({
          status: 200,
          message: 'OK',
        });
      }
      return ({
        status: 400,
        error: 'missing.mandatory.fields.error',
        message: 'You must set all mandatory fields for billing address request! [first_name, last_name, address1, city and country_code]',
      });
    }
    return ({
      status: 400,
      error: 'no.customer.sfcc.uuid.error',
      message: 'You must set a customer_sfcc_uuid on params object for billing address request!',
    });
  }
  return ({
    status: 400,
    error: 'no.basket.id.error',
    message: 'You must set a basket_id on params object for billing address request!',
  });
};

const addBillingAddressToBasket = (apiSettings, method, loginOnBehalfToken, params, payload, _additionalConfig) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketBillingEndPoints(
    apiSettings,
    method,
    params,
    _additionalConfig,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

exports.performBillingOperations = async (apiSettings, method, loginOnBehalfToken, params, payload, _additionalConfig) => {
  switch (method) {
    case 'PUT':

      const validationResult = validateBillingAddressRequest(params, payload);
      if (validationResult.status == 200) {
        const updatedBasket = await addBillingAddressToBasket(
          apiSettings,
          method,
          loginOnBehalfToken,
          params,
          payload,
          _additionalConfig,
        ).catch((error) => {
          console.error('Error on add billing address to Basket');
          console.log(error);
          throw new Error('add.billing.address.into.basket.error');
        });
        if (updatedBasket.status >= 200 && updatedBasket.status <= 304) {
          return (
            {
              status: 200,
              data: updatedBasket.data,
              message: 'OK',
            }
          );
        }
        return (
          {
            status: updatedBasket.status,
            error: updatedBasket.data,
            message: 'Error on Adding billing Address to Basket',
          }
        );
      }

      throw validationResult;

    default:
      throw new Error('not.implement.method.error');
  }
};

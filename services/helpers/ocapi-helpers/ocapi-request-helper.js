const axios = require('axios');
const OcapiEndpointHelper = require('./ocapi-endpoint-helper');

exports.loginCustomerEndPoints = (version, customerId) => `/shop/${version}/customers/${customerId}/auth`;

exports.performOcapiRequest = (infoOrganization, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = infoOrganization.credentials.siteUrl;
  const version = infoOrganization.credentials.version;
  const finalAddItemUrl = siteBaseUrl + OcapiEndpointHelper.customerEndpoints(
    version,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

exports.retrieveCustomerEndpoints = (version, method, customerId) => {
  try {
    switch (method) {
      case 'GET':
        return `/shop/${version}/customers/${customerId}`;
      case 'POST':
        return `/shop/${version}/customers`;
      case 'PATCH':
        return `/shop/${version}/customers/${customerId}`;
      default:
        throw new Error('method.not.allowed.error');
    }
  } catch (error) {
    throw new Error('error.retrieve.customer.endpoint');
  }
};

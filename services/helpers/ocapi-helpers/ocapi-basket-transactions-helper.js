

const axios = require('axios');
const _ = require('lodash');
const commerceApiRouterService = require('../../commerce-api-router-service.js');
const clientelingHttpHandler = require('../unify-helpers/clienteling-http-handler-helper.js');

const retrieveCustomerBaskets = (apiSettings, loginOnBehalfToken, params) => {
  const apiIdentifier = apiSettings.apiIdentifier;
  const baseUrl = apiSettings.siteUrl;
  const finalUrl = baseUrl + commerceApiRouterService.customerBasketsEndpoints(
    apiSettings,
    'GET',
    params,
  );
  const newParams = _.merge(params, { token: loginOnBehalfToken });
  return clientelingHttpHandler.makeHttpRequest(
    apiIdentifier,
    finalUrl,
    'GET',
    newParams,
  );
};

const createEmptyBasket = (apiSettings, method, loginOnBehalfToken) => {
  const baseUrl = apiSettings.siteUrl;
  const createBasketFinalUrl = baseUrl + commerceApiRouterService.basketEndPoints(
    apiSettings,
    method,
    undefined,
    undefined,
  );

  return axios({
    method,
    url: createBasketFinalUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

const getOrDeleteOperation = (apiSettings, method, loginOnBehalfToken, params) => {
  const siteBaseUrl = apiSettings.siteUrl;
  const finalAddItemUrl = siteBaseUrl + commerceApiRouterService.basketEndPoints(
    apiSettings,
    method,
    params,
  );
  return axios({
    method,
    url: finalAddItemUrl,
    headers: {
      Authorization: `${loginOnBehalfToken}`,
      'Content-Type': 'application/json',
    },
  });
};

const createCustomerBasket = async (apiSettings, method, loginOnBehalfToken,
  customerNumber, customerEmail, params) => {
  try {
    const hasBaskets = await retrieveCustomerBaskets(apiSettings, loginOnBehalfToken, params).catch((error) => {
      console.error('Error on retrieve customer baskets');
      console.error(error);
      return (
        {
          status: 500,
          error: 'retrieve.customer.baskets.error',
          message: 'Error on retrieving all baskets! Please check the logs for more information',
        }
      );
    });

    if (hasBaskets.status >= 200 && hasBaskets.status <= 304) {
      if (hasBaskets.data && hasBaskets.data.total > 0) {
        return ({
          status: 200,
          message: 'OK',
          data: hasBaskets.data.baskets[0],
        });
      }

      const apiResponse = await createEmptyBasket(apiSettings, method, loginOnBehalfToken).catch((error) => {
        console.error('Error on creating empty basket');
        console.error(error);
        return (
          {
            status: 500,
            error: 'create.basket.error',
            message: 'Error on creating an empty basket! Please check the logs for more information',
          }
        );
      });
      if (apiResponse.status >= 200 && apiResponse.status <= 304) {
        const emptyBasketId = apiResponse.data.basket_id;
        const addCustomerToBasketFinalUrl = apiSettings.siteUrl + commerceApiRouterService.basketEndPoints(
          apiSettings,
          'PUT',
          { basket_id: emptyBasketId },
        );
        const customerBasket = await axios({
          method: 'PUT',
          url: addCustomerToBasketFinalUrl,
          data: { customer_no: customerNumber, email: customerEmail },
          headers: {
            Authorization: `${loginOnBehalfToken}`,
            'Content-Type': 'application/json',
          },
        }).catch((error) => {
          console.error('Error on placing empty basket to customer');
          console.error(error);
          return (
            {
              status: 500,
              error: 'place.basket.error',
              message: 'Error on placing an empty basket to a customer! Please check the logs for more information',
            }
          );
        });
        if (customerBasket.status >= 200 && customerBasket.status <= 304) {
          return (customerBasket);
        }
        return (customerBasket);
      }
      return (apiResponse);
    }
    return (hasBaskets);
  } catch (e) {
    console.error('Internal error on defineCustomerToBasket function!');
    console.error(e);
    return ({
      status: 500,
      error: e,
      message: 'Internal Server Error',
    });
  }
};

exports.performBasketOperations = async (apiSettings, method, loginOnBehalfToken,
  customerNumber, customerEmail, params) => {
  switch (method) {
    case 'POST':
      const emptyCustomerBasket = await createCustomerBasket(apiSettings, method, loginOnBehalfToken,
        customerNumber, customerEmail, params).catch((error) => {
        console.error('Error on creating Customer Empty Basket! Details: ');
        console.error(error);
        throw new Error('basket.creation.error');
      });
      return emptyCustomerBasket;
    case 'GET':
      const retrievedBaskets = await getOrDeleteOperation(
        apiSettings,
        method,
        loginOnBehalfToken,
        params,
      ).catch((error) => {
        console.error('Error on retrieving Customer Baskets! Details: ');
        console.error(error);
        throw new Error('basket.retrieve.error');
      });
      return retrievedBaskets;
    default:
      throw new Error('not.implement.method.error');
  }
};

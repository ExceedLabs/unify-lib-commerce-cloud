const _ = require('lodash');
const ocapiObjectConverterHelper = require('./ocapi-object-converter-helper.js');
const clientelingConstants = require('../../../constants/clienteling-constants.js');

exports.removeNonUpdatFields = (object, updatedPayload) => {
  switch (object) {
    case 'customer':
      const keysToExclude = clientelingConstants.COMMERCE_OBJECTS_EXCLUSION_FIELDS.OCAPI.ACCOUNT;
      return _.omit(updatedPayload, keysToExclude);
    default:
      return updatedPayload;
  }
};

exports.exportObjectsToDb = (object, rawPayload, params, req) => {
  switch (object) {
    case 'order':
      const orderSchema = clientelingConstants.COMMERCE_OBJECTS_SCHEMA.OCAPI.ORDER;
      const orderObject = ocapiObjectConverterHelper.convertOCAPIObjectOnUnifyObjects(
        object,
        rawPayload,
        params,
      );
      const innerParams = {
        order_id: orderObject[orderSchema.order_token.outputFieldName],
        currencyCode: orderObject[orderSchema.currency.outputFieldName],
        basePriceBook: params.basePriceBook,
      };
      const products = rawPayload.product_items;
      const orderItemsArray = ocapiObjectConverterHelper.convertOCAPIObjectOnUnifyObjects(
        'order_item',
        products,
        innerParams,
      );
      return { order: orderObject, order_items: orderItemsArray };
    case 'customer':
      const accountObject = ocapiObjectConverterHelper.convertOCAPIObjectOnUnifyObjects(
        object,
        rawPayload,
        params,
      );
      // Send to Heroku DB
      return { account: accountObject };
  }
};

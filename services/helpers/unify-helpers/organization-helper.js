const _ = require('lodash');
const BusinessGenericModel = require('../../../models/business-generic-model.js');
const RedisService = require('../../../services/redis-service');
const CollectionName = require('../../../constants/collection-name');

exports.retrieveOrganizationConfig = async (userAccount) => {
  try {
    const retrievedFromRedis = await RedisService.get(userAccount);
    if (!_.isEmpty(retrievedFromRedis) && !_.isEmpty(retrievedFromRedis.data) && retrievedFromRedis.data.length > 0) {
      return JSON.parse(retrievedFromRedis.data);
    }
    console.log('Account Organization not found on Redis, pulling from MongoDB...');
    const query = { accountId: userAccount };
    const organizationInfo = await BusinessGenericModel.findOne(CollectionName.ORGANIZATIONS_INFO, query);
    if (_.isEmpty(organizationInfo)) {
      throw new Error('organization.configuration.not.found');
    } else {
      RedisService.set(userAccount, organizationInfo, 'EX', 720);
      return organizationInfo;
    }
  } catch (e) {
    console.error('Error on retriving organization configuration!');
    console.error(e);
    return undefined;
  }
};

/**
 * Save Organization Information in RedisDB
 * @param {object} infoOrganization
 * @example
 * {
      "_id" : 5babd034adc2215c50ec525A,
      "accountId" : "admin@osf-global.com",
      "name" : "OSF Sportico Company",
      "isActive" : true,
      "createdAt" : "2018-08-30T13:52:17.284Z",
      "clientelingConfigurations" : {
          "integrationLibs" : [
              {
                  "id" : "unify-lib-commerce-cloud",
                  "type" : "commerce",
                  "credentials" : {
                      "apiIdentifier" : "sfcc.ocapi",
                      "userId" : "stefan.ioachim",
                      "userPassword" : "",
                      "clientId" : "",
                      "clientPassword" : "",
                      "baseUrl" : " https://osfglobalservices14-alliance-prtnr-na03-dw.demandware.net",
                      "version" : "v18_8",
                      "siteUrl" : " https://osfglobalservices14-alliance-prtnr-na03-dw.demandware.net/s/Sites-Sportful-Site/dw",
                      "basePriceBookValue" : "",
                      "accountRecordType" : "",
                      "siteId" : "Sites-Sportiful-Site",
                      "storeName" : "OSF Sportiquo Store"
                  },
                  "allowedObjects" : {
                      "commerce" : [
                          {
                              "object_id" : "commerce-cloud",
                              "methods" : [
                                  "POST",
                                  "GET",
                                  "PUT"
                              ]
                          }
                      ]
                  }
              }
          ]
      }
    }
 */
exports.saveOrganizationInfoInRedis = async (infoOrganization) => {
  try {
    const accountId = infoOrganization.accountId;
    const result = await RedisService
      .set(`${accountId}_info_organization`, infoOrganization)
      .catch(error => error);
    return result;
  } catch (error) {
    console.log('it was not possible save information in Redis DB');
    return null;
  }
};

/**
 * Get Organization Information from RedisDB
 * @param {string} accountId
 */
exports.getOrganizationInfoFromRedis = async (accountId) => {
  try {
    const result = await RedisService
      .get(`${accountId}_info_organization`)
      .catch(error => error);
    return result;
  } catch (error) {
    console.log('it was not possible get information from Redis DB');
    return null;
  }
};

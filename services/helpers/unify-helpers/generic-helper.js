const _ = require('lodash');
const ProductConstant = require('../../commerce/product/product-constant');
const WishlistItemConverter = require('../../commerce/wishlist-item/wishlist-item-converter');
const AuthModel = require('../../../models/auth-model');

class GenericHelper {
  static transformParamsInFieldValuesArray(params = {}) {
    const fields = [];
    const values = [];

    Object.keys(params).forEach((key) => {
      fields.push(key);
      values.push(params[key]);
    });

    return {
      fields,
      values,
    };
  }

  static isSuccess(status) {
    return status >= 200 && status <= 304;
  }

  static isEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  static removeAttributesNull(allData) {
    try {
      return _.map(allData, (data) => {
        let newObj = {};
        for (const key in data) {
          if (data[key]) {
            newObj = { ...newObj, ...{ [key]: data[key] } };
          }
        }
        return newObj;
      });
    } catch (error) {
      return {};
    }
  }

  static getImages(Converters, obj, retrievedCustomer, targetToMix) {
    let data = null;
    if (Array.isArray(retrievedCustomer.data)) {
      data = retrievedCustomer.data;
    } else {
      data = [retrievedCustomer.data];
    }
    const images = WishlistItemConverter.getImagesDataMapToCore(obj, data);
    const imagesReplaced = Converters.replaceFields(images.data, images.map);

    return _.map(targetToMix, (d, index) => {
      return { ...d, images: { ...imagesReplaced[index] } };
    });
  }

  static getProductSearchImage(fieldsCore, hit) {
    if (hit && hit.image) {
      return {
        images: [{
          largeImages: {
            [fieldsCore.imageSearch.type]: hit.image[ProductConstant.imageSearchField._type],
            [fieldsCore.imageSearch.alt]: hit.image[ProductConstant.imageSearchField.alt],
            [fieldsCore.imageSearch.url]: hit.image[ProductConstant.imageSearchField.dis_base_link],
            [fieldsCore.imageSearch.link]: hit.image[ProductConstant.imageSearchField.link],
            [fieldsCore.imageSearch.title]: hit.image[ProductConstant.imageSearchField.title],
          }
        }]
      };
    }
    return {
      images: [],
    };
  }

  static getImageSizes(image_groups) {
    return _.map(image_groups, group => {
      var largeImages = _.find(group, { view_type: 'large' });
      var mediumImages = _.find(group, { view_type: 'medium' });
      var smallImages = _.find(group, { view_type: 'small' });
      var swatchImages = _.find(group, { view_type: 'swatch' });
      return {
        largeImages: (largeImages) ? largeImages.images : [],
        mediumImages: (mediumImages) ? mediumImages.images : [],
        smallImages: (smallImages) ? smallImages.images : [],
        swatchImages: (swatchImages) ? swatchImages.images : [],
      };
    });
  }

  static getFormattedImages(obj, rawData) {
    const { replaceFields } = obj.privateLibraries.Converters;

    var products = _.map(rawData, value => {
      return value.product || value;
    })

    var image_groups = _.map(products, product => {
      return product.image_groups;
    });

    // By the moment, we just have only large images coming back to client-side
    var images = this.getImageSizes(image_groups);

    const nestedMap = {
      list: 'list',
      item: {
        url: 'dis_base_link',
        alt: 'alt',
        link: 'link',
        title: 'title',
      },
    };

    const map = {
      list: 'data',
      item: {
        largeImages: 'largeImages',
        mediumImages: 'mediumImages',
        smallImages: 'smallImages',
        swatchImages: 'swatchImages',
      },
      operate: [{
        on: 'largeImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }, {
        on: 'mediumImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }, {
        on: 'smallImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }, {
        on: 'swatchImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }],
    };

    const data = {
      'data': images,
    };

    return {
      map,
      data,
    };
  }

  static getVariations(obj, rawData) {

    var variants = _.map(rawData.variants, value => {
      return {
        link: value.link,
        orderable: value.orderable,
        price: value.price,
        productId: value.product_id,
        variationValues: value.variation_values,
      };
    });

    var variationsAttributes = _.map(rawData.variation_attributes, value => {
      return {
        id: value.id,
        name: value.name,
        type: value._type,
        values: value.values.map(v => {
          return {
            type: v._type,
            name: v.name,
            orderable: v.orderable,
            value: v.value,
          }
        })
      };
    });

    return {
      variants,
      variationsAttributes,
    };
  }

  static getPromotions(fieldsCore, promotions) {
    const promotionsFormatted = _.map(promotions, value => {
      return {
        [fieldsCore.promotion.type]: value._type,
        [fieldsCore.promotion.message]: value.callout_msg,
        [fieldsCore.promotion.link]: value.link,
        [fieldsCore.promotion.promotionId]: value.promotion_id,
        [fieldsCore.promotion.promotionalPrice]: value.promotional_price,
      };
    });

    return {
      promotions: promotionsFormatted,
    };
  }

  static getInvetory(fieldsCore, inventory) {
    return {
      [fieldsCore.inventory.type]: inventory._type,
      [fieldsCore.inventory.stockLevel]: inventory.stock_level,
      [fieldsCore.inventory.id]: inventory.id,
      [fieldsCore.inventory.ats]: inventory.ats,
      [fieldsCore.inventory.orderable]: inventory.orderable,
      [fieldsCore.inventory.preorderable]: inventory.preorderable,
      [fieldsCore.inventory.backorderable]: inventory.backorderable,
    };
  }

  static getBMTokenFormatted(BMToken) {
    return { token: BMToken };
  }

  static async performRefreshToken(obj) {
    if (obj.status === 401) {
      const newCurrentLib = await AuthModel.doLogin(obj.currentLib);
      obj.PerformRefreshToken(newCurrentLib, obj.tokenValueBearer || obj.infoOrganization.tokenValue);
      return await obj.callback(newCurrentLib);
    }
    return null;
  }

  static getShipingAddressesFromShipments(fieldsCore, shipments) {
    if (shipments && shipments.length) {
      return _.map(shipments, shipment => {
        return {
          [fieldsCore.shippingAddress.street]: shipment.shipping_address.address1,
          [fieldsCore.shippingAddress.city]: shipment.shipping_address.city,
          [fieldsCore.shippingAddress.state]: shipment.shipping_address.state_code,
          [fieldsCore.shippingAddress.type]: shipment.shipping_address._type,
          [fieldsCore.shippingAddress.country]: shipment.shipping_address.country_code,
          [fieldsCore.shippingAddress.id]: shipment.shipping_address.id,
          [fieldsCore.shippingAddress.firstName]: shipment.shipping_address.first_name,
          [fieldsCore.shippingAddress.lastName]: shipment.shipping_address.last_name,
          [fieldsCore.shippingAddress.phone]: shipment.shipping_address.phone,
          [fieldsCore.shippingAddress.zipCode]: shipment.shipping_address.postal_code,
        };
      });
    }
    return [];
  }

  /**
   * Need iterate here to get the eventType="custom_1" and eventTypeValue="OSFUC_CAB_"
   */
  static getAbandonedCarts(data) {
    return data.filter(d => {
      return d.eventType === 'custom_1' && d.eventTypeValue === 'OSFUC_CAB_';
    });
  }

  static async performDataRefreshToken(obj) {
    if (obj.status === 401) {
      const dataToken = await AuthModel.getDataApiLogin(obj.currentLib.credentials);
      const newCurrentLib = {
        ...obj.currentLib,
        credentials: {
          ...obj.currentLib.credentials,
          DataToken: `Bearer ${dataToken.data.access_token}`,
        },
      };
      obj.PerformRefreshToken(newCurrentLib, obj.tokenValueBearer || obj.infoOrganization.tokenValue);
      return await obj.callback(newCurrentLib);
    }
    return null;
  }
}

module.exports = GenericHelper;

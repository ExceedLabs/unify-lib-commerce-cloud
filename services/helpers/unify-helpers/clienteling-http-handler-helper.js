const axios = require('axios');
const _ = require('lodash');
const clientelingConstants = require('../../../constants/clienteling-constants.js');
const apiIdentifiersConstants = require('../../../constants/api-identifiers-constants.js');

const mountRequiredApiHeader = (apiIdentifier, params) => {
  switch (apiIdentifier) {
    case apiIdentifiersConstants.sfcc_ocapi:
      return ({
        Authorization: `${params.token}`,
        'Content-Type': 'application/json',
      });
    default:
      throw ({ status: 501, error: 'api.not.implement', message: 'The current platform version does not support this api' });
  }
};

exports.makeHttpRequest = (apiIdentifier, finalUrl, method, params, body) => {
  const headers = mountRequiredApiHeader(apiIdentifier, params);
  const allowedMethods = clientelingConstants.ALLOWED_OPERATIONS_METHODS;
  if (method == allowedMethods.GET || method == allowedMethods.DELETE) {
    return axios(
      {
        method,
        url: finalUrl,
        headers,
      },
    );
  }
  return axios(
    {
      method,
      url: finalUrl,
      headers,
      data: body,
    },
  );
};

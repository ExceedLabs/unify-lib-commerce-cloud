const _ = require('lodash');

class ImageHelper {
    static getImageSizes(image_groups) {
        return _.map(image_groups, group => {
          var largeImages = _.find(group, { view_type: 'large' });
          var mediumImages = _.find(group, { view_type: 'medium' });
          var smallImages = _.find(group, { view_type: 'small' });
          var swatchImages = _.find(group, { view_type: 'swatch' });
          return { 
            largeImages: largeImages.images,
            mediumImages: mediumImages.images,
            smallImages: smallImages.images,
            swatchImages: swatchImages.images,
          };
        });
      }

      static getImages(Converters, obj, retrievedCustomer, targetToMix, WishlistItemConverter) {
        let data = null;
        if (Array.isArray(retrievedCustomer.data)) {
          data = retrievedCustomer.data;
        } else {
          data = [retrievedCustomer.data];
        }
        const images = WishlistItemConverter.getImagesDataMapToCore(obj, data);
        const imagesReplaced = Converters.replaceFields(images.data, images.map);
    
        return _.map(targetToMix, (d, index) => {
          if  ( d.product.lenght > 0) {
            d.product[0].images = imagesReplaced[index];
            delete d.product[0].imageGroups;
          }
            return d;
        });
      }
}

module.exports = ImageHelper;
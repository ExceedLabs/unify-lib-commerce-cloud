const _ = require('lodash');
const FieldsConstant = require('./customer-constant');
const FieldsConstantAddress = require('../commerce/address/address-constant');
const AbandonedCartFieldsConstant = require('../commerce/abandoned-cart/abandoned-cart-constant');

const FIELD_CUSTOMER = FieldsConstant.fields;
const FIELD_ADDRESS = FieldsConstantAddress.fields;

const ABANDONED_CART = AbandonedCartFieldsConstant.cartFields;
const ABANDONED_CART_ITEM = AbandonedCartFieldsConstant.cartItemFields;

class CustomerMapping {
  static getCustomerFields(fieldsCore) {
    return {
      [FIELD_CUSTOMER.salutation]: fieldsCore.salutation,
      [FIELD_CUSTOMER.email]: fieldsCore.email,
      [FIELD_CUSTOMER.login]: fieldsCore.email, // it was intencionally used here
      [FIELD_CUSTOMER.birthday]: fieldsCore.birthday,
      [FIELD_CUSTOMER.creation_date]: fieldsCore.createdAt,
      [FIELD_CUSTOMER.customer_id]: fieldsCore.customerId,
      [FIELD_CUSTOMER.customer_no]: fieldsCore.customerNumber,
      [FIELD_CUSTOMER.first_name]: fieldsCore.firstName,
      [FIELD_CUSTOMER.last_name]: fieldsCore.lastName,
      [FIELD_CUSTOMER.gender]: fieldsCore.gender,
      [FIELD_CUSTOMER.phone_business]: fieldsCore.phoneBusiness,
      [FIELD_CUSTOMER.phone_home]: fieldsCore.phoneHome,
      [FIELD_CUSTOMER.phone_mobile]: fieldsCore.phoneMobile,
    };
  }

  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.salutation]: FIELD_CUSTOMER.salutation,
      [fieldsCore.email]: FIELD_CUSTOMER.email,
      [fieldsCore.birthday]: FIELD_CUSTOMER.birthday,
      [fieldsCore.createdAt]: FIELD_CUSTOMER.creation_date,
      [fieldsCore.customerId]: FIELD_CUSTOMER.customer_id,
      [fieldsCore.customerNumber]: FIELD_CUSTOMER.customer_no,
      [fieldsCore.firstName]: FIELD_CUSTOMER.first_name,
      [fieldsCore.lastName]: FIELD_CUSTOMER.last_name,
      [fieldsCore.gender]: FIELD_CUSTOMER.gender,
      [fieldsCore.phoneBusiness]: FIELD_CUSTOMER.phone_business,
      [fieldsCore.phoneHome]: FIELD_CUSTOMER.phone_home,
      [fieldsCore.phoneMobile]: FIELD_CUSTOMER.phone_mobile,
    };
  }

  static getAddressCustomerFields(fieldsCore) {
    return {
      [FIELD_ADDRESS._type]: fieldsCore.typeAddress,
      [FIELD_ADDRESS.address1]: fieldsCore.street,
      [FIELD_ADDRESS.city]: fieldsCore.city,
      [FIELD_ADDRESS.country_code]: fieldsCore.country,
      [FIELD_ADDRESS.first_name]: fieldsCore.firstName,
      [FIELD_ADDRESS.last_name]: fieldsCore.lastName,
      [FIELD_ADDRESS.phone]: fieldsCore.phone,
      [FIELD_ADDRESS.postal_code]: fieldsCore.zipCode,
      [FIELD_ADDRESS.post_box]: fieldsCore.postBox,
      [FIELD_ADDRESS.state_code]: fieldsCore.stateCode,
      [FIELD_ADDRESS.preferred]: fieldsCore.preferred,
      [FIELD_ADDRESS.address_id]: fieldsCore.id,
    };
  }

  static getAddressCoreFields(fieldsCore) {
    return {
      [fieldsCore.typeAddress]: FIELD_ADDRESS._type,
      [fieldsCore.street]: FIELD_ADDRESS.address1,
      [fieldsCore.city]: FIELD_ADDRESS.city,
      [fieldsCore.stateCode]: FIELD_ADDRESS.state_code,
      [fieldsCore.zipCode]: FIELD_ADDRESS.postal_code,
      [fieldsCore.postBox]: FIELD_ADDRESS.post_box,
      [fieldsCore.phone]: FIELD_ADDRESS.phone,
      [fieldsCore.firstName]: FIELD_ADDRESS.first_name,
      [fieldsCore.lastName]: FIELD_ADDRESS.last_name,
      [fieldsCore.country]: FIELD_ADDRESS.country_code,
      [fieldsCore.preferred]: FIELD_ADDRESS.preferred,
      [fieldsCore.id]: FIELD_ADDRESS.address_id,
    };
  }

  static getAbandonedCartFields(fieldsCore) {
    return {
      [fieldsCore.abandonedCartId]: ABANDONED_CART.id,
      [fieldsCore.name]: ABANDONED_CART.name,
      [fieldsCore.lastModified]: ABANDONED_CART.last_modified,
      [fieldsCore.creationDate]: ABANDONED_CART.creation_date,
      [fieldsCore.currencyCode]: ABANDONED_CART.currencyCode,
      [fieldsCore.total]: ABANDONED_CART.total,
      [fieldsCore.itemsNumber]: ABANDONED_CART.itemsNumber,
      [fieldsCore.items]: ABANDONED_CART.customer_product_list_items,
    };
  }

  static getAbandonedCartItemsFields(fieldsCore) {
    return {
      [fieldsCore.abandonedCartItemId]: ABANDONED_CART_ITEM.id,
      [fieldsCore.sku]: ABANDONED_CART_ITEM.product_id,
      [fieldsCore.unitPrice]: ABANDONED_CART_ITEM.price,
      [fieldsCore.name]: ABANDONED_CART_ITEM.name,
      [fieldsCore.shortDescription]: ABANDONED_CART_ITEM.short_description,
      [fieldsCore.longDescription]: ABANDONED_CART_ITEM.long_description,
      [fieldsCore.quantity]: ABANDONED_CART_ITEM.quantity,
      [fieldsCore.images]: ABANDONED_CART_ITEM.images,
    };
  }
}

module.exports = CustomerMapping;

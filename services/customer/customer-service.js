const axios = require('axios');
const _ = require('lodash');
const CustomerEndpoint = require('./customer-endpoint');
const CustomerConstant = require('./customer-constant');

class CustomerService {
  static getHitsFromCustomerData(customerData) {
    try {
      const hasData = customerData && customerData.data;
      const hasHits = customerData.data.hits && customerData.data.hits.length;
      if (hasData && hasHits) {
        const hitsCustomerData = _.map(customerData.data.hits, hit => hit.data);
        return { status: customerData.status, data: hitsCustomerData };
      }
      return customerData;
    } catch (error) {
      return error;
    }
  }

  static async getAllCustomers(currentLib, tokenObj, params) {
    try {
      if (params && !params.customerId) {
        const { baseUrl, version, siteId } = currentLib.credentials;
        const URL = `${baseUrl}/s/-/dw/data/${version}/customer_lists/${siteId}/customer_search`;
        return axios({
          method: 'POST',
          url: URL,
          data: {
            query: {
              text_query: {
                fields: ['email', 'first_name', 'last_name', 'phone_home', 'phone_business', 'phone_mobile'],
                search_phrase: params.email || params.firstName || params.phone,
              },
            },
            select: '(**)',
          },
          headers: {
            Authorization: `${tokenObj.token}`,
            'Content-Type': 'application/json',
          },
        });
      }
      return params.customerId;
    } catch (error) {
      throw (error);
    }
  }

  static async getCustomer(currentLib, tokenObj) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.get,
      tokenObj.customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async createCustomer(currentLib, tokenObj, payload) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.post,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async updateCustomer(currentLib, tokenObj, payloadObj) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.patch,
      payloadObj.customerId,
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: payloadObj.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async createCustomerAddress(currentLib, tokenObj, payload) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.postAddress,
      tokenObj.customerId,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });

  }

  static async createCustomerPaymentInstruments(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.postPaymentInstruments,
      params.customerId,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });

  }

  static async createCustomerWishlists(currentLib, tokenObj, customerId, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.postCustomerWishlist,
      customerId,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });

  }

  static async createCustomerWishlistItems(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.postWishlistItem,
      customerId,
      params
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async createCustomerPurchases(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.postWishlistPurchase,
      customerId,
      params
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async deleteCustomerWishlistItemById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.deleteWishlistItemById,
      customerId,
      params
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async deleteCustomerAddressById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.deleteAddressByName,
      customerId,
      params
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async deleteCustomerWishlistsById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.deleteCustomerWishlistsById,
      customerId,
      params
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async deleteCustomerPaymentInstruments(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.deleteCustomerPaymentInstruments,
      customerId,
      params
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async getCustomerAddress(currentLib, tokenObj) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getAddress,
      tokenObj.customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerAddressesById(currentLib, tokenObj, customerId, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getAddressById,
      customerId,
      params,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerBaskets(currentLib, tokenObj, customerId) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getBaskets,
      customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerOrders(currentLib, tokenObj, customerId) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getOrders,
      customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async putCustomerBasket(currentLib, tokenObj, customerId) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.putCustomerBasket,
      customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerPaymentInstruments(currentLib, tokenObj, customerId) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getCustomerPaymentInstruments,
      customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerPurchases(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getCustomerPurchases,
      customerId,
      params,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerWishlists(currentLib, tokenObj, customerId) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getCustomerWishlists,
      customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerWishlistsById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getCustomerWishlistsById,
      customerId,
      params,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerWishlistItems(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getCustomerWishlistItems,
      customerId,
      params
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async listCustomerWishlistItemsById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getCustomerWishlistItemsById,
      customerId,
      params
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async updateCustomerWishlistById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.updateCustomerWishlistById,
      customerId,
      params
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async updateCustomerWishlistItemsById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.updateCustomerWishlistItemsById,
      customerId,
      params
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async updateCustomerAddressById(currentLib, tokenObj, customerId, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.updateCustomerAddressById,
      customerId,
      params
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async retrieveAbandonedCarts(currentLib, tokenObj, customerId, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CustomerEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CustomerConstant.methods.getAbandonedCarts,
      customerId,
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
      },
    });
  }

  static isSuccess(status) {
    return status >= 200 && status <= 304;
  }

  static removeAttributesNull(allData) {
    try {
      return _.map(allData, (data) => {
        let newObj = {};
        for (const key in data) {
          if (data[key] !== null) {
            newObj = { ...newObj, ...{ [key]: data[key] } };
          }
        }
        return newObj;
      });
    } catch (error) {
      return {};
    }
  }
}

module.exports = CustomerService;

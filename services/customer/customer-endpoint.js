const { methods } = require('./customer-constant');

class CustomerEndpoints {
  static retrieveEndpoint(version, method, customerId, params) {
    switch (method) {
      case methods.get:
        return `/shop/${version}/customers/${customerId}`;
      case methods.postAddress:
        return `/shop/${version}/customers/${customerId}/addresses`;
      case methods.getAddress:
        return `/shop/${version}/customers/${customerId}/addresses`;
      case methods.getAddressById:
        return `/shop/${version}/customers/${customerId}/addresses/${params.id}`;
      case methods.post:
        return `/shop/${version}/customers`;
      case methods.patch:
        return `/shop/${version}/customers/${customerId}`;
      case methods.getBaskets:
        return `/shop/${version}/customers/${customerId}/baskets`;
      case methods.getOrders:
        return `/shop/${version}/customers/${customerId}/orders`;
      case methods.getCustomerPaymentInstruments:
        return `/shop/${version}/customers/${customerId}/payment_instruments`;
      case methods.postPaymentInstruments:
        return `/shop/${version}/customers/${customerId}/payment_instruments`;
      case methods.getCustomerPurchases:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items/${params.itemId}/purchases`;
      case methods.getCustomerWishlists:
        return `/shop/${version}/customers/${customerId}/product_lists?expand=product,images,availability`;
      case methods.getAbandonedCarts:
        return `/shop/${version}/customers/${customerId}/product_lists?expand=images,items,product,availability,prices`;
      case methods.getCustomerWishlistsById:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}?expand=product,images,availability`;
      case methods.getCustomerWishlistItems:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items?expand=product,images,availability,prices`;
      case methods.postCustomerWishlist:
        return `/shop/${version}/customers/${customerId}/product_lists`;
      case methods.postWishlistItem:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items`;
      case methods.deleteWishlistItemById:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items/${params.itemId}`;
      case methods.deleteCustomerPaymentInstruments:
        return `/shop/${version}/customers/${customerId}/payment_instruments/${params.id}`;
      case methods.deleteAddressByName:
        return `/shop/${version}/customers/${customerId}/addresses/${params.id}`;
      case methods.deleteCustomerWishlistsById:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}`;
      case methods.getCustomerWishlistItemsById:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items/${params.itemId}?expand=product,images,availability,prices`;
      case methods.updateCustomerWishlistById:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}`;
      case methods.updateCustomerWishlistItemsById:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items/${params.itemId}`;
      case methods.updateCustomerAddressById:
        return `/shop/${version}/customers/${customerId}/addresses/${params.id}`;
      case methods.postWishlistPurchase:
        return `/shop/${version}/customers/${customerId}/product_lists/${params.id}/items/${params.itemId}/purchases`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = CustomerEndpoints;

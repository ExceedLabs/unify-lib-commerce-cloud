const _ = require('lodash');
const CustomerMapping = require('./customer-mapping');
const CustomerConstants = require('./customer-constant');
const AbandonedCartConstants = require('../commerce/abandoned-cart/abandoned-cart-constant');

class CustomerConverter {
  static applyAbandonedCartItemsPreMapping(productsObj) {
    const retrieveUniqueProducts = _.uniq(productsObj.map(item => {return item.product_id}));
    const productsWithoutDuplicates = retrieveUniqueProducts.map( sku => {
      return productsObj.find(product => {
        return product.product_id === sku;
      });
    });
    const productsRemapped = productsWithoutDuplicates.map(sfccProduct => {
      const productMapping = AbandonedCartConstants.preMappingFields.itemFields;
      const imageMapping = AbandonedCartConstants.preMappingFields.imagesFields;
      const linksObjMapping = AbandonedCartConstants.preMappingFields.imageLinkFields;
      const linkObjKeys = Object.keys(linksObjMapping);
      const imagesObj = sfccProduct.product.image_groups.map(image => {
        const linkObjMapping = image.images.map(link => {
          return linkObjKeys.map( key => {
            return {[key]: link[linksObjMapping[key]]}
          }).reduce((acc, cur) => ({...acc, ...cur}));
        });
        return {
          links: linkObjMapping,
          view_type: image[imageMapping.view_type],
        };
      });
      return Object.keys(productMapping).map(key => {
        if(key === 'images') {
          return { [key]: imagesObj };
        } else {
          return { [key]: Object.byString(sfccProduct, productMapping[key]) };
        }
      }).reduce((acc, cur) => ({...acc, ...cur}));
    });
    return productsRemapped;
  }

  static applyAbandonedCartPreMapping(cartObj) {
    const cartTotal = cartObj.customer_product_list_items.map(item => (item.product.price * item.quantity))
    .reduce((acc, cur) => (acc + cur));
    const cartProducts = this.applyAbandonedCartItemsPreMapping(cartObj.customer_product_list_items);
    const cartItemsTotal = cartProducts.length;
    const cartMapping = AbandonedCartConstants.preMappingFields.cartFields;
    const auxObj = Object.keys(cartMapping).map(key => ({
      [key]: Object.byString(cartObj, cartMapping[key])
    })).reduce((acc, cur) => ({...acc, ...cur}));
    return {
      ...auxObj,
      total: cartTotal,
      itemsNumber: cartItemsTotal,
      customer_product_list_items: cartProducts
    };
  }

  static checkGender(val) {
    if (typeof val !== 'undefined' && val === 0) {
      return 'M';
    }
    if (typeof val !== 'undefined' && val !== 0) {
      return 'F';
    }
    return null;
  }

  static getDataMapToCore(obj, customerData) {
    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const libObjId = obj.libObjId;

      const map = {
        list: libObjId,
        item: {
          ...CustomerMapping.getCoreFields(fieldsCore),
        },
        operate: [{
          on: CustomerConstants.fields.gender,
          run: val => this.checkGender(val),
        }],
      };

      const data = {
        [libObjId]: customerData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      throw ({
        status: 500, error, message: 'error.replace.customer.fields', service: 'conversor-fields-service.js',
      });
    }
  }

  static getDataMapToCustomer(obj, customerData) {
    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const libObjId = obj.libObjId;

      const map = {
        list: libObjId,
        item: {
          ...CustomerMapping.getCustomerFields(fieldsCore),
        },
        operate: [{
          on: CustomerConstants.fields.gender,
          run: (val) => {
            if (val === null) { return null; }
            return (val === 'M') ? 0 : 1;
          },
        }],
      };

      const data = {
        [libObjId]: customerData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      throw (error);
    }
  }

  static getAddressDataMapToCustomer(obj, customerData) {
    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const libObjId = obj.libObjId;

      const map = {
        list: libObjId,
        item: {
          ...CustomerMapping.getAddressCustomerFields(fieldsCore),
        },
      };

      const data = {
        [libObjId]: customerData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      throw ({
        status: 500, error, message: 'error.replace.customer.fields', service: 'conversor-fields-service.js',
      });
    }
  }

  static getAddressDataMapToCore(obj, customerData) {
    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const libObjId = obj.libObjId;

      const map = {
        list: libObjId,
        item: {
          ...CustomerMapping.getAddressCoreFields(fieldsCore),
        },
      };

      const data = {
        [libObjId]: customerData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      throw ({
        status: 500, error, message: 'error.replace.customer.fields', service: 'conversor-fields-service.js',
      });
    }
  }

  static getAbandonedCartMapToCore(obj, abandonedCartData) {
    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const libObjId = obj.libObjId;

      const map = {
        list: libObjId,
        item: {
          ...CustomerMapping.getAbandonedCartFields(fieldsCore),
        },
      };

      const data = {
        [libObjId]: abandonedCartData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      throw ({
        status: 500, error, message: 'error.replace.customer.fields', service: 'conversor-fields-service.js',
      });
    }
  }
}

module.exports = CustomerConverter;

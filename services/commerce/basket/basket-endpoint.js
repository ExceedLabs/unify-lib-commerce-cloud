const { methods } = require('./basket-constant');

class BasketEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.post:
        return `/shop/${version}/baskets`;
      case methods.get:
        return `/shop/${version}/baskets`;
      case methods.getById:
        return `/shop/${version}/baskets/${params.basketId}`;
      case methods.getShipments:
        return `/shop/${version}/baskets/${params.basketId}/shipments`;
      case methods.getShippingMethods:
        return `/shop/${version}/baskets/${params.basketId}/shipments/${params.shipmentId}/shipping_methods`;
      case methods.postItem:
        return `/shop/${version}/baskets/${params.basketId}/items`;
      case methods.putBillingAddress:
        return `/shop/${version}/baskets/${params.basketId}/billing_address`;
      case methods.postPaymentInstrument:
        return `/shop/${version}/baskets/${params.basketId}/payment_instruments`;
      case methods.postShipment:
        return `/shop/${version}/baskets/${params.basketId}/shipments`;
      case methods.getBasketPaymentMethod:
        return `/shop/${version}/baskets/${params.basketId}/payment_methods`;
      case methods.putShipmentMethodToShipment:
        return `/shop/${version}/baskets/${params.basketId}/shipments/${params.shipmentId}/shipping_method`;
      case methods.putShipmentAddressToShipment:
        return `/shop/${version}/baskets/${params.basketId}/shipments/${params.shipmentId}/shipping_address?use_as_billing=${params.useAsBilling}`;
      case methods.putBasketCustomer:
        return `/shop/${version}/baskets/${params.basketId}/customer`;
      case methods.putBasketShipment:
        return `/shop/${version}/baskets/${params.basketId}/shipments/${params.shipmentId}`;
      case methods.putBasketProductItem:
        return `/shop/${version}/baskets/${params.basketId}/items/${params.itemId}`;
      case methods.putBasketPaymentInstrumentById:
        return `/shop/${version}/baskets/${params.basketId}/payment_instruments/${params.paymentInstrumentId}`;
      case methods.delete:
        return `/shop/${version}/baskets/${params.basketId}`;
      case methods.deleteProductItem:
        return `/shop/${version}/baskets/${params.basketId}/items/${params.itemId}`;
      case methods.deleteShipment:
        return `/shop/${version}/baskets/${params.basketId}/shipments/${params.shipmentId}`;
      case methods.deletePaymentInstrument:
        return `/shop/${version}/baskets/${params.basketId}/payment_instruments/${params.paymentInstrumentId}`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = BasketEndpoints;

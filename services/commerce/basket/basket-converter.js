const BasketMapping = require('./basket-mapping');

class BasketConverter {
  static getDataMapToCore(obj, basketData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...BasketMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: basketData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToCategory(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...BasketMapping.getCategoryFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = BasketConverter;

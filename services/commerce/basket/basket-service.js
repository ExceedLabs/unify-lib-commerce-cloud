const axios = require('axios');
const GenericHelper = require('../../helpers/unify-helpers/generic-helper');
const BasketConstants = require('./basket-constant');
const BasketEndpoint = require('./basket-endpoint');

class BasketService {
  static createBasket(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.post,
      { siteId, ...params },
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listBasket(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.get,
      { siteId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listBasketById(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.getById,
      { siteId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listBasketShipments(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.getShipments,
      { siteId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listBasketShippingMethods(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.getShippingMethods,
      { siteId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static createBasketProductItem(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.postItem,
      { siteId, ...params },
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static updateBasketBillingAddress(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putBillingAddress,
      { siteId, ...params },
    );

    return axios({
      method: 'put',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static createBasketPaymentInstrument(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.postPaymentInstrument,
      { siteId, ...params },
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static createBasketShipment(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.postShipment,
      { siteId, ...params },
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static putBasketShipmentMethodToShipment(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putShipmentMethodToShipment,
      { siteId, ...params },
    );

    return axios({
      method: 'put',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static putBasketShipmentAddressToShipment(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putShipmentAddressToShipment,
      { siteId, ...params },
    );

    return axios({
      method: 'put',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static getBasketPaymentMethod(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.getBasketPaymentMethod,
      { siteId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static putBasketCustomer(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putBasketCustomer,
      { siteId, ...params },
    );

    return axios({
      method: 'put',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static putBasketShipment(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putBasketShipment,
      { siteId, ...params },
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static putBasketProductItem(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putBasketProductItem,
      { siteId, ...params },
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static putBasketPaymentInstrumentById(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.putBasketPaymentInstrumentById,
      { siteId, ...params },
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static removeBasket(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.delete,
      { siteId, ...params },
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static removeBasketShipment(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.deleteShipment,
      { siteId, ...params },
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static removeBasketItemById(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.deleteProductItem,
      { siteId, ...params },
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static removeBasketPaymentInstrumentById(currentLib, tokenObj, params) {
    const { siteUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + BasketEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      BasketConstants.methods.deletePaymentInstrument,
      { siteId, ...params },
    );

    return axios({
      method: 'delete',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }
}

module.exports = BasketService;

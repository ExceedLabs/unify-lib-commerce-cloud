const FieldsConstant = require('./basket-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class BasketMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.basketId]: FIELD_CATEGORY.basket_id,
      [fieldsCore.customerId]: FIELD_CATEGORY.customer_id,
      [fieldsCore.channelType]: FIELD_CATEGORY.channel_type,
      [fieldsCore.currency]: FIELD_CATEGORY.currency,
      [fieldsCore.shippingTotal]: FIELD_CATEGORY.shipping_total,
      [fieldsCore.shippingTotalTax]: FIELD_CATEGORY.shipping_total_tax,
      [fieldsCore.productSubTotal]: FIELD_CATEGORY.product_sub_total,
      [fieldsCore.productTotal]: FIELD_CATEGORY.product_total,
      [fieldsCore.taxTotal]: FIELD_CATEGORY.tax_total,
      [fieldsCore.orderTotal]: FIELD_CATEGORY.order_total,
      [fieldsCore.createdAt]: FIELD_CATEGORY.creation_date,
      [fieldsCore.updatedAt]: FIELD_CATEGORY.last_modified,
    };
  }

  static getCategoryFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.id]: fieldsCore.id,
      [FIELD_CATEGORY.name]: fieldsCore.name,
      [FIELD_CATEGORY.description]: fieldsCore.shortDescription,
      [FIELD_CATEGORY.catalog_id]: fieldsCore.catalogId,
      [FIELD_CATEGORY.online]: fieldsCore.isOnline,
      [FIELD_CATEGORY.link]: fieldsCore.link,
      [FIELD_CATEGORY.image]: fieldsCore.image,
      [FIELD_CATEGORY.thumbnail]: fieldsCore.thumbnail,
      [FIELD_CATEGORY.creation_date]: fieldsCore.createdAt,
    };
  }
}

module.exports = BasketMapping;

const FieldsConstant = require('./wishlist-constant');

const FIELD_WISHLIST = FieldsConstant.fields;

class WishlistMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_WISHLIST.type,
      [fieldsCore.name]: FIELD_WISHLIST.name,
      [fieldsCore.description]: FIELD_WISHLIST.description,
      [fieldsCore.link]: FIELD_WISHLIST.link,
      [fieldsCore.id]: FIELD_WISHLIST.id,
      [fieldsCore.eventType]: FIELD_WISHLIST.event_type,
      [fieldsCore.eventTypeValue]: FIELD_WISHLIST.event_type_value,
      [fieldsCore.isPublic]: FIELD_WISHLIST.public,
      [fieldsCore.createdAt]: FIELD_WISHLIST.creation_date,
      [fieldsCore.updatedAt]: FIELD_WISHLIST.last_modified,
    };
  }

  static getWishlistFields(fieldsCore) {
    return {
      [FIELD_WISHLIST.type]: fieldsCore.type,
      [FIELD_WISHLIST.name]: fieldsCore.name,
      [FIELD_WISHLIST.description]: fieldsCore.description,
      [FIELD_WISHLIST.link]: fieldsCore.link,
      [fieldsCore.id]: FIELD_WISHLIST.id,
      [fieldsCore.public]: FIELD_WISHLIST.isPublic,
      [fieldsCore.creation_date]: FIELD_WISHLIST.createdAt,
      [fieldsCore.last_modified]: FIELD_WISHLIST.updatedAt,
    };
  }
}

module.exports = WishlistMapping;

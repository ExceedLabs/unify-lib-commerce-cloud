const { methods } = require('./wishlist-constant');

class WishlistEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.get:
        return `/s/${params.siteId}/dw/shop/${version}/product_lists?email=${params.email}&client_id=${params.clientId}`;
      case methods.getById:
        return `/s/${params.siteId}/dw/shop/${version}/product_lists/${params.id}?client_id=${params.clientId}&expand=images,items,product,availability`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = WishlistEndpoints;

module.exports = {
  methods: {
    get: 'GET',
    post: 'POST',
    patch: 'PATCH',
    put: 'PUT',
    getById: 'GET_BY_ID',
  },
  fields: {
      type: 'type',
      title: 'title',
      name: 'name',
      description: 'description',
      link: 'link',
      id: 'id',
      event_type_value: 'event._type', // this represent a value type from event
      event_type: 'type', // this represent a type from event
      creation_date: 'creation_date',
      last_modified: 'last_modified',
      public: 'public',
      c_herokuLastModified: 'c_herokuLastModified',
      c_salesforceExported: 'c_salesforceExported',
      c_salesforceExternalId: 'c_salesforceExternalId',
      c_salesforceLastModified: 'c_salesforceLastModified',
  },
};

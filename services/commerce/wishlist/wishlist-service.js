const axios = require('axios');
const WishlistEndpoint = require('./wishlist-endpoint');
const WishlistConstant = require('./wishlist-constant');

class WishlistService {
  static listWishlist(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + WishlistEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      WishlistConstant.methods.get,
      { siteId, clientId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listWishlistById(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + WishlistEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      WishlistConstant.methods.getById,
      { siteId, clientId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }
}

module.exports = WishlistService;

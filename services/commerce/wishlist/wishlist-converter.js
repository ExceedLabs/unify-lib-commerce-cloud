const WishlistMapping = require('./wishlist-mapping');

class WishlistConverter {
  static getDataMapToCore(obj, wishlistData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...WishlistMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: wishlistData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToWishlist(obj, wishlistData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...WishlistMapping.getWishlistFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: wishlistData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = WishlistConverter;

const CatalogMapping = require('./catalog-mapping');

exports.getDataMapToCore = (obj, productData) => {
  const { fieldsCore } = obj.privateLibraries.Converters;
  const { libObjId } = obj;

  const map = {
    list: libObjId,
    item: {
      ...CatalogMapping.getCoreFields(fieldsCore),
    },
  };

  const data = {
    [libObjId]: productData,
  };

  return {
    map,
    data,
  };
};

exports.getDataMapToCategory = (obj, productData) => {
  const { fieldsCore } = obj.privateLibraries.Converters;
  const { libObjId } = obj;

  const map = {
    list: libObjId,
    item: {
      ...CatalogMapping.getCategoryFields(fieldsCore),
    },
  };

  const data = {
    [libObjId]: productData,
  };

  return {
    map,
    data,
  };
};

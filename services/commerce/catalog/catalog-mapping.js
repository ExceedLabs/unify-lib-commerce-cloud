const FieldsConstant = require('./catalog-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class CatalogMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.id]: FIELD_CATEGORY.id,
      [fieldsCore.name]: FIELD_CATEGORY.name,
      [fieldsCore.shortDescription]: FIELD_CATEGORY.description,
      [fieldsCore.catalogId]: FIELD_CATEGORY.catalog_id,
      [fieldsCore.isOnline]: FIELD_CATEGORY.online,
      [fieldsCore.link]: FIELD_CATEGORY.link,
      [fieldsCore.mediumImage]: FIELD_CATEGORY.image,
      [fieldsCore.smallImage]: FIELD_CATEGORY.thumbnail,
      [fieldsCore.createdAt]: FIELD_CATEGORY.creation_date,
    };
  }

  static getCategoryFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.id]: fieldsCore.id,
      [FIELD_CATEGORY.name]: fieldsCore.name,
      [FIELD_CATEGORY.description]: fieldsCore.shortDescription,
      [FIELD_CATEGORY.catalog_id]: fieldsCore.catalogId,
      [FIELD_CATEGORY.online]: fieldsCore.isOnline,
      [FIELD_CATEGORY.link]: fieldsCore.link,
      [FIELD_CATEGORY.image]: fieldsCore.mediumImage,
      [FIELD_CATEGORY.thumbnail]: fieldsCore.smallImage,
      [FIELD_CATEGORY.creation_date]: fieldsCore.createdAt,
    };
  }
}

module.exports = CatalogMapping;

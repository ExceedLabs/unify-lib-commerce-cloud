const axios = require('axios');
const GenericHelper = require('../../helpers/unify-helpers/generic-helper');
const CatalogConstants = require('./catalog-constant');
const CatalogEndpoint = require('./catalog-endpoint');

class CatalogService {
  static searchCatalog(currentLib, tokenObj, params) {
    const { baseUrl } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + CatalogEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CatalogConstants.methods.search,
      params,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: {
        query: {
          text_query: {
            fields: ['id', 'name'],
            search_phrase: params.q,
          },
        },
        select: '(**)',
      },
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static searchCatalogProduct(currentLib, tokenObj, params) {
    const { baseUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + CatalogEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CatalogConstants.methods.searchCatalogProduct,
      { siteId },
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: {
        query: {
          text_query: {
            fields: ['catalog_id'],
            search_phrase: params.catalogId,
          },
        },
        select: '(**)',
      },
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }
}

module.exports = CatalogService;

const { methods } = require('./catalog-constant');

class CatalogEndpoints {
  static retrieveEndpoint(version, method) {
    switch (method) {
      case methods.searchCatalogProduct:
        return `/s/-/dw/data/${version}/product_search`;
      case methods.search:
        return `/s/-/dw/data/${version}/catalog_search`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = CatalogEndpoints;

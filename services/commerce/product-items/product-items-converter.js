const _ = require('lodash');
const ProductItemsMapping = require('./product-items-mapping');

class ProductItemsConverter {
  static getDataMapToCore(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ProductItemsMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToProductItems(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ProductItemsMapping.getProductItemsFieldsToSave(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = ProductItemsConverter;

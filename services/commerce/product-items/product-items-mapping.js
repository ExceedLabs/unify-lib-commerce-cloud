const FieldsConstant = require('./product-items-constant');

const FIELDS = FieldsConstant.fields;

class ProductItemsMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELDS._type,
      [fieldsCore.price]: FIELDS.price,
      [fieldsCore.productId]: FIELDS.product_id,
      [fieldsCore.productName]: FIELDS.product_name,
      [fieldsCore.adjustedTax]: FIELDS.adjusted_tax,
      [fieldsCore.basePrice]: FIELDS.base_price,
      [fieldsCore.bonusProductLineItem]: FIELDS.bonus_product_line_item,
      [fieldsCore.gift]: FIELDS.gift,
      [fieldsCore.itemId]: FIELDS.item_id,
      [fieldsCore.itemText]: FIELDS.item_text,
      [fieldsCore.priceAfterItemDiscount]: FIELDS.price_after_item_discount,
      [fieldsCore.priceAfterOrderDiscount]: FIELDS.price_after_order_discount,
      [fieldsCore.quantity]: FIELDS.quantity,
      [fieldsCore.shipmentId]: FIELDS.shipment_id,
      [fieldsCore.tax]: FIELDS.tax,
      [fieldsCore.taxBasis]: FIELDS.tax_basis,
      [fieldsCore.taxClassId]: FIELDS.tax_class_id,
      [fieldsCore.taxRate]: FIELDS.tax_rate,
    };
  }

  // NEED to CHANGE
  static getProductItemsFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.email]: fieldsCore.email,
    };
  }

  // Fields from Core to save in SFCC
  static getProductItemsFieldsToSave(fieldsCore) {
    return {
      [FIELDS.product_id]: fieldsCore.productId,
      [FIELDS.quantity]: fieldsCore.quantity,
      [FIELDS.shipment_id]: fieldsCore.shipmentId,
    };
  }
}

module.exports = ProductItemsMapping;

const ShipmentMapping = require('./shipment-mapping');

class ShipmentConverter {
  static getDataMapToCore(obj, shipmentData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ShipmentMapping.getCoreFields(fieldsCore),
        shippingAddress: {
          address: 'address1'
        },
      },
    };

    const data = {
      [libObjId]: shipmentData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToShipment(obj, shipmentData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ShipmentMapping.getShipmentFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: shipmentData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = ShipmentConverter;

const FieldsConstant = require('./shipment-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class ShipmentMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_CATEGORY._type,
      [fieldsCore.adjustedMerchandizeTotalTax]: FIELD_CATEGORY.adjusted_merchandize_total_tax,
      [fieldsCore.adjustedShippingTotalTax]: FIELD_CATEGORY.adjusted_shipping_total_tax,
      [fieldsCore.gift]: FIELD_CATEGORY.gift,
      [fieldsCore.merchandizeTotalTax]: FIELD_CATEGORY.merchandize_total_tax,
      [fieldsCore.productSubTotal]: FIELD_CATEGORY.product_sub_total,
      [fieldsCore.productTotal]: FIELD_CATEGORY.product_total,
      [fieldsCore.shipmentId]: FIELD_CATEGORY.shipment_id,
      [fieldsCore.shipmentTotal]: FIELD_CATEGORY.shipment_total,
      [fieldsCore.shippingStatus]: FIELD_CATEGORY.shipping_status,
      [fieldsCore.shippingTotal]: FIELD_CATEGORY.shipping_total,
      [fieldsCore.shippingTotalTax]: FIELD_CATEGORY.shipping_total_tax,
      [fieldsCore.taxTotal]: FIELD_CATEGORY.tax_total,
    };
  }

  static getShipmentFields(fieldsCore) {
    return {
      [fieldsCore._type]: FIELD_CATEGORY.type,
      [fieldsCore.adjusted_merchandize_total_tax]: FIELD_CATEGORY.adjustedMerchandizeTotalTax,
      [fieldsCore.adjusted_shipping_total_tax]: FIELD_CATEGORY.adjustedShippingTotalTax,
      [fieldsCore.gift]: FIELD_CATEGORY.gift,
      [fieldsCore.merchandize_total_tax]: FIELD_CATEGORY.merchandizeTotalTax,
      [fieldsCore.product_sub_total]: FIELD_CATEGORY.productSubTotal,
      [fieldsCore.product_total]: FIELD_CATEGORY.productTotal,
      [fieldsCore.shipment_id]: FIELD_CATEGORY.shipmentId,
      [fieldsCore.shipment_total]: FIELD_CATEGORY.shipmentTotal,
      [fieldsCore.shipping_status]: FIELD_CATEGORY.shippingStatus,
      [fieldsCore.shipping_total]: FIELD_CATEGORY.shippingTotal,
      [fieldsCore.shipping_total_tax]: FIELD_CATEGORY.shippingTotalTax,
      [fieldsCore.tax_total]: FIELD_CATEGORY.taxTotal,
    };
  }
}

module.exports = ShipmentMapping;

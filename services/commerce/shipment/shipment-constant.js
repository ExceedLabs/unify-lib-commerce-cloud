module.exports = {
    methods: {
      get: 'GET',
      post: 'POST',
      patch: 'PATCH',
      put: 'PUT',
      delete: 'DELETE',
    },
    fields: {
      _type: '_type',
      adjusted_merchandize_total_tax: 'adjusted_merchandize_total_tax',
      adjusted_shipping_total_tax: 'adjusted_shipping_total_tax',
      gift: 'gift',
      merchandize_total_tax: 'merchandize_total_tax',
      product_sub_total: 'product_sub_total',
      product_total: 'product_total',
      shipment_id: 'shipment_id',
      shipment_total: 'shipment_total',
      shipping_status: 'shipping_status',
      shipping_total: 'shipping_total',
      shipping_total_tax: 'shipping_total_tax',
      tax_total: 'tax_total',
    },
  };
  
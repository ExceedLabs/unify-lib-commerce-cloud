const CategoryMapping = require('./category-mapping');

class CategoryConverter {
  static getDataMapToCore(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...CategoryMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToCategory(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...CategoryMapping.getCategoryFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = CategoryConverter;

const FieldsConstant = require('./category-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class CategoryMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.id]: FIELD_CATEGORY.id,
      [fieldsCore.name]: FIELD_CATEGORY.name,
      [fieldsCore.description]: FIELD_CATEGORY.description,
      [fieldsCore.pageDescription]: FIELD_CATEGORY.page_description,
      [fieldsCore.pageTitle]: FIELD_CATEGORY.page_title,
      [fieldsCore.catalogId]: FIELD_CATEGORY.catalog_id,
      [fieldsCore.isOnline]: FIELD_CATEGORY.online,
      [fieldsCore.link]: FIELD_CATEGORY.link,
      [fieldsCore.image]: FIELD_CATEGORY.image,
      [fieldsCore.thumbnail]: FIELD_CATEGORY.thumbnail,
      [fieldsCore.createdAt]: FIELD_CATEGORY.creation_date,
    };
  }

  static getCategoryFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.id]: fieldsCore.id,
      [FIELD_CATEGORY.name]: fieldsCore.name,
      [FIELD_CATEGORY.description]: fieldsCore.shortDescription,
      [FIELD_CATEGORY.page_description]: fieldsCore.pageDescription,
      [FIELD_CATEGORY.page_title]: fieldsCore.pageTitle,
      [FIELD_CATEGORY.catalog_id]: fieldsCore.catalogId,
      [FIELD_CATEGORY.online]: fieldsCore.isOnline,
      [FIELD_CATEGORY.link]: fieldsCore.link,
      [FIELD_CATEGORY.image]: fieldsCore.image,
      [FIELD_CATEGORY.thumbnail]: fieldsCore.thumbnail,
      [FIELD_CATEGORY.creation_date]: fieldsCore.createdAt,
    };
  }
}

module.exports = CategoryMapping;

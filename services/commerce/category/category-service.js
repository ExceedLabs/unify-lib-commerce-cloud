const axios = require('axios');
const GenericHelper = require('../../helpers/unify-helpers/generic-helper');
const CategoryConstants = require('./category-constant');
const CategoryEndpoint = require('./category-endpoint');

class CategoryService {
  static getCategory(currentLib, tokenObj, params) {
    const { baseUrl, siteId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + CategoryEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CategoryConstants.methods.get,
      { siteId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static searchCategory(currentLib, tokenObj, params) {
    const { baseUrl } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + CategoryEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CategoryConstants.methods.search,
      params,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: {
        query: {
          text_query: {
            fields: ['id', 'name'],
            search_phrase: params.name,
          },
        },
        select: '(**)',
      },
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static searchCategoryProduct(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + CategoryEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CategoryConstants.methods.searchCategoryProduct,
      { siteId, ...params, clientId },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listCategories(currentLib, tokenObj, params) {
    const { siteUrl, clientId } = currentLib.credentials;
    const categoryConfiguration = currentLib.credentials.categoryId;
    let newParams = params;
    if (categoryConfiguration) {
      newParams = { ...params, categoryId: categoryConfiguration };
    }
    const finalAddItemUrl = siteUrl + CategoryEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CategoryConstants.methods.get,
      { ...newParams, clientId },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listCategoriesById(currentLib, tokenObj, params) {
    const { siteUrl, clientId } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + CategoryEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      CategoryConstants.methods.getById,
      { ...params, clientId },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }
}

module.exports = CategoryService;

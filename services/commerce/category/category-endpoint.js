const { methods } = require('./category-constant');

class CategoryEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.searchCategoryProduct:
        return `/s/${params.siteId}/dw/shop/${version}/product_search?q=${params.q}&expand=images,prices,availability&client_id=${params.clientId}`;
      case methods.search:
        return `/s/-/dw/data/${version}/category_search`;
      case methods.get:
        if (params.categoryId) {
          return `/shop/${version}/categories/${params.categoryId}?levels=100&client_id=${params.clientId}`;
        }
        return `/shop/${version}/categories/root?levels=100&client_id=${params.clientId}`;
      case methods.getById:
        return `/shop/${version}/categories/${params.categoryId}?levels=0&client_id=${params.clientId}`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = CategoryEndpoints;

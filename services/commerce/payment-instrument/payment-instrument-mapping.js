const FieldsConstant = require('./payment-instrument-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class PaymentInstrumentMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.paymentMethodId]: FIELD_CATEGORY.payment_method_id,
      [fieldsCore.paymentInstrumentId]: FIELD_CATEGORY.payment_instrument_id,
      [fieldsCore.holder]: `payment_card.${FIELD_CATEGORY.holder}`,
      [fieldsCore.cardType]: `payment_card.${FIELD_CATEGORY.card_type}`,
      [fieldsCore.cardNumber]: `payment_card.${FIELD_CATEGORY.number}`,
      [fieldsCore.securityCode]: `payment_card.${FIELD_CATEGORY.security_code}`,
      [fieldsCore.expirationMonth]: `payment_card.${FIELD_CATEGORY.expiration_month}`,
      [fieldsCore.expirationYear]: `payment_card.${FIELD_CATEGORY.expiration_year}`,
      [fieldsCore.isCreditCardExpired]: `payment_card.${FIELD_CATEGORY.credit_card_expired}`,
      [fieldsCore.maskedNumber]: `payment_card.${FIELD_CATEGORY.masked_number}`,
    };
  }

  static getPaymentInstrumentFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.payment_method_id]: fieldsCore.paymentMethodId,
      [FIELD_CATEGORY.payment_instrument_id]: fieldsCore.paymentInstrumentId,
      [FIELD_CATEGORY.amount]: fieldsCore.amount,
      [`paymentCard.${fieldsCore.holder}`]: `paymentCard.${fieldsCore.holder}`,
      [fieldsCore.card_type]: `paymentCard.${fieldsCore.cardType}`,
      [fieldsCore.number]: `payment_card.${fieldsCore.cardNumber}`,
      [fieldsCore.security_code]: `payment_card.${fieldsCore.securityCode}`,
      [fieldsCore.expiration_month]: `payment_card.${fieldsCore.expirationMonth}`,
      [fieldsCore.expiration_year]: `payment_card.${fieldsCore.expirationYear}`,
      [fieldsCore.credit_card_expired]: `payment_card.${fieldsCore.isCreditCardExpired}`,
      [fieldsCore.masked_number]: `payment_card.${fieldsCore.maskedNumber}`,
    };
  }
}

module.exports = PaymentInstrumentMapping;

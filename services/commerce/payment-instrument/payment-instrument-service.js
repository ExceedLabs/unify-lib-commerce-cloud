const axios = require('axios');

class PaymentInstrumentService {
  static getStructureToSave(payload) {
      const structureJson = {
        payment_card: {
            ...payload
        },
        payment_method_id: payload.payment_method_id
    };
    delete structureJson.payment_card.payment_method_id;
    return structureJson;
  }
}

module.exports = PaymentInstrumentService;

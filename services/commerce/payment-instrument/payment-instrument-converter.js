const _ = require('lodash');
const PaymentInstrumentMapping = require('./payment-instrument-mapping');

class PaymentInstrumentConverter {
  static getDataMapToCore(obj, paymentData) {
    const { fieldsCore, replaceFields } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...PaymentInstrumentMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: paymentData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToPaymentInstrument(obj, paymentData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...PaymentInstrumentMapping.getPaymentInstrumentFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: paymentData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = PaymentInstrumentConverter;

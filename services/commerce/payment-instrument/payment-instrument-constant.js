module.exports = {
  methods: {
    get: 'GET',
    post: 'POST',
    patch: 'PATCH',
    put: 'PUT',
  },
  fields: {
      amount: 'amount',
      number: 'number',
      security_code: 'security_code',
      holder: 'holder',
      card_type: 'card_type',
      expiration_month: 'expiration_month',
      expiration_year: 'expiration_year',
      payment_method_id: 'payment_method_id',
      masked_number: 'masked_number',
      credit_card_expired: 'credit_card_expired',
      payment_instrument_id: 'payment_instrument_id',
  },
};

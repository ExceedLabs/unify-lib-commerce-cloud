const ShippingMapping = require('./shipping-mapping');

class ShippingConverter {
  static getDataMapToCore(obj, shippingData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ShippingMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: shippingData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToShipping(obj, shippingData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ShippingMapping.getShipmentFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: shippingData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = ShippingConverter;

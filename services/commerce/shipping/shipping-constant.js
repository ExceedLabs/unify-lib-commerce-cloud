module.exports = {
    methods: {
      get: 'GET',
      post: 'POST',
      patch: 'PATCH',
      put: 'PUT',
      delete: 'DELETE',
    },
    fields: {
      _type: '_type',
      description: 'description',
      id: 'id',
      name: 'name',
      price: 'price',
    },
  };
  
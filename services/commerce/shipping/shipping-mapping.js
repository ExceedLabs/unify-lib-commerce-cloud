const FieldsConstant = require('./shipping-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class ShippingMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_CATEGORY._type,
      [fieldsCore.description]: FIELD_CATEGORY.description,
      [fieldsCore.id]: FIELD_CATEGORY.id,
      [fieldsCore.name]: FIELD_CATEGORY.name,
      [fieldsCore.price]: FIELD_CATEGORY.price,
    };
  }

  static getShippingFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_CATEGORY._type,
      [fieldsCore.description]: FIELD_CATEGORY.description,
      [fieldsCore.id]: FIELD_CATEGORY.id,
      [fieldsCore.name]: FIELD_CATEGORY.name,
      [fieldsCore.price]: FIELD_CATEGORY.price,
    };
  }
}

module.exports = ShippingMapping;

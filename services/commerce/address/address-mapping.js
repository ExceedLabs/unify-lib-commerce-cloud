const FieldsConstant = require('./address-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class AddressMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.street]: FIELD_CATEGORY.address1,
      [fieldsCore.country]: FIELD_CATEGORY.country_code,
      [fieldsCore.firstName]: FIELD_CATEGORY.first_name,
      [fieldsCore.lastName]: FIELD_CATEGORY.last_name,
      [fieldsCore.phone]: FIELD_CATEGORY.phone,
      [fieldsCore.city]: FIELD_CATEGORY.city,
      [fieldsCore.zipCode]: FIELD_CATEGORY.postal_code,
      [fieldsCore.state]: FIELD_CATEGORY.state_code,
      [fieldsCore.id]: FIELD_CATEGORY.id,
      [fieldsCore.typeAddress]: FIELD_CATEGORY._type,
    };
  }

  static getAddressFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.address1]: fieldsCore.street,
      [FIELD_CATEGORY.country_code]: fieldsCore.country,
      [FIELD_CATEGORY.first_name]: fieldsCore.firstName,
      [FIELD_CATEGORY.last_name]: fieldsCore.lastName,
      [FIELD_CATEGORY.phone]: fieldsCore.phone,
      [FIELD_CATEGORY.city]: fieldsCore.city,
      [FIELD_CATEGORY.postal_code]: fieldsCore.zipCode,
      [FIELD_CATEGORY.state_code]: fieldsCore.state,
      [FIELD_CATEGORY.id]: fieldsCore.id,
      [FIELD_CATEGORY._type]: fieldsCore.typeAddress,
    };
  }
}

module.exports = AddressMapping;

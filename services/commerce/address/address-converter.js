const _ = require('lodash');
const AddressMapping = require('./address-mapping');

class AddressConverter {
  static getDataMapToCore(obj, billingAddressData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...AddressMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: billingAddressData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToAddress(obj, billingAddressData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...AddressMapping.getAddressFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: billingAddressData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = AddressConverter;

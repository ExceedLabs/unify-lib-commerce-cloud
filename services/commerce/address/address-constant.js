module.exports = {
    methods: {
      get: 'GET',
      post: 'POST',
      patch: 'PATCH',
      put: 'PUT',
    },
    fields: {
      _type: '_type', // home, work, billing
      address1: 'address1',
      country_code: 'country_code',
      first_name: 'first_name',
      last_name: 'last_name',
      phone: 'phone',
      postal_code: 'postal_code',
      state_code: 'state_code',
      id: 'id',
      city: 'city',
      preferred: 'preferred',
      post_box: 'post_box',
      address_id: 'address_id',
    },
  };
    
const axios = require('axios');
const ProductEndpoint = require('./product-endpoint');
const ProductConstant = require('./product-constant');

class ProductService {
  static getProduct(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + ProductEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      ProductConstant.methods.get,
      { siteId, ...params, clientId },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static searchProduct(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + ProductEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      ProductConstant.methods.search,
      { siteId, clientId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static getProductInventory(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalGetInventoryUrl = baseUrl + ProductEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      ProductConstant.methods.getInventory,
      { siteId, clientId, ...params },
    );

    return axios({
      method: 'get',
      url: finalGetInventoryUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
      },
    });
  }
}

module.exports = ProductService;

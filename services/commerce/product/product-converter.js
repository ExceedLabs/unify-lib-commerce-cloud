const _ = require('lodash');
const ProductMapping = require('./product-mapping');

const getCoreFieldsProduct = (isProductItems, fieldsCore) => {
  if (isProductItems) {
    return ProductMapping.getCoreProductItemsFields(fieldsCore);
  }
  return ProductMapping.getProductsCoreFields(fieldsCore);
}

const getProductFields = (isProductItems, fieldsCore) => {
  if (isProductItems) {
    return ProductMapping.getProductItemsFields(fieldsCore);
  }
  return ProductMapping.getProductFields(fieldsCore);
}

class ProductConverter {
  static getDataMapToCore(obj, productData, isProductItems = false) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...getCoreFieldsProduct(isProductItems, fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }

  /**
   * API customer/search-products?=shoes
   * @param {object} obj
   * @param {array} productData
   */
  static getSearchDataMapToCore(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ProductMapping.getSearchProductsCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToProduct(obj, productData, isProductItems = false) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ProductMapping.getCoreToProductItemsFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToProductInventory(obj, productInventoryData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ProductMapping.getProductInventoryFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productInventoryData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = ProductConverter;

const { methods } = require('./product-constant');

class ProductEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.get:
        return `/s/${params.siteId}/dw/shop/${version}/products/${params.productId}?expand=availability,prices,images,variations,promotions&client_id=${params.clientId}`;
      case methods.search:
        return `/s/${params.siteId}/dw/shop/${version}/product_search?q=${params.q}&expand=images,prices,availability,variations&client_id=${params.clientId}`;
      case methods.getInventory:
        return `/s/-/dw/data/${version}/inventory_lists/${params.storeId}/product_inventory_records/${params.productId}`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = ProductEndpoints;

const _ = require('lodash');
const FieldsConstant = require('./product-constant');

const FIELD_PRODUCT = FieldsConstant.fields; // deprecated
const FIELD_PRODUCT_ITEM = FieldsConstant.fieldsProductItems;
const FIELDS = FieldsConstant.fieldsProduct;
const SEARCH_FIELDS_PRODUCT = FieldsConstant.searchFieldsProduct;
const PRODUCT_INVENTORY_FIELDS = FieldsConstant.productInventory;

class ProductMapping {
  static getProductsFields(fieldsCore) {
    return {
      [FIELD_PRODUCT.id]: fieldsCore.id,
      [FIELD_PRODUCT.name]: fieldsCore.name,
      [FIELD_PRODUCT.link]: fieldsCore.link,
      [FIELD_PRODUCT.image]: fieldsCore.image,
      [FIELD_PRODUCT.image_groups]: fieldsCore.imageGroups,
      [FIELD_PRODUCT.currency]: fieldsCore.currency,
      [FIELD_PRODUCT.price]: fieldsCore.price,
      [FIELD_PRODUCT.online]: fieldsCore.isOnline,
      [FIELD_PRODUCT.in_stock]: fieldsCore.isInStock,
      [FIELD_PRODUCT.owning_category_id]: fieldsCore.categoryId,
      [FIELD_PRODUCT.owning_catalog_id]: fieldsCore.catalogId,
      [FIELD_PRODUCT.owning_catalog_name]: fieldsCore.catalogName,
      [FIELD_PRODUCT.short_description]: fieldsCore.shortDescription,
      [FIELD_PRODUCT.creation_date]: fieldsCore.createdAt,
      [FIELD_PRODUCT.last_modified]: fieldsCore.updatedAt,
    };
  }

  // Fields from products only in SFCC
  // [DEPRECATED]
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.id]: FIELD_PRODUCT.id,
      [fieldsCore.name]: FIELD_PRODUCT.name,
      [fieldsCore.link]: FIELD_PRODUCT.link,
      [fieldsCore.price]: FIELD_PRODUCT.price,
      [fieldsCore.image]: FIELD_PRODUCT.image,
      [fieldsCore.currency]: FIELD_PRODUCT.currency,
      [fieldsCore.isOnline]: FIELD_PRODUCT.online,
      [fieldsCore.isInStock]: FIELD_PRODUCT.in_stock,
      [fieldsCore.categoryId]: FIELD_PRODUCT.primary_category_id,
      [fieldsCore.catalogId]: FIELD_PRODUCT.owning_catalog_id,
      [fieldsCore.catalogName]: FIELD_PRODUCT.owning_catalog_name,
      [fieldsCore.shortDescription]: FIELD_PRODUCT.short_description,
      [fieldsCore.createdAt]: FIELD_PRODUCT.creation_date,
      [fieldsCore.updatedAt]: FIELD_PRODUCT.last_modified,
    };
  }

  static getProductsCoreFields(fieldsCore) {
    return {
      [fieldsCore.id]: FIELDS.id,
      [fieldsCore.name]: FIELDS.name,
      [fieldsCore.currency]: FIELDS.currency,
      [fieldsCore.price]: FIELDS.price,
      [fieldsCore.longDescription]: FIELDS.long_description,
      [fieldsCore.shortDescription]: FIELDS.short_description,
      [fieldsCore.stepQuantity]: FIELDS.step_quantity,
      [fieldsCore.minOrderQuantity]: FIELDS.min_order_quantity,
      [fieldsCore.unit]: FIELDS.unit,
      [fieldsCore.primaryCategory]: FIELDS.primary_category_id,
      [fieldsCore.availableForInStorePickup]: FIELDS.c_availableForInStorePickup,
    };
  }

  /**
   * /customer/search-product?q=shoes
   * @param {object} fieldsCore
   */
  static getSearchProductsCoreFields(fieldsCore) {
    return {
      [fieldsCore.id]: SEARCH_FIELDS_PRODUCT.product_id,
      [fieldsCore.name]: SEARCH_FIELDS_PRODUCT.product_name,
      [fieldsCore.currency]: SEARCH_FIELDS_PRODUCT.currency,
      [fieldsCore.price]: SEARCH_FIELDS_PRODUCT.price,
      [fieldsCore.hitType]: SEARCH_FIELDS_PRODUCT.hit_type,
      [fieldsCore.link]: SEARCH_FIELDS_PRODUCT.link,
      [fieldsCore.orderable]: SEARCH_FIELDS_PRODUCT.orderable,
      [fieldsCore.representedProductId]: SEARCH_FIELDS_PRODUCT.represented_product_id,
      [fieldsCore.representedProductLink]: SEARCH_FIELDS_PRODUCT.represented_product_link,
      [fieldsCore.representedProductType]: SEARCH_FIELDS_PRODUCT.represented_product_type,
    };
  }

  // Fields from products_items returned inside Order from SFCC
  static getCoreProductItemsFields(fieldsCore) {
    return {
      [fieldsCore.id]: FIELD_PRODUCT_ITEM.item_id,
      [fieldsCore.name]: FIELD_PRODUCT_ITEM.item_text,
      [fieldsCore.link]: FIELD_PRODUCT_ITEM.link,
      [fieldsCore.quantity]: FIELD_PRODUCT_ITEM.quantity,
      [fieldsCore.price]: FIELD_PRODUCT_ITEM.price,
      [fieldsCore.image]: FIELD_PRODUCT_ITEM.image,
      [fieldsCore.imageGroups]: FIELD_PRODUCT_ITEM.image_groups,
      [fieldsCore.currency]: FIELD_PRODUCT_ITEM.currency,
      [fieldsCore.isOnline]: FIELD_PRODUCT_ITEM.online,
      [fieldsCore.isInStock]: FIELD_PRODUCT_ITEM.in_stock,
      [fieldsCore.categoryId]: FIELD_PRODUCT_ITEM.owning_category_id,
      [fieldsCore.catalogId]: FIELD_PRODUCT_ITEM.owning_catalog_id,
      [fieldsCore.catalogName]: FIELD_PRODUCT_ITEM.owning_catalog_name,
      [fieldsCore.shortDescription]: FIELD_PRODUCT_ITEM.short_description,
      [fieldsCore.createdAt]: FIELD_PRODUCT_ITEM.creation_date,
      [fieldsCore.updatedAt]: FIELD_PRODUCT_ITEM.last_modified,
    };
  }

  static getProductInventoryFields(fieldsCore) {
    return {
      [fieldsCore.ats]: PRODUCT_INVENTORY_FIELDS.ats,
      [fieldsCore.creationDate]: PRODUCT_INVENTORY_FIELDS.creation_date,
      [fieldsCore.inventoryListId]: PRODUCT_INVENTORY_FIELDS.inventory_list_id,
      [fieldsCore.inventoryTurnover]: PRODUCT_INVENTORY_FIELDS.inventory_turnover,
      [fieldsCore.lastModified]: PRODUCT_INVENTORY_FIELDS.last_modified,
      [fieldsCore.perpetualFlag]: PRODUCT_INVENTORY_FIELDS.perpetual_flag,
      [fieldsCore.preOrderBackOrderHandling]: PRODUCT_INVENTORY_FIELDS.pre_order_back_order_handling,
      [fieldsCore.productId]: PRODUCT_INVENTORY_FIELDS.product_id,
      [fieldsCore.productName]: PRODUCT_INVENTORY_FIELDS.product_name,
      [fieldsCore.quantityOnOrder]: PRODUCT_INVENTORY_FIELDS.quantity_on_order,
      [fieldsCore.stockLevel]: PRODUCT_INVENTORY_FIELDS.stock_level,
      [fieldsCore.resetDate]: PRODUCT_INVENTORY_FIELDS.reset_date,
    };
  }
}

module.exports = ProductMapping;

module.exports = {
  methods: {
    get: 'GET',
    post: 'POST',
    patch: 'PATCH',
    put: 'PUT',
  },
  fields: {
    _type: '_type',
    id: 'id',
    name: 'name',
    payment_processor_id: 'payment_processor_id',
    cards: 'cards',
    card_type: 'card_type',
    card_name: 'name',
    security_code_length: 'security_code_length',
    checksum_verification_enabled: 'checksum_verification_enabled'
  },
};

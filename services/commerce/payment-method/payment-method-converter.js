const _ = require('lodash');
const PaymentMethodMapping = require('./payment-method-mapping');

class PaymentMethodConverter {
  static getDataMapToCore(obj, productData) {
    const { fieldsCore, replaceFields } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const nestedCardsMap = {
        list: 'list',
        item: {
            ...PaymentMethodMapping.getCoreCardsFields(fieldsCore),    
        }
    };

    const map = {
      list: libObjId,
      item: {
        ...PaymentMethodMapping.getCoreFields(fieldsCore),
        cards: 'cards'
      },
      operate: [{
          run: (values) => {
              if (values) {
                return replaceFields({ list: values }, nestedCardsMap);
              }
              return [];
          },
          on: 'cards'
      }]
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToPaymentMethod(obj, productData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...PaymentMethodMapping.getPaymentMethodFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: productData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = PaymentMethodConverter;

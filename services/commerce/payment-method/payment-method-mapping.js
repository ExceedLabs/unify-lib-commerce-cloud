const FieldsConstant = require('./payment-method-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class PaymentMethodMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_CATEGORY._type,
      [fieldsCore.id]: FIELD_CATEGORY.id,
      [fieldsCore.name]: FIELD_CATEGORY.name,
      [fieldsCore.paymentProcessorId]: FIELD_CATEGORY.payment_processor_id,
      [fieldsCore.cards]: FIELD_CATEGORY.cards,
    };
  }

  // Get only the card array fields
  static getCoreCardsFields(fieldsCore) {
    return {
        [fieldsCore.cardType]: FIELD_CATEGORY.card_type,
        [fieldsCore.cardName]: FIELD_CATEGORY.card_name,
        [fieldsCore.cardSecurityCodeLength]: FIELD_CATEGORY.security_code_length,
        [fieldsCore.isCardChecksumVerificationEnabled]: FIELD_CATEGORY.checksum_verification_enabled,
      };  
  }

  static getPaymentMethodFields(fieldsCore) {
    return {
        [FIELD_CATEGORY._type] : fieldsCore.type,
        [FIELD_CATEGORY.id] : fieldsCore.id,
        [FIELD_CATEGORY.holder] : fieldsCore.holder,
        [FIELD_CATEGORY.name] : fieldsCore.name,
        [FIELD_CATEGORY.payment_processor_id] : fieldsCore.paymentProcessorId,
        [FIELD_CATEGORY.checksum_verification_enabled] : fieldsCore.checksumVerificationEnabled,
        [FIELD_CATEGORY.cards] : fieldsCore.cards,
        [FIELD_CATEGORY.card_type] : fieldsCore.cardType,
        [FIELD_CATEGORY.card_name] : fieldsCore.cardName,
        [FIELD_CATEGORY.number] : fieldsCore.cardNumber,
        [FIELD_CATEGORY.security_code_length] : fieldsCore.cardSecurityCodeLength,
        [FIELD_CATEGORY.payment_method_id] : fieldsCore.paymentMethodId,
        [FIELD_CATEGORY.security_code] : fieldsCore.securityCode,
        [FIELD_CATEGORY.expiration_month] : fieldsCore.expirationMonth,
        [FIELD_CATEGORY.expiration_year] : fieldsCore.expirationYear,
    };
  }
}

module.exports = PaymentMethodMapping;

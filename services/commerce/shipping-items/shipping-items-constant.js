module.exports = {
  methods: {
    get: 'GET',
    post: 'POST',
    patch: 'PATCH',
    put: 'PUT',
  },
  fields: {
    _type: '_type',
    price: 'price',
    adjusted_tax: 'adjusted_tax',
    base_price: 'base_price',
    item_id: 'item_id',
    item_text: 'item_text',
    price_after_item_discount: 'price_after_item_discount',
    shipment_id: 'shipment_id',
    tax: 'tax',
    tax_basis: 'tax_basis',
    tax_class_id: 'tax_class_id',
    tax_rate: 'tax_rate',
  },
};

const _ = require('lodash');
const ShippingItemsMapping = require('./shipping-items-mapping');

class ShippingItemsConverter {
  static getDataMapToCore(obj, shippingItemsData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ShippingItemsMapping.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: shippingItemsData,
    };

    return {
      map,
      data,
    };
  }

  static getDataMapToProductItems(obj, shippingItemsData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...ShippingItemsMapping.getProductItemsFieldsToSave(fieldsCore),
      },
    };

    const data = {
      [libObjId]: shippingItemsData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = ShippingItemsConverter;

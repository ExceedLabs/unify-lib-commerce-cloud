const FieldsConstant = require('./shipping-items-constant');

const FIELDS = FieldsConstant.fields;

class ShippingItemsMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELDS._type,
      [fieldsCore.price]: FIELDS.price,
      [fieldsCore.adjustedTax]: FIELDS.adjusted_tax,
      [fieldsCore.basePrice]: FIELDS.base_price,
      [fieldsCore.itemId]: FIELDS.item_id,
      [fieldsCore.itemText]: FIELDS.item_text,
      [fieldsCore.priceAfterItemDiscount]: FIELDS.price_after_item_discount,
      [fieldsCore.shipmentId]: FIELDS.shipment_id,
      [fieldsCore.tax]: FIELDS.tax,
      [fieldsCore.taxBasis]: FIELDS.tax_basis,
      [fieldsCore.taxClassId]: FIELDS.tax_class_id,
      [fieldsCore.taxRate]: FIELDS.tax_rate,
    };
  }

  // NEED to CHANGE
  static getShippingItemsFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.email]: fieldsCore.email,
    };
  }
}

module.exports = ShippingItemsMapping;

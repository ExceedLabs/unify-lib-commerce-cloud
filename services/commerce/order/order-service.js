const axios = require('axios');
const GenericHelper = require('../../helpers/unify-helpers/generic-helper');
const OrderConstants = require('./order-constant');
const OrderEndpoint = require('./order-endpoint');

class OrderService {
  static searchOrder(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + OrderEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      OrderConstants.methods.search,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: {
        query: {
          text_query: {
            fields: ['customer_no'],
            search_phrase: params.customerNumber,
          },
        },
        select: '(**)',
      },
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static createOrder(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + OrderEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      OrderConstants.methods.post,
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static getOrderByNumber(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + OrderEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      OrderConstants.methods.get,
      params
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static getPaymentMethod(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + OrderEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      OrderConstants.methods.getPaymentMethod,
      params
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static getPaymentInstrument(currentLib, tokenObj, params) {
    return this.getOrderByNumber(currentLib, tokenObj, params);
  }

  static createPaymentInstrument(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + OrderEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      OrderConstants.methods.createPaymentInstrument,
      params
    );

    return axios({
      method: 'post',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static updateOrder(currentLib, tokenObj, params) {
    const { siteUrl } = currentLib.credentials;
    const finalAddItemUrl = siteUrl + OrderEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      OrderConstants.methods.updateOrder,
      params
    );

    return axios({
      method: 'patch',
      url: finalAddItemUrl,
      data: params.payload,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static async AddPayworksInfoToOrder(currentLib, tokenObj, params) {
    const payworksValues = {
      [OrderConstants.fields.c_payworksTransactionId]: params[OrderConstants.fields.c_payworksTransactionId],
      [OrderConstants.fields.c_payworksTransactionStatus]: params[OrderConstants.fields.c_payworksTransactionStatus],
    };
    const orderNumber = params[OrderConstants.fields.order_no];
    return this.updateOrder(
      currentLib,
      tokenObj,
      {
        payload: payworksValues,
        orderNumber,
      },
    );
  }
}

module.exports = OrderService;

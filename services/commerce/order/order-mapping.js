const FieldsConstant = require('./order-constant');

const FIELD_CATEGORY = FieldsConstant.fields;

class OrderMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.c_sterlingOmsOrderNumber]: FIELD_CATEGORY.c_sterlingOmsOrderNumber,
      [fieldsCore.email]: FIELD_CATEGORY.email,
      [fieldsCore.basketId]: FIELD_CATEGORY.basket_id,
      [fieldsCore.channelType]: FIELD_CATEGORY.channel_type,
      [fieldsCore.customerId]: FIELD_CATEGORY.customer_id,
      [fieldsCore.customerNumber]: FIELD_CATEGORY.customer_no,
      [fieldsCore.customerName]: FIELD_CATEGORY.customer_name,
      [fieldsCore.orderNumber]: FIELD_CATEGORY.order_no,
      [fieldsCore.orderToken]: FIELD_CATEGORY.order_token,
      [fieldsCore.orderTotal]: FIELD_CATEGORY.order_total,
      [fieldsCore.currency]: FIELD_CATEGORY.currency,
      [fieldsCore.paymentStatus]: FIELD_CATEGORY.payment_status,
      [fieldsCore.productSubTotal]: FIELD_CATEGORY.product_sub_total,
      [fieldsCore.productTotal]: FIELD_CATEGORY.product_total,
      [fieldsCore.shippingStatus]: FIELD_CATEGORY.shipping_status,
      [fieldsCore.shippingTotal]: FIELD_CATEGORY.shipping_total,
      [fieldsCore.shippingTotalTax]: FIELD_CATEGORY.shipping_total_tax,
      [fieldsCore.taxTotal]: FIELD_CATEGORY.tax_total,
      [fieldsCore.confirmationStatus]: FIELD_CATEGORY.confirmation_status,
      [fieldsCore.updatedAt]: FIELD_CATEGORY.last_modified,
      [fieldsCore.createdAt]: FIELD_CATEGORY.creation_date,
      [fieldsCore.payworksToken]: FIELD_CATEGORY.c_payworksTransactionId,
      [fieldsCore.paymentTransactionStatus]: FIELD_CATEGORY.c_payworksTransactionStatus,
    };
  }

  static getOrderFields(fieldsCore) {
    return {
      [FIELD_CATEGORY.email]: fieldsCore.email,
      [FIELD_CATEGORY.basket_id]: fieldsCore.basketId,
      [FIELD_CATEGORY.customer_id]: fieldsCore.customerId,
      [FIELD_CATEGORY.customer_no]: fieldsCore.customerNumber,
      [FIELD_CATEGORY.customer_name]: fieldsCore.customerName,
      [FIELD_CATEGORY.order_no]: fieldsCore.orderNumber,
      [FIELD_CATEGORY.order_token]: fieldsCore.orderToken,
      [FIELD_CATEGORY.order_total]: fieldsCore.orderTotal,
      [FIELD_CATEGORY.currency]: fieldsCore.currency,
      [FIELD_CATEGORY.payment_status]: fieldsCore.paymentStatus,
      [FIELD_CATEGORY.product_sub_total]: fieldsCore.productSubTotal,
      [FIELD_CATEGORY.product_total]: fieldsCore.productTotal,
      [FIELD_CATEGORY.shipping_status]: fieldsCore.shippingStatus,
      [FIELD_CATEGORY.shipping_total]: fieldsCore.shippingTotal,
      [FIELD_CATEGORY.shipping_total_tax]: fieldsCore.shippingTotalTax,
      [FIELD_CATEGORY.tax_total]: fieldsCore.taxTotal,
      [FIELD_CATEGORY.confirmation_status]: fieldsCore.confirmationStatus,
      [FIELD_CATEGORY.last_modified]: fieldsCore.updatedAt,
      [FIELD_CATEGORY.creation_date]: fieldsCore.createdAt,
      [FIELD_CATEGORY.c_payworksTransactionId]: fieldsCore.payworksToken,
      [FIELD_CATEGORY.c_payworksTransactionStatus]: fieldsCore.paymentTransactionStatus,
    };
  }
}

module.exports = OrderMapping;

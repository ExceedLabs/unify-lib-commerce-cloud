const { methods } = require('./order-constant');

class OrderEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.search:
        return `/shop/${version}/order_search`;
      case methods.get:
        return `/shop/${version}/orders/${params.orderNumber}`;
      case methods.getPaymentMethod:
        return `/shop/${version}/orders/${params.orderNumber}/payment_methods`;
      case methods.createPaymentInstrument:
        return `/shop/${version}/orders/${params.orderNumber}/payment_instruments`;
      case methods.post:
        return `/shop/${version}/orders`;
      case methods.updateOrder:
        return `/shop/${version}/orders/${params.orderNumber}`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = OrderEndpoints;

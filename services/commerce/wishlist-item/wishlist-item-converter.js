const _ = require('lodash');
const WishlistItemMapping = require('./wishlist-item-mapping');
const GenericHelper = require('../../helpers/unify-helpers/generic-helper');
const ImageHelper = require('../../helpers/unify-helpers/image-helper');

class WishlistItemConverter {
  static getDataMapToCore(obj, wishlistData) {
    const { fieldsCore, replaceFields } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const nestedProductMap = {
      list: 'list',
      item: {
        id: 'id',
        imageGroups: 'image_groups',
        name: 'name',
        currency: 'currency',
        price: 'price',
        shortDescription: 'short_description',
        longDescription: 'long_description',
        categoryId: 'primary_category_id',
        unit: 'unit',
        stepQuantity: 'step_quantity',
        minOrderQuantity: 'min_order_quantity',
        type: '_type',
      },
    };

    const map = {
      list: libObjId,
      item: {
        ...WishlistItemMapping.getCoreFields(fieldsCore),
        product: 'product',
      },
      operate: [{
        on: 'product',
        run: (value) => {
          if (value) {
            return replaceFields({ list: [value] }, nestedProductMap);
          }
          return [];
        }
      }]
    };

    const data = {
      [libObjId]: wishlistData,
    };

    return {
      map,
      data,
    };
  }

  static getImagesDataMapToCore(obj, wishlistData) {
    const { fieldsCore, replaceFields } = obj.privateLibraries.Converters;

    var wishListWithProducts = _.filter(wishlistData, whishlist => whishlist.product != null && whishlist.product != undefined);

    var products = _.map(wishListWithProducts, value => {
      return value.product;
    });
    
    var image_groups = _.map(products, product => {
      return product.image_groups;
    });

    // By the moment, we just have only large images coming back to client-side
    var images = ImageHelper.getImageSizes(image_groups);

    const nestedMap = {
      list: 'list',
      item: {
        url: 'dis_base_link',
        alt: 'alt',
        link: 'link',
        title: 'title',
      },
    };

    const map = {
      list: 'data',
      item: {
        largeImages: 'largeImages',
        mediumImages: 'mediumImages',
        smallImages: 'smallImages',
        swatchImages: 'swatchImages',
      },
      operate: [{
        on: 'largeImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }, {
        on: 'mediumImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }, {
        on: 'smallImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }, {
        on: 'swatchImages',
        run: (value) => {
          return replaceFields({ list: value }, nestedMap);
        },
      }],
    };

    const data = {
      'data': images,
    };

    return {
      map,
      data,
    };
  }

  static async getDataMapToWishlistItem(obj, wishlistData, loginObject) {
    const { UserHelper } = obj.privateLibraries;
    const userData = await UserHelper.getUserOrganization(obj.req);
    return {
      "type": "wish_list",
      "name": wishlistData.name,
      "description": wishlistData.description,
      "public": wishlistData.public,
      "registrant" : {
        "email" : loginObject.customerEmail,
      },
      "event" : {
        "city" : wishlistData.event.city,
        "country" : wishlistData.event.country,
        "date" : wishlistData.event.date,
        "state" : wishlistData.event.state,
        "type" : wishlistData.event.type,
      }
    };
  }

  static getDataMapSaveToWishlistItem(obj, wishlistData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...WishlistItemMapping.getWishlistFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: wishlistData,
    };

    return {
      map,
      data,
    };
  }
}

module.exports = WishlistItemConverter;

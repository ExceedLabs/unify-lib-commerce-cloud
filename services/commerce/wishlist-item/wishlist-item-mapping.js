const FieldsConstant = require('./wishlist-item-constant');

const FIELD_WISHLIST = FieldsConstant.fields;

class WishlistItemMapping {
  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_WISHLIST.type,
      [fieldsCore.id]: FIELD_WISHLIST.id,
      [fieldsCore.description]: FIELD_WISHLIST.description,
      [fieldsCore.isPublic]: FIELD_WISHLIST.public,
      [fieldsCore.priority]: FIELD_WISHLIST.priority,
      [fieldsCore.productId]: FIELD_WISHLIST.product_id,
      [fieldsCore.quantity]: FIELD_WISHLIST.quantity,
      [fieldsCore.resourceState]: FIELD_WISHLIST._resource_state,
      [fieldsCore.purchasedQuantity]: FIELD_WISHLIST.purchased_quantity,
    };
  }

  static getWishlistFields(fieldsCore) {
    return {
      [FIELD_WISHLIST.type]: fieldsCore.type,
      [FIELD_WISHLIST.priority]: fieldsCore.priority,
      ['product_id']: fieldsCore.productId,
      ['public']: 'public',
      ['quantity']: 'quantity',
    };
  }
}

module.exports = WishlistItemMapping;

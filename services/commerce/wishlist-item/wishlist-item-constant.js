module.exports = {
    methods: {
      get: 'GET',
      post: 'POST',
      patch: 'PATCH',
      put: 'PUT',
      getById: 'GET_BY_ID',
    },
    fields: {
        type: 'type',
        title: 'title',
        description: 'description',
        id: 'id',
        public: 'public',
        creation_date: 'creation_date',
        last_modified: 'last_modified',
        quantity: 'quantity',
        priority: 'priority',
        product_id: 'product_id',
        _resource_state: '_resource_state',
        purchased_quantity: 'purchased_quantity',
        c_salesforceExported: 'c_salesforceExported',
        c_salesforceExternalId: 'c_salesforceExternalId',
    },
  };
  
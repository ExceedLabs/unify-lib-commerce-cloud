const { methods } = require('./wishlist-item-constant');

class WishlistItemEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.get:
        return `/s/${params.siteId}/dw/shop/${version}/product_lists/${params.id}?client_id=${params.clientId}&expand=images,items,product,availability,prices`;
      case methods.getById:
        return `/s/${params.siteId}/dw/shop/${version}/product_lists/${params.id}/items/${params.itemId}?client_id=${params.clientId}&expand=images,items,product,availability,prices`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = WishlistItemEndpoints;

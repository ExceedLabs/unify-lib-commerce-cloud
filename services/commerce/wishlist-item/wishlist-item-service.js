const axios = require('axios');
const WishlistItemEndpoint = require('./wishlist-item-endpoint');
const WishlistItemConstant = require('./wishlist-item-constant');

class WishlistItemService {
  static listWishlistItem(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + WishlistItemEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      WishlistItemConstant.methods.get,
      { siteId, clientId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }

  static listWishlistItemById(currentLib, tokenObj, params) {
    const { baseUrl, siteId, clientId } = currentLib.credentials;
    const finalAddItemUrl = baseUrl + WishlistItemEndpoint.retrieveEndpoint(
      currentLib.credentials.version,
      WishlistItemConstant.methods.getById,
      { siteId, clientId, ...params },
    );

    return axios({
      method: 'get',
      url: finalAddItemUrl,
      headers: {
        Authorization: `${tokenObj.token}`,
        'Content-Type': 'application/json',
      },
    });
  }
}

module.exports = WishlistItemService;

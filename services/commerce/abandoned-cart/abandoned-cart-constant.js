module.exports = {
  methods: {
    get: 'GET',
  },
  preMappingFields: {
    cartFields: {
      _type: '_type', // home, work, billing
      id: 'id',
      name: 'name',
      last_modified: 'last_modified',
      creation_date: 'creation_date',
      currencyCode: 'customer_product_list_items[0].product.currency',
      c_isAbandonedCart: 'c_isAbandonedCart',
      c_salesforceExported: 'c_salesforceExported',
      c_salesforceExternalId: 'c_salesforceExternalId',
      customer_product_list_items: 'customer_product_list_items',
      /* total: 'total',
      itemsNumber: 'itemsNumber'*/

    },
    itemFields: {
      id: 'id',
      product_id: 'product.id',
      price: 'product.price',
      name: 'product.name',
      long_description: 'product.long_description',
      short_description: 'product.short_description',
      quantity: 'quantity',
      c_salesforceExported: 'c_salesforceExported',
      c_salesforceExternalId: 'c_salesforceExternalId',
      images: 'images',
    },
    imagesFields: {
      links: 'links',
      view_type: 'view_type'
    },
    imageLinkFields: {
      alt: 'alt',
      link: 'link',
      title: 'title',
    }
  },
  cartFields: {
    id: 'id',
    name: 'name',
    last_modified: 'last_modified',
    creation_date: 'creation_date',
    currencyCode: 'currencyCode',
    customer_product_list_items: 'customer_product_list_items',
    total: 'total',
    itemsNumber: 'itemsNumber'
  },
  cartItemFields: {
    id: 'id',
    product_id: 'product.id',
    price: 'product.price',
    name: 'product.name',
    long_description: 'product.long_description',
    short_description: 'product.short_description',
    quantity: 'product.quantity',
    images: 'images',
  },
};

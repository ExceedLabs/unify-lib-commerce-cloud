module.exports = {
  methods: {
    get: 'GET',
  },
  fields: {
    id: 'id',
    description: 'description',
    last_modified: 'last_modified',
    on_order_inventory_enabled: 'on_order_inventory_enabled',
    use_bundle_inventory_only: 'use_bundle_inventory_only',
    creation_date: 'creation_date',
  },
};

const StoreConstant = require('./store-constant');

class StoreMapping {
  static getStoreFields(fieldsCore) {
    const storeFields = StoreConstant.fields;
    return {
      [fieldsCore.id] : storeFields.id,
      [fieldsCore.description] : storeFields.description,
      [fieldsCore.lastModified] : storeFields.last_modified,
      [fieldsCore.onOrderInventoryEnabled] : storeFields.on_order_inventory_enabled,
      [fieldsCore.useBundleInventoryOnly] : storeFields.use_bundle_inventory_only,
      [fieldsCore.creationDate] : storeFields.creation_date,
    }
  }
}
module.exports = StoreMapping;

const StoreEndpoints = require('./store-endpoint');
const StoreConstants = require('./store-constant');
const axios = require('axios');

class StoreService {
  static getStores(currentLib, tokenObj, params) {
    const { baseUrl, clientId } = currentLib.credentials;
    const finalUrl = baseUrl + StoreEndpoints.retrieveEndpoint(
      currentLib.credentials.version,
      StoreConstants.methods.get,
      { clientId },
    );

    return axios({
      method: 'get',
      url: finalUrl,
      headers: {
        Authorization: `${tokenObj.token}`
      },
    });
  }
}
module.exports = StoreService;

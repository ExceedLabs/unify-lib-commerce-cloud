const StoreMapping = require('./store-mapping');

class StoreConverter {
  static getDataMapToStore(obj, storeData) {
    const { fieldsCore } = obj.privateLibraries.Converters;
    const { libObjId } = obj;

    const map = {
      list: libObjId,
      item: {
        ...StoreMapping.getStoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: storeData,
    };

    return {
      map,
      data,
    };
  }
}
module.exports = StoreConverter;

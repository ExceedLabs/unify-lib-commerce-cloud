const { methods } = require('./store-constant');

class StoreEndpoints {
  static retrieveEndpoint(version, method, params) {
    switch (method) {
      case methods.get:
        return `/s/-/dw/data/${version}/inventory_lists?select=(**)`;
      default:
        throw new Error('method.not.allowed.error');
    }
  }
}

module.exports = StoreEndpoints;

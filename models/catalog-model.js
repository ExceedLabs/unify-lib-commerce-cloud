const AuthDemandwareModel = require('./auth-demandware-model');
const CatalogService = require('../services/commerce/catalog/catalog-service');
const CatalogConverter = require('../services/commerce/catalog/catalog-converter');
const ProductConverter = require('../services/commerce/product/product-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class Catalog {
  static async searchCatalog(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await CatalogService
        .searchCatalog(currentLib, { token: loginObject }, query)
        .catch(async (error) => {
          console.error('Error on searching product! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return CatalogService.searchCatalog(newCurrentLib, { token: newTokenOnBehalf.token }, query);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = CatalogConverter.getDataMapToCore(obj, retrievedCustomer.data.hits);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching product by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async searchCatalogProduct(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await CatalogService
        .searchCatalogProduct(currentLib, { token: loginObject }, params)
        .catch(async (error) => {
          console.error('Error on searching product! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return CatalogService.searchCatalogProduct(newCurrentLib, { token: newTokenOnBehalf.token }, params);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductConverter.getDataMapToCore(obj, retrievedCustomer.data.hits);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching product by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }
}

module.exports = Catalog;

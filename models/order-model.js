const _ = require('lodash');
const AuthDemandwareModel = require('./auth-demandware-model');
const OrderService = require('../services/commerce/order/order-service');
const OrderConverter = require('../services/commerce/order/order-converter');
const PaymentMethodConverter = require('../services/commerce/payment-method/payment-method-converter');
const PaymentInstrumentConverter = require('../services/commerce/payment-instrument/payment-instrument-converter');
const ShipmentConverter = require('../services/commerce/shipment/shipment-converter');
const CustomerService = require('../services/customer/customer-service');
const ProductItemsConverter = require('../services/commerce/product-items/product-items-converter');
const AddressConverter = require('../services/commerce/address/address-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');
const OMSOrderModel = require('./oms-order-model');
const OrderConstants = require('../services/commerce/order/order-constant');

const processOMSOrder = async (obj, currentLib, dataReplaced) => {
  try {
    const omsOrder = await OMSOrderModel.createOrderOMS(obj, currentLib, dataReplaced);
    const omsOrderId = omsOrder.data.id;
    const objCloned = _.cloneDeep(obj);
    objCloned.req.body = { c_sterlingOmsOrderNumber: omsOrderId };
    objCloned.req.params = { orderNumber: dataReplaced.order_no };
    OrderModel.updateOrder(objCloned, currentLib);
  } catch (e) {
    console.log('error on creating order in OMS');
  }
};

const listOMSOrderStatus = async (obj, currentLib, orderOMSId) => {
  try {
    if (!orderOMSId) {
      return 'not_shipped';
    }
    const omsOrder = await OMSOrderModel.listStatusShipmentOrderOMS(obj, orderOMSId);
    return omsOrder.data.Status;
  } catch (e) {
    return 'not_shipped';
  }
};

const createOrder = async (currentLib, params) => {
  const {
    query, loginObject, payload, PerformRefreshToken, obj,
    MessageService,
  } = params;
  const createOrderResult = await OrderService.createOrder(
    currentLib,
    { token: loginObject.token },
    { ...query, ...loginObject, payload: payload[0] },
  ).catch(async (error) => {
    console.error('Error on creating order! Details: ');
    console.error(error);
    const result = await GenericHelper.performRefreshToken({
      PerformRefreshToken,
      currentLib,
      tokenValueBearer: obj.req.headers.authorization,
      status: error.response.status,
      callback: async (newCurrentLib) => {
        let newTokenOnBehalf = {};
        if (params.customerId) {
          newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
        } else {
          newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
        }
        return OrderService.createOrder(
          newCurrentLib,
          newTokenOnBehalf,
          { ...query, ...newTokenOnBehalf, payload: payload[0] },
        );
      },
    });

    if (result) {
      return result;
    }
    throw (error.response.data.fault, error.response.status, error.message);
  });
  if (GenericHelper.isSuccess(createOrderResult.status)) {
    if (payload[0][OrderConstants.fields.c_payworksTransactionId]) {
      const updateOrderWithPayworksInfo = await OrderService.AddPayworksInfoToOrder(
        currentLib,
        loginObject,
        {
          ...payload[0],
          [OrderConstants.fields.order_no]: createOrderResult.data[OrderConstants.fields.order_no],
        },
      ).catch(async (error) => {
        console.error('Error on updating order! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            let newTokenOnBehalf = {};
            if (params.customerId) {
              newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            } else {
              newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
            }
            return OrderService.AddPayworksInfoToOrder(
              newCurrentLib,
              newTokenOnBehalf,
              payload[0],
            );
          },
        });

        if (result) {
          return result;
        }
        throw (error.response.data.fault, error.response.status, error.message);
      });
      if (GenericHelper.isSuccess(updateOrderWithPayworksInfo.status)) {
        const newResult = {
          ...updateOrderWithPayworksInfo.data,
          ...createOrderResult.data,
        };
        return MessageService.getSuccess(newResult, updateOrderWithPayworksInfo.status);
      }
    }
    return MessageService.getSuccess(createOrderResult.data, createOrderResult.status);
  }
  throw (createOrderResult);
};

class OrderModel {
  static async searchOrder(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    let customerInfo = null;
    let loginObject = null;
    try {
      if (params && params.customerId && GenericHelper.isEmail(params.customerId)) {
        loginObject = { token: currentLib.credentials.BMToken };
        const result = await CustomerService
          .getAllCustomers(currentLib, loginObject, { email: params.customerId });
        if (result.data && result.data.total > 0) {
          customerInfo = result.data.hits[0].data;
        }
        params.customerNumber = customerInfo.customer_no;
      } else {
        loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      }

      const retrievedCustomer = await OrderService
        .searchOrder(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on searching order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.searchOrder(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = OrderConverter.getDataMapToCore(obj, _.map(retrievedCustomer.data.hits, hit => hit.data));
        const dataReplaced = Converters.replaceFields(data, map);
        const dateReplacedSorted = dataReplaced.sort((a, b) => ((a.updatedAt < b.updatedAt) ? 0 : -1));

        if (obj.currentObjMethod.oms) {
          const promises = [];
          _.each(dateReplacedSorted, (dt) => {
            const promise = listOMSOrderStatus(obj, currentLib, dt.c_sterlingOmsOrderNumber);
            promises.push(promise);
          });

          const results = await Promise.all(promises).catch(error => error);
          _.each(results, (result, index) => {
            console.log(result);
            dateReplacedSorted[index].shippingStatus = result;
          });
          return MessageService.getSuccess(dateReplacedSorted, retrievedCustomer.status);
        }
        return MessageService.getSuccess(dateReplacedSorted, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error(error);
      console.error('Error on searching order by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async createOrder(obj, currentLib) {
    const {
      MessageService, Converters, PerformRefreshToken, currentObjMethod,
    } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    let loginObject = {};
    try {
      if (params.customerId) {
        loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      } else {
        loginObject = { token: currentLib.credentials.BMToken };
      }
      const { map, data } = OrderConverter.getDataMapToOrder(obj, [body]);
      const dataReplaced = Converters.replaceFields(data, map);
      const payload = GenericHelper.removeAttributesNull(dataReplaced);
      const placeOrderResult = await createOrder(
        currentLib,
        {
          query,
          loginObject,
          payload,
          PerformRefreshToken,
          obj,
          MessageService,
        },
      ).catch((error) => {
        console.error('Error on creating order!');
        throw (MessageService.getError(error.stack, error.status, error.message));
      });

      if (GenericHelper.isSuccess(placeOrderResult.status)) {
        const { map, data } = OrderConverter.getDataMapToCore(obj, [placeOrderResult.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        if (obj.currentObjMethod && obj.currentObjMethod.oms) {
          processOMSOrder(obj, currentLib, placeOrderResult.data);
        }
        return MessageService.getSuccess(dataReplaced[0], placeOrderResult.status);
      }
      return placeOrderResult;
    } catch (error) {
      console.error('Error on creating order by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async getOrderByNumber(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    let customerInfo = null;
    let loginObject = null;
    try {
      if (params && params.customerId && GenericHelper.isEmail(params.customerId)) {
        loginObject = { token: currentLib.credentials.BMToken };
        const result = await CustomerService.getAllCustomers(currentLib, loginObject, { email: params.customerId });
        if (result.data && result.data.total > 0) {
          customerInfo = result.data.hits[0].data;
        }
        params.customerNumber = customerInfo.customer_no;
      } else {
        loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      }

      const retrievedCustomer = await OrderService
        .getOrderByNumber(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on searching order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.getOrderByNumber(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = OrderConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        const result = (dataReplaced && dataReplaced.length) ? dataReplaced[0] : {};
        if (obj.currentObjMethod && obj.currentObjMethod.oms && dataReplaced[0].c_sterlingOmsOrderNumber) {
          result.shippingStatus = await listOMSOrderStatus(obj, currentLib, dataReplaced[0].c_sterlingOmsOrderNumber);
        }
        return MessageService.getSuccess(result, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching order by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async getPaymentMethod(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      const retrievedCustomer = await OrderService
        .getPaymentMethod(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on searching order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.getPaymentMethod(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentMethodConverter.getDataMapToCore(obj, retrievedCustomer.data.applicable_payment_methods);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching order by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async getPaymentInstrument(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      const retrievedCustomer = await OrderService
        .getPaymentInstrument(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on getting order payment instrument! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.getPaymentInstrument(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...loginObject });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentInstrumentConverter.getDataMapToCore(obj, retrievedCustomer.data.payment_instruments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on getting order payment instrument');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async createPaymentInstrument(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));

      const paymentToSave = {
        payment_card: {
          expiration_year: body.expirationYear,
          expiration_month: body.expirationMonth,
          number: body.cardNumber,
          holder: body.holder,
          card_type: body.cardType,
        },
        payment_method_id: body.paymentMethodId,
      };

      const infoParams = {
        ...query, ...params, loginObject, payload: paymentToSave,
      };

      const retrievedCustomer = await OrderService
        .createPaymentInstrument(currentLib, loginObject, infoParams)
        .catch(async (error) => {
          console.error('Error on posting order payment instrument! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.createPaymentInstrument(newCurrentLib, newTokenOnBehalf, infoParams);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentInstrumentConverter.getDataMapToCore(obj, retrievedCustomer.data.payment_instruments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on posting order payment instrument');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async updateOrder(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const infoParams = {
        ...query, ...params, loginObject, payload: body,
      };

      const retrievedCustomer = await OrderService
        .updateOrder(currentLib, loginObject, infoParams)
        .catch(async (error) => {
          console.error('Error on updating order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return OrderService.updateOrder(newCurrentLib, newTokenOnBehalf, infoParams);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = OrderConverter.getDataMapToCore(obj, _.map(retrievedCustomer.data.hits, hit => hit.data));
        const dataReplaced = Converters.replaceFields(data, map);
        const dateReplacedSorted = dataReplaced.sort((a, b) => ((a.updatedAt < b.updatedAt) ? 0 : -1));
        return MessageService.getSuccess(dateReplacedSorted, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on updating order');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listProductByOrder(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      const retrievedCustomer = await OrderService
        .getOrderByNumber(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on getting products from order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.getOrderByNumber(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductItemsConverter.getDataMapToCore(obj, retrievedCustomer.data.product_items);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on getting products from order');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBillingAddressByOrder(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      const retrievedCustomer = await OrderService
        .getOrderByNumber(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on getting billing address from order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.getOrderByNumber(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = AddressConverter.getDataMapToCore(obj, [retrievedCustomer.data.billing_address]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on getting billing address from order');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listShipmentAddressByOrder(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => ({ token: 'invalid', status: error.status }));
      const retrievedCustomer = await OrderService
        .getOrderByNumber(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on getting shipment address from order! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return OrderService.getOrderByNumber(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ShipmentConverter.getDataMapToCore(obj, retrievedCustomer.data.shipments);
        const shipments = GenericHelper.getShipingAddressesFromShipments(Converters.fieldsCore, retrievedCustomer.data.shipments);
        const dataReplaced = Converters.replaceFields(data, map);
        const mixedData = _.map(dataReplaced, (dt, index) => ({
          ...dt,
          shippingAddress: shipments[index],
        }));
        return MessageService.getSuccess(mixedData, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on getting billing address from order');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }
}

module.exports = OrderModel;

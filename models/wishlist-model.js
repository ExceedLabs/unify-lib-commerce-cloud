const AuthDemandwareModel = require('./auth-demandware-model');
const WishlistService = require('../services/commerce/wishlist/wishlist-service');
const WishlistConverter = require('../services/commerce/wishlist/wishlist-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class WishlistModel {
  static async listWishlist(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await WishlistService
        .listWishlist(currentLib, loginObject, { email: query.email })
        .catch(async (error) => {
          console.error('Error on getting wishlist! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return WishlistService.listWishlist(newCurrentLib, newTokenOnBehalf, { email: query.email });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = WishlistConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list wishlist');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listWishlistById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await WishlistService
        .listWishlistById(currentLib, loginObject, { id: params.id })
        .catch(async (error) => {
          console.error('Error on getting wishlist by id! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return WishlistService.listWishlistById(newCurrentLib, newTokenOnBehalf, { id: params.id });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = WishlistConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list wishlist by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }
}

module.exports = WishlistModel;

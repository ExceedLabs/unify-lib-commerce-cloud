const _ = require('lodash');
const axios = require('axios');

class OMSOrderModel {
  static async createOrderOMS(obj, currentLib, payload) {
    try {
      const URL = `${process.env.BACK_END_URL}/oms/commerce/customer/rsmith@yahoo.com/order`;
      const config = {
        headers: {
            'context-type': 'application/json',
            'Authorization': obj.req.headers.authorization,
          }
        };
      const result = await axios.post(URL, payload, config);
      return result.data;
    } catch (error) {
      console.error(error);
      console.error('Error on creating OMS order');
    }
  }

  static async listStatusShipmentOrderOMS(obj, orderOMSId) {
    try {
      const URL = `${process.env.BACK_END_URL}/oms/commerce/customer/rsmith@yahoo.com/order/${orderOMSId}/status-shipment`;
      const config = {
        headers: {
            'context-type': 'text/xml',
            'Authorization': obj.req.headers.authorization,
          }
        };
      const result = await axios.get(URL, config);
      return result.data;
    } catch (error) {
      console.error(error);
      console.error('Error on getting OMS Shipment Status Order');
    }
  }
}

module.exports = OMSOrderModel;

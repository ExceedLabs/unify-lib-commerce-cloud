const _ = require('lodash');
const ProductService = require('../services/commerce/product/product-service');
const ProductConverter = require('../services/commerce/product/product-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class Product {
  static async listProduct(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await ProductService.getProduct(currentLib, loginObject, params)
        .catch(async (error) => {
          console.error('Error on getting product by id! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              return ProductService.getProduct(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, params);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

        // get variations
        // check if exists variation
        // if true -> must pass the variation ID


      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);

        const images = GenericHelper.getFormattedImages(obj, [retrievedCustomer.data]);
        const imagesReplaced = Converters.replaceFields(images.data, images.map);

        const variationsReplaced = GenericHelper.getVariations(obj, retrievedCustomer.data);

        const promotionsReplaced = GenericHelper.getPromotions(Converters.fieldsCore, retrievedCustomer.data.product_promotions);

        const inventory = GenericHelper.getInvetory(Converters.fieldsCore, retrievedCustomer.data.inventory);

        const setFinalData = _.map(dataReplaced, (d, index) => {
          return {
            ...d,
            ...variationsReplaced,
            ...promotionsReplaced,
            inventory: { ...inventory },
            images: { ...imagesReplaced[index] },
          };
        });

        return MessageService.getSuccess(setFinalData[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list product by id');
      throw(MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async searchProduct(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await ProductService
        .searchProduct(currentLib, loginObject, query)
        .catch(async (error) => {
          console.error('Error on searching product! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              return ProductService.searchProduct(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, query);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductConverter.getSearchDataMapToCore(obj, retrievedCustomer.data.hits);
        const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);

        const variationsReplaced = _.map(retrievedCustomer.data.hits, hit => {
          return GenericHelper.getVariations(obj, hit);
        });

        const images = _.map(retrievedCustomer.data.hits, hit => {
          return GenericHelper.getProductSearchImage(Converters.fieldsCore, hit);
        });

        const setFinalData = _.map(dataReplaced, (d, index) => {
          return {
            ...d,
            ...variationsReplaced[index],
            ...images[index],
          };
        });

        return MessageService.getSuccess(setFinalData, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching product');
      throw(MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async getProductInventory(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;
    try {
      if(!currentLib.credentials.DataToken) {
        currentLib = await GenericHelper.performDataRefreshToken(
          {
            currentLib,
            PerformRefreshToken,
            status: 401,
            tokenValueBearer: obj.req.headers.authorization,
            callback: (newCurrentLib) => {return newCurrentLib;},
          }
        );
      }
      const loginObject = { token: currentLib.credentials.DataToken };

      const retrieveInventory = await ProductService.getProductInventory(
        currentLib,
        loginObject,
        params,)
        .catch(async (error) => {
          console.error('Error on getting product inventory! Details: ');
          console.error(error);
          const result = await GenericHelper.performDataRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              return ProductService.getProductInventory(newCurrentLib, { token: newCurrentLib.credentials.DataToken }, params);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrieveInventory.status)) {
        const { map, data } = ProductConverter.getDataMapToProductInventory(obj, [retrieveInventory.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrieveInventory.status);
      }
      else {
        if(retrieveInventory.status.toString() === '404') {
          return MessageService.getSuccess([], 200);
        }
        return MessageService.getError(retrieveInventory, retrieveInventory.status);
      }
    } catch (error) {
      console.error('Error on searching product');
      return MessageService.getError(error.stack, 500, error.message);
    }
  }
}

module.exports = Product;

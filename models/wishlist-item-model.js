const _ = require('lodash');
const WishlistItemService = require('../services/commerce/wishlist-item/wishlist-item-service');
const WishlistItemConverter = require('../services/commerce/wishlist-item/wishlist-item-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class WishlistItemsModel {
  static async listWishlistItem(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params, query } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await WishlistItemService
        .listWishlistItem(currentLib, loginObject, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on getting wishlist items! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return WishlistItemService.listWishlistItem(newCurrentLib, newTokenOnBehalf, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = WishlistItemConverter.getDataMapToCore(obj, retrievedCustomer.data.product_list_items);
        const dataReplaced = Converters.replaceFields(data, map);

        const images = WishlistItemConverter.getImagesDataMapToCore(obj, retrievedCustomer.data.product_list_items);
        const imagesReplaced = Converters.replaceFields(images.data, images.map);

        const setFinalData = _.map(dataReplaced, (d, index) => {
          d.product[0] = {
            ...d.product[0],
            images: {
              ...imagesReplaced[index]
            }
          };
          delete d.product[0].imageGroups;
          return d;
        });

        return MessageService.getSuccess(setFinalData, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list wishlist items');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listWishlistItemById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params, query } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await WishlistItemService
        .listWishlistItemById(currentLib, loginObject, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on getting wishlist items! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return WishlistItemService.listWishlistItemById(newCurrentLib, newTokenOnBehalf, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);

        const images = WishlistItemConverter.getImagesDataMapToCore(obj, [retrievedCustomer.data]);
        const imagesReplaced = Converters.replaceFields(images.data, images.map);

        const setFinalData = _.map(dataReplaced, (d, index) => {
          return { ...d, images: { ...imagesReplaced[index] } };
        });

        return MessageService.getSuccess(setFinalData[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list wishlist items');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }
}

module.exports = WishlistItemsModel;

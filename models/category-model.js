const AuthDemandwareModel = require('./auth-demandware-model');
const CustomerService = require('../services/customer/customer-service');
const CategoryService = require('../services/commerce/category/category-service');
const CategoryConverter = require('../services/commerce/category/category-converter');
const ProductConverter = require('../services/commerce/product/product-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class Category {
  static async listCategoryProducts(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await CategoryService
        .searchCategoryProduct(currentLib, { token: loginObject }, { q: params.categoryId })
        .catch(async (error) => {
          console.error('Error on getting category products! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return CategoryService.searchCategoryProduct(newCurrentLib, { token: newTokenOnBehalf.token }, { q: params.categoryId });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductConverter.getDataMapToCore(obj, retrievedCustomer.data.hits);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list category products');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async searchCategory(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await CategoryService
        .searchCategory(currentLib, { token: loginObject }, query)
        .catch(async (error) => {
          console.error('Error on searching product! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return CategoryService.searchCategory(newCurrentLib, { token: newTokenOnBehalf.token }, query);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = CategoryConverter.getDataMapToCore(obj, retrievedCustomer.data.hits);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching product by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listCategories(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await CategoryService
        .listCategories(currentLib, { token: loginObject }, params)
        .catch(async (error) => {
          console.error('Error on getting categories! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return CategoryService.listCategories(newCurrentLib, { token: newTokenOnBehalf.token }, params);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = CategoryConverter.getDataMapToCore(obj, retrievedCustomer.data.categories);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list categories');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listCategoriesById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await CategoryService
        .listCategoriesById(currentLib, { token: loginObject }, params)
        .catch(async (error) => {
          console.error('Error on getting category by id! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return CategoryService.listCategoriesById(newCurrentLib, { token: newTokenOnBehalf.token }, params);
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = CategoryConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error to list category by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }
}

module.exports = Category;

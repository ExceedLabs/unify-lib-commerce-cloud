const AuthDemandwareModel = require('./auth-demandware-model');
const CustomerService = require('../services/customer/customer-service');
const CustomerConverter = require('../services/customer/customer-converter');
const BasketConverter = require('../services/commerce/basket/basket-converter');
const OrderConverter = require('../services/commerce/order/order-converter');
const PaymentInstrumentConverter = require('../services/commerce/payment-instrument/payment-instrument-converter');
const PaymentInstrumentService = require('../services/commerce/payment-instrument/payment-instrument-service');
const WishlistConverter = require('../services/commerce/wishlist/wishlist-converter');
const WishlistItemConverter = require('../services/commerce/wishlist-item/wishlist-item-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');
const ImageHelper = require('../services/helpers/unify-helpers/image-helper');

exports.listCustomer = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService.getCustomer(currentLib, loginObject)
      .catch(async (error) => {
        console.error('Error on getting customer! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.getCustomer(newCurrentLib, { token: newTokenOnBehalf.token });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = CustomerConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      const resultFinal = (dataReplaced.length) ? dataReplaced[0] : {};
      return MessageService.getSuccess(resultFinal, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listAllCustomers = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  try {
    const params = obj.req.query;
    const loginObject = { token: currentLib.credentials.BMToken };

    const retrievedCustomer = await CustomerService.getAllCustomers(currentLib, loginObject, params)
      .catch(async (error) => {
        console.error('Error on getting customer! Details: ');
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            return CustomerService.getAllCustomers(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (retrievedCustomer && retrievedCustomer.data && retrievedCustomer.data.count === 0) {
      return MessageService.getError([], 404, 'customers not found');
    }

    if (retrievedCustomer) {
      const filteredCustomer = CustomerService.getHitsFromCustomerData(retrievedCustomer);

      if (filteredCustomer && filteredCustomer.data.length && GenericHelper.isSuccess(filteredCustomer.status)) {
        const { map, data } = CustomerConverter.getDataMapToCore(obj, filteredCustomer.data);
        const resultFinal = Converters.replaceFields(data, map);
        return MessageService.getSuccess(resultFinal, filteredCustomer.status);
      }
      return MessageService.getError(filteredCustomer, 404, 'customers not found');
    }
  } catch (error) {
    console.error('Error to list customer');
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.createCustomer = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  try {
    const loginObject = { token: currentLib.credentials.BMToken };
    const payload = obj.req.body;

    const { map, data } = CustomerConverter.getDataMapToCustomer(obj, [payload]);
    const dataReplaced = Converters.replaceFields(data, map);
    const payloadReplaced = {
      customer: dataReplaced[0],
    };

    const createdCustomer = await CustomerService
      .createCustomer(currentLib, loginObject, payloadReplaced)
      .catch(async (error) => {
        console.error('Error on creating customer! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            return CustomerService.createCustomer(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, payloadReplaced);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });
    if (GenericHelper.isSuccess(createdCustomer.status)) {
      const { data, map } = CustomerConverter.getDataMapToCore(obj, [createdCustomer.data]);
      const responseReplaced = Converters.replaceFields(data, map);
      const resultFinal = (responseReplaced.length) ? responseReplaced[0] : {};
      return MessageService.getSuccess(resultFinal, createdCustomer.status);
    }
    return createdCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.updateCustomer = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  try {
    const loginObject = { token: currentLib.credentials.BMToken };
    const { customerId } = obj.req.params;
    const payload = obj.req.body;

    const { map, data } = CustomerConverter.getDataMapToCustomer(obj, [payload]);
    const dataReplaced = Converters.replaceFields(data, map);
    const dataReplacedWithoutNull = CustomerService.removeAttributesNull(dataReplaced);
    const payloadReplaced = dataReplacedWithoutNull[0];
    if (!payloadReplaced) { return {}; }
    const dataToUpdate = {
      payload: payloadReplaced,
      customerId,
    };
    const updatedCustomer = await CustomerService.updateCustomer(currentLib, loginObject, dataToUpdate)
      .catch(async (error) => {
        console.error('Error on updating customer! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            return CustomerService.updateCustomer(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, dataToUpdate);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(updatedCustomer.status)) {
      const { map, data } = CustomerConverter.getDataMapToCore(obj, [updatedCustomer.data]);
      const responseReplaced = Converters.replaceFields(data, map);
      const resultFinal = (responseReplaced.length) ? responseReplaced[0] : {};
      return MessageService.getSuccess(resultFinal, updatedCustomer.status);
    }
    return updatedCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.createCustomerAddress = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  try {
    const loginObject = { token: currentLib.credentials.BMToken };
    const payload = obj.req.body;
    const { customerId } = obj.req.params;

    const { map, data } = CustomerConverter.getAddressDataMapToCustomer(obj, [payload]);
    const dataReplaced = Converters.replaceFields(data, map);

    const createdCustomer = await CustomerService
      .createCustomerAddress(currentLib, { token: loginObject, customerId }, dataReplaced[0])
      .catch(async (error) => {
        console.error('Error on creating customer addresses! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            return CustomerService.createCustomerAddress(newCurrentLib, {
              token: newCurrentLib.credentials.BMToken,
              customerId,
            }, dataReplaced[0]);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });
    if (GenericHelper.isSuccess(createdCustomer.status)) {
      const { map, data } = CustomerConverter.getAddressDataMapToCore(obj, [createdCustomer.data]);
      const responseReplaced = Converters.replaceFields(data, map);
      const resultFinal = (responseReplaced.length) ? responseReplaced[0] : {};
      return MessageService.getSuccess(resultFinal, createdCustomer.status);
    }
    return createdCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.createCustomerPaymentInstruments = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = { token: currentLib.credentials.BMToken };

    const paymentToSave = {
      "payment_card":{
        "expiration_year": body.expirationYear,
        "expiration_month": body.expirationMonth,
        "number": body.cardNumber,
        "holder": body.holder,
        "card_type": body.cardType
      },
      "payment_method_id": body.paymentMethodId,
    };

    const infoParams = { ...query, ...params, loginObject, payload: paymentToSave };

    const createdCustomer = await CustomerService
      .createCustomerPaymentInstruments(currentLib, loginObject, infoParams)
      .catch(async (error) => {
        console.error('Error on creating customer payment instruments! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            return CustomerService.createCustomerPaymentInstruments(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, infoParams);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(createdCustomer.status)) {
      const { data, map } = PaymentInstrumentConverter.getDataMapToCore(obj, [createdCustomer.data]);
      const responseReplaced = Converters.replaceFields(data, map);
      const resultFinal = (responseReplaced.length) ? responseReplaced[0] : {};
      return MessageService.getSuccess(resultFinal, createdCustomer.status);
    }
    return createdCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.createCustomerWishlists = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const payload = await WishlistItemConverter.getDataMapToWishlistItem(obj, body, loginObject);
    const infoParams = { ...params, payload };

    const retrievedCustomer = await CustomerService
      .createCustomerWishlists(currentLib, loginObject, params.customerId, infoParams)
      .catch(async (error) => {
        console.error('Error on creating customer wishlist! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.createCustomerWishlists(newCurrentLib, { token: newTokenOnBehalf.token }, params.customerId, infoParams);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to creating customer wishlist');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.createCustomerWishlistItems = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const { data, map } = WishlistItemConverter.getDataMapSaveToWishlistItem(obj, [body]);
    const payload = Converters.replaceFields(data, map);
    const infoParams = { ...params, payload: payload[0] };

    const retrievedCustomer = await CustomerService
      .createCustomerWishlistItems(currentLib, loginObject, params.customerId, infoParams)
      .catch(async (error) => {
        console.error('Error on creating customer wishlist items! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.createCustomerWishlistItems(newCurrentLib, { token: newTokenOnBehalf.token }, params.customerId, infoParams);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to creating customer wishlist items');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.createCustomerPurchases = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { body, params } = obj.req;
  const infoParams = { ...params, payload: body };

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .createCustomerPurchases(currentLib, loginObject, params.customerId, infoParams)
      .catch(async (error) => {
        console.error('Error on creating customer purchases! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.createCustomerPurchases(newCurrentLib, { token: newTokenOnBehalf.token }, params.customerId, infoParams);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to creating customer purchases');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerAddresses = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  try {
    const { customerId } = obj.req.params;
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .getCustomerAddress(currentLib, { ...loginObject, customerId })
      .catch(async (error) => {
        console.error('Error on getting customer! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.getCustomerAddress(newCurrentLib, { ...newTokenOnBehalf, customerId });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = CustomerConverter.getAddressDataMapToCore(obj, retrievedCustomer.data.data);
      const resultFinal = Converters.replaceFields(data, map);
      return MessageService.getSuccess(resultFinal, 200);
    }
    return retrievedCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.listCustomerAddressesById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { params } = obj.req;
  try {
    const { customerId } = obj.req.params;
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerAddressesById(currentLib, { ...loginObject }, customerId, params)
      .catch(async (error) => {
        console.error('Error on getting customer addresses by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerAddressesById(newCurrentLib, { ...newTokenOnBehalf }, customerId, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = CustomerConverter.getAddressDataMapToCore(obj, [retrievedCustomer.data]);
      const resultFinal = Converters.replaceFields(data, map);
      return MessageService.getSuccess(resultFinal[0], 200);
    }
    return retrievedCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.listCustomerBaskets = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerBaskets(currentLib, loginObject, params.customerId)
      .catch(async (error) => {
        console.error('Error on getting customer baskets! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerBaskets(newCurrentLib, { ...newTokenOnBehalf }, params.customerId);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = BasketConverter.getDataMapToCore(obj, retrievedCustomer.data.baskets);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer baskets');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerOrders = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerOrders(currentLib, loginObject, params.customerId)
      .catch(async (error) => {
        console.error('Error on getting customer orders! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerOrders(newCurrentLib, { ...newTokenOnBehalf }, params.customerId);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = OrderConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer orders');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerPaymentInstruments = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerPaymentInstruments(currentLib, loginObject, params.customerId)
      .catch(async (error) => {
        console.error('Error on getting customer payment instruments! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerPaymentInstruments(newCurrentLib, { ...newTokenOnBehalf }, params.customerId);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = PaymentInstrumentConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer payment insturments');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerPurchases = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerPurchases(currentLib, loginObject, params.customerId, params)
      .catch(async (error) => {
        console.error('Error on getting customer purchases! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerPurchases(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer purchases');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerWishlists = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerWishlists(currentLib, loginObject, params.customerId)
      .catch(async (error) => {
        console.error('Error on getting customer wishlists! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerWishlists(newCurrentLib, { ...newTokenOnBehalf }, params.customerId);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer wishlists');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerAbandonedCart = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      /** this method name is the same name in wishlist. I need only interate over values and get the event type and event type value */
      .listCustomerWishlists(currentLib, loginObject, params.customerId)
      .catch(async (error) => {
        console.error('Error on getting customer abandoned cart! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerWishlists(newCurrentLib, { ...newTokenOnBehalf }, params.customerId);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      const abandonedCarts = GenericHelper.getAbandonedCarts(dataReplaced);
      return MessageService.getSuccess(abandonedCarts, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer  abandoned cart');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerWishlistsById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerWishlistsById(currentLib, loginObject, params.customerId, params)
      .catch(async (error) => {
        console.error('Error on getting customer wishlists by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerWishlistsById(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer wishlists by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerWishlistItems = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerWishlistItems(currentLib, loginObject, params.customerId, params)
      .catch(async (error) => {
        console.error('Error on getting customer wishlist items! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerWishlistItems(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      if (!retrievedCustomer.data.count) {
        return MessageService.getSuccess([], retrievedCustomer.status);
      }
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, retrievedCustomer.data.data);
      const dataReplaced = Converters.replaceFields(data, map);
      const resultFinalWithImagesMixed = ImageHelper.getImages(Converters, obj, retrievedCustomer.data, dataReplaced, WishlistItemConverter);
      return MessageService.getSuccess(resultFinalWithImagesMixed, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer wishlist items');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.listCustomerWishlistItemsById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .listCustomerWishlistItemsById(currentLib, loginObject, params.customerId, params)
      .catch(async (error) => {
        console.error('Error on getting customer wishlist items by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.listCustomerWishlistItemsById(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);

      const resultFinalWithImagesMixed = ImageHelper.getImages(Converters, obj, retrievedCustomer, dataReplaced, WishlistItemConverter);

      return MessageService.getSuccess(resultFinalWithImagesMixed[0], retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to list customer wishlist items  by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.deleteCustomerAddressById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { params } = obj.req;
  try {
    const { customerId } = obj.req.params;
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const retrievedCustomer = await CustomerService
      .deleteCustomerAddressById(currentLib, { ...loginObject }, customerId, params)
      .catch(async (error) => {
        console.error('Error on removing customer addresses by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.deleteCustomerAddressById(newCurrentLib, { ...newTokenOnBehalf }, customerId, params);
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      return MessageService.getSuccess({}, 200, "Address removed successfully");
    }
    return retrievedCustomer;
  } catch (error) {
    throw(MessageService.getError(error.stack, 500, error.message));
  }
};

exports.deleteCustomerWishlistItemById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });

    const retrievedCustomer = await CustomerService
      .deleteCustomerWishlistItemById(currentLib, loginObject, params.customerId, { ...params, ...query })
      .catch(async (error) => {
        console.error('Error on removing customer wishlist item by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.deleteCustomerWishlistItemById(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, { ...params, ...query });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      return MessageService.getSuccess({}, 200, 'wishlist item removed successufully');
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to removing customer wishlist item by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.deleteCustomerWishlistsById = async (obj, currentLib) => {
  const { MessageService, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });

    const retrievedCustomer = await CustomerService
      .deleteCustomerWishlistsById(currentLib, loginObject, params.customerId, { ...params, ...query })
      .catch(async (error) => {
        console.error('Error on removing customer wishlist by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.deleteCustomerWishlistsById(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, { ...params, ...query });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      return MessageService.getSuccess({}, 200, 'wishlist removed successfully');
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to removing customer wishlist by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.deleteCustomerPaymentInstruments = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });

    const retrievedCustomer = await CustomerService
      .deleteCustomerPaymentInstruments(currentLib, loginObject, params.customerId, { ...params, ...query })
      .catch(async (error) => {
        console.error('Error on removing customer payment instrument by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.deleteCustomerPaymentInstruments(newCurrentLib, { ...newTokenOnBehalf }, params.customerId, { ...params, ...query });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      return MessageService.getSuccess({}, 200, 'payment instrument removed successfully');
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to removing customer payment instrument by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.updateCustomerWishlistById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });

    const retrievedCustomer = await CustomerService
      .updateCustomerWishlistById(currentLib, loginObject, params.customerId, { ...params, ...query, payload: body })
      .catch(async (error) => {
        console.error('Error on updating customer wishlist by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.updateCustomerWishlistById(newCurrentLib, newTokenOnBehalf, params.customerId, { ...params, ...query, payload: body });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to updating customer wishlist by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.updateCustomerWishlistItemsById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });

    const retrievedCustomer = await CustomerService
      .updateCustomerWishlistItemsById(currentLib, loginObject, params.customerId, { ...params, ...query, payload: body })
      .catch(async (error) => {
        console.error('Error on updating customer wishlist item by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.updateCustomerWishlistItemsById(newCurrentLib, newTokenOnBehalf, params.customerId, { ...params, ...query, payload: body });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to removing customer wishlist item by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.updateCustomerAddressById = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
    const dataMap = CustomerConverter.getAddressDataMapToCustomer(obj, [body]);
    const payload = Converters.replaceFields(dataMap.data, dataMap.map);
    const a = GenericHelper.removeAttributesNull(payload);

    const retrievedCustomer = await CustomerService
      .updateCustomerAddressById(currentLib, loginObject, params.customerId, { ...params, ...query, payload: a[0] })
      .catch(async (error) => {
        console.error('Error on updating customer address by id! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
            return CustomerService.updateCustomerAddressById(newCurrentLib, newTokenOnBehalf, params.customerId, { ...params, ...query, payload: a[0] });
          },
        });

        if (result) {
          return result;
        }
        return MessageService.getError(error.response.data.fault, error.response.status, error.message);
      });

    if (GenericHelper.isSuccess(retrievedCustomer.status)) {
      const { map, data } = CustomerConverter.getAddressDataMapToCore(obj, [retrievedCustomer.data]);
      const dataReplaced = Converters.replaceFields(data, map);
      return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
    }
    return retrievedCustomer;
  } catch (error) {
    console.error('Error to removing customer address item by id');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

exports.getAbandonedCarts = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
  const { query, params, body } = obj.req;

  try {
    const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib)
      .catch(error => { return { token: 'invalid', status: error.status } });
    const retrieveAbandonedCarts = await CustomerService.retrieveAbandonedCarts(
      currentLib,
      loginObject,
      params.account_id
    ).catch(async (error) => {
      console.error('Error on updating customer address by id! Details: ');
      console.error(error);
      const result = await GenericHelper.performRefreshToken({
        PerformRefreshToken,
        currentLib,
        tokenValueBearer: obj.req.headers.authorization,
        status: error.response.status,
        callback: async (newCurrentLib) => {
          const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
          return CustomerService.retrieveAbandonedCarts(newCurrentLib, newTokenOnBehalf, params.account_id, { ...params, ...query});
        },
      });

      if (result) {
        return result;
      }
      return MessageService.getError(error.response.data.fault, error.response.status, error.message);
    });

    if (GenericHelper.isSuccess(retrieveAbandonedCarts.status)) {
      if(retrieveAbandonedCarts.data.data && retrieveAbandonedCarts.data.data.length) {
        const abandonedCarts = retrieveAbandonedCarts.data.data.filter((cart) => cart.c_isAbandonedCart === true);
        const productsRemapped = abandonedCarts.map(cart => {
          const preMappedCart = CustomerConverter.applyAbandonedCartPreMapping(cart);
          const { map, data } = CustomerConverter.getAbandonedCartMapToCore(obj, [preMappedCart]);
          return Converters.replaceFields(data, map);
        });
        return MessageService.getSuccess(productsRemapped, retrieveAbandonedCarts.status);
      }
      return MessageService.getSuccess([], retrieveAbandonedCarts.status);
    } else {
      return MessageService.getError(retrieveAbandonedCarts.data, retrieveAbandonedCarts.status);
    }
  } catch (e) {
    console.error('Error retrieving customer abandoned carts');
    throw(MessageService.getError(error.stack, error.status, error.message));
  }
};

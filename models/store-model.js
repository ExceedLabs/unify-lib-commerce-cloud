const StoreService = require('../services/commerce/store/store-service');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');
const StoreConverter = require('../services/commerce/store/store-converter');

exports.getStores = async (obj, currentLib) => {
  const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;

  try {
    if(!currentLib.credentials.DataToken) {
      currentLib = await GenericHelper.performDataRefreshToken(
        {
          currentLib,
          PerformRefreshToken,
          status: 401,
          tokenValueBearer: obj.req.headers.authorization,
          callback: (newCurrentLib) => {return newCurrentLib;},
        }
      );
    }
    const loginObject = { token: currentLib.credentials.DataToken };

    const storesRetrieved = await StoreService.getStores(
      currentLib,
      loginObject,
    ).catch(async (e) => {
      const result = await GenericHelper.performDataRefreshToken({
        PerformRefreshToken,
        currentLib,
        tokenValueBearer: obj.req.headers.authorization,
        status: e.response.status,
        callback: async (newCurrentLib) => {
          return StoreService.getStores(newCurrentLib, { token: newCurrentLib.credentials.DataToken });
        },
      });

      if (result) {
        return result;
      }
      return MessageService.getError(e.response.data.fault, e.response.status, e.message);
    });

    if (GenericHelper.isSuccess(storesRetrieved.status)) {
      if(storesRetrieved.data.count) {
        const { map, data } = StoreConverter.getDataMapToStore(obj, storesRetrieved.data.data);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, storesRetrieved.status);
      }
      return MessageService.getSuccess([], storesRetrieved.status);
    }
    return MessageService.getError(storesRetrieved.data, storesRetrieved.status);

  } catch (error) {
    console.error('Error to list stores');
    return MessageService.getError(error.stack, error.status, error.message);
  }
};

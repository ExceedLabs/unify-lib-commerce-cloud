const AuthDemandwareModel = require('./auth-demandware-model');
const OrderService = require('../services/commerce/order/order-service');
const OrderConverter = require('../services/commerce/order/order-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class OrderModel {
  static async getOrderBotById(obj, currentLib) {
    const { MessageService } = obj.privateLibraries;
    const { params } = obj.req;
    try {
      return MessageService.getSuccess({ orderId: params.orderId }, 200);
    } catch (error) {
      console.error('Error on bot order id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }
}

module.exports = OrderModel;

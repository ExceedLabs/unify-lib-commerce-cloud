const _ = require('lodash');
const AuthDemandwareModel = require('./auth-demandware-model');
const CustomerService = require('../services/customer/customer-service');
const BasketService = require('../services/commerce/basket/basket-service');
const AddressConverter = require('../services/commerce/address/address-converter');
const BasketConverter = require('../services/commerce/basket/basket-converter');
const ProductConverter = require('../services/commerce/product/product-converter');
const ShipmentConverter = require('../services/commerce/shipment/shipment-converter');
const ShippingMethodConverter = require('../services/commerce/shipping/shipping-converter');
const ShippingItemsConverter = require('../services/commerce/shipping-items/shipping-items-converter');
const ProductItemsConverter = require('../services/commerce/product-items/product-items-converter');
const PaymentInstrumentConverter = require('../services/commerce/payment-instrument/payment-instrument-converter');
const PaymentInstrumentService = require('../services/commerce/payment-instrument/payment-instrument-service');
const PaymentMethodService = require('../services/commerce/payment-method/payment-method-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

class BasketModel {
  /**
   * ============================================================================================
   * GET
   * ============================================================================================
  */

  // [This request does not exists in API eCommerce]
  static async listBasket(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
      const retrievedCustomer = await BasketService
        .listBasket(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on listing baskets! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return BasketService.listBasket(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = BasketConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching baskets');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketById(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on listing basket by id! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.listBasketById(newCurrentLib, { token: newTokenOnBehalf.token }, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = BasketConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketShipments(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketById(currentLib, { token: loginObject.token }, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on listing basket shipments! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              return BasketService.listBasketById(newCurrentLib, { token: newCurrentLib.credentials.BMToken }, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ShipmentConverter.getDataMapToCore(obj, retrievedCustomer.data.shipments);
        const dataReplaced = Converters.replaceFields(data, map);
        if (retrievedCustomer.data.shipments) {
          _.each(retrievedCustomer.data.shipments, (shipment, index) => {
            if (shipment.shipping_address) {
              dataReplaced[index].shippingAddress = {
                _type: shipment.shipping_address._type,
                address: shipment.shipping_address.address1,
                city: shipment.shipping_address.city,
                countryCode: shipment.shipping_address.country_code,
                firstName: shipment.shipping_address.first_name,
                fullName: shipment.shipping_address.full_name,
                lastName: shipment.shipping_address.last_name,
                id: shipment.shipping_address.id,
                phone: shipment.shipping_address.phone,
                postalCode: shipment.shipping_address.postal_code,
                stateCode: shipment.shipping_address.state_code,
              };
            } else {
              dataReplaced[index].shippingAddress = {};
            }
          });
        }

        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket shipments');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketProductItems(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketById(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on listing basket by id! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.listBasketById(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductItemsConverter.getDataMapToCore(obj, retrievedCustomer.data.product_items);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket product items');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketBillingAddress(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketById(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on listing basket billing address! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.listBasketById(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = AddressConverter.getDataMapToCore(obj, [retrievedCustomer.data.billing_address]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket billing address');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketPaymentInstruments(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketById(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on listing basket payment instruments! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.listBasketById(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentInstrumentConverter.getDataMapToCore(obj, retrievedCustomer.data.payment_instruments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket payment instruments');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketShippingItems(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketById(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on listing basket shipping items! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.listBasketById(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ShippingItemsConverter.getDataMapToCore(obj, retrievedCustomer.data.shipping_items);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket shipping items');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async listBasketShippingMethods(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .listBasketShippingMethods(currentLib, { token: loginObject.token }, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on listing basket shipments methods! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.listBasketShippingMethods(newCurrentLib, newTokenOnBehalf, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ShippingMethodConverter.getDataMapToCore(obj, retrievedCustomer.data.applicable_shipping_methods);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching basket shipments methods');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async getBasketPaymentMethod(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const retrievedCustomer = await BasketService
        .getBasketPaymentMethod(currentLib,
          { token: loginObject.token },
          { ...query, ...params, ...loginObject }
        )
        .catch(async (error) => {
          console.error('Error on getting basket payment methods! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.getBasketPaymentMethod(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentMethodService.getDataMapToCore(obj, retrievedCustomer.data.applicable_payment_methods);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on getting basket payment methods');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  /**
   * ============================================================================================
   * POST
   * ============================================================================================
  */
  static async createBasket(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .createBasket(currentLib, { token: loginObject.token }, { ...query, ...loginObject, payload: body })
        .catch(async (error) => {
          console.error('Error on creating basket! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.createBasket(newCurrentLib, newTokenOnBehalf, { ...query, ...newTokenOnBehalf, payload: body });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = BasketConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on searching order by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async createBasketProductItem(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const { map, data } = ProductItemsConverter.getDataMapToProductItems(obj, body);
      const payload = Converters.replaceFields(data, map);

      const retrievedCustomer = await BasketService
        .createBasketProductItem(currentLib, { token: loginObject.token }, { ...query, ...params, ...loginObject, payload })
        .catch(async (error) => {
          console.error('Error on creating basket item! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.createBasketProductItem(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductItemsConverter.getDataMapToCore(obj, retrievedCustomer.data.product_items);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on creating basket item');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async createBasketPaymentInstrument(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const data = {
        "amount": body.amount,
        "payment_card" : {
          "number": body.cardNumber,
          "security_code": body.securityCode,
          "holder": body.holder,
          "card_type": body.cardType,
          "expiration_month": body.expirationMonth,
          "expiration_year": body.expirationYear,
        },
        "payment_method_id": body.paymentMethodId,
      };

      const payload = GenericHelper.removeAttributesNull([data]);

      const retrievedCustomer = await BasketService
        .createBasketPaymentInstrument(currentLib, loginObject, { ...query, ...params, ...loginObject, payload: payload[0] })
        .catch(async (error) => {
          console.error('Error on creating basket payment instrument! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.createBasketPaymentInstrument(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: payload[0] });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentInstrumentConverter.getDataMapToCore(obj, retrievedCustomer.data.payment_instruments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on creating basket payment instrument');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async createBasketShipment(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const { data, map } = AddressConverter
        .getDataMapToAddress(obj, [body.shippingAddress]);

      const dataReplaced = Converters.replaceFields(data, map);

      const payload = {
        shipping_method: { ...body.shippingMethod },
        shipping_address: dataReplaced[0]
      };

      const retrievedCustomer = await BasketService
        .createBasketShipment(currentLib, loginObject, { ...query, ...params, ...loginObject, payload })
        .catch(async (error) => {
          console.error('Error on creating basket shipment! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.createBasketShipment(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ShipmentConverter.getDataMapToCore(obj, retrievedCustomer.data.shipments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on creating basket shipment');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  /**
   * ============================================================================================
   * UPDATE
   * ============================================================================================
  */
  static async updateBasketBillingAddress(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const { map, data } = AddressConverter.getDataMapToAddress(obj, [body]);
      const payload = Converters.replaceFields(data, map);

      const retrievedCustomer = await BasketService
        .updateBasketBillingAddress(currentLib, loginObject, { ...query, ...params, ...loginObject, payload: payload[0] })
        .catch(async (error) => {
          console.error('Error on updating basket billing address! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.updateBasketBillingAddress(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: payload[0] });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = AddressConverter.getDataMapToCore(obj, [retrievedCustomer.data.billing_address]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on updating basket billing address');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async putBasketShipmentMethodToShipment(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const retrievedCustomer = await BasketService
        .putBasketShipmentMethodToShipment(currentLib, loginObject, { ...query, ...params, ...loginObject, payload: body })
        .catch(async (error) => {
          console.error('Error on updating basket shipping method to shipment! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.putBasketShipmentMethodToShipment(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: body });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const shippingMethod = retrievedCustomer.data.shipments.filter(shipment => shipment.shipping_method && shipment.shipping_method.id === body.id);
        if (shippingMethod && shippingMethod.length) {
          const { map, data } = ShippingMethodConverter.getDataMapToCore(obj, [shippingMethod[0].shipping_method]);
          const dataReplaced = Converters.replaceFields(data, map);
          return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
        }
        return MessageService.getSuccess([], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on updating basket shipping method to shipment');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async putBasketShipment(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const retrievedCustomer = await BasketService
        .putBasketShipment(currentLib,
          loginObject,
          { ...query, ...params, ...loginObject, payload: body }
        )
        .catch(async (error) => {
          console.error('Error on updating basket shipping method to shipment! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.putBasketShipment(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: body });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ShipmentConverter.getDataMapToCore(obj, retrievedCustomer.data.shipments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on updating basket shipping method to shipment');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async putBasketShipmentAddressToShipment(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const { data, map } = AddressConverter.getDataMapToAddress(obj, [body]);

      const dataReplaced = Converters.replaceFields(data, map);

      const retrievedCustomer = await BasketService
        .putBasketShipmentAddressToShipment(currentLib, loginObject, { ...query, ...params, ...loginObject, payload: dataReplaced[0] })
        .catch(async (error) => {
          console.error('Error on updating basket shipment method to shipment! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.putBasketShipmentAddressToShipment(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: dataReplaced[0] });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = AddressConverter.getDataMapToCore(obj, [retrievedCustomer.data.billing_address]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on updating basket shipment method to shipment');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async putBasketProductItem(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };

      const { map, data } = ProductItemsConverter.getDataMapToProductItems(obj, [body]);
      const payload = Converters.replaceFields(data, map);
      const p = GenericHelper.removeAttributesNull(payload);

      const retrievedCustomer = await BasketService
        .putBasketProductItem(currentLib, loginObject, { ...query, ...params, ...loginObject, payload: p[0] })
        .catch(async (error) => {
          console.error('Error on updating basket product item! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.putBasketProductItem(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: p[0] });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = ProductItemsConverter.getDataMapToCore(obj, retrievedCustomer.data.product_items);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced, retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on updating basket product item');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async putBasketCustomer(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, currentLib).catch(error => { return { token: 'invalid', status: error.status } });
      const payload = {
        customer_no: loginObject.customerNumber,
        email: loginObject.customerEmail,
      };

      const retrievedCustomer = await BasketService
        .putBasketCustomer(currentLib,
          { token: loginObject.token },
          { ...query, ...params, ...loginObject, payload }
        )
        .catch(async (error) => {
          console.error('Error on putting basket customer! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = await AuthDemandwareModel.auth({ ...obj, PerformRefreshToken }, newCurrentLib);
              return BasketService.putBasketCustomer(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = BasketConverter.getDataMapToCore(obj, [retrievedCustomer.data]);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on putting basket customer');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async putBasketPaymentInstrumentById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const a = {
        payment_method_id: body.paymentMethodId,
        payment_card: {
          number: body.paymentCard.cardNumber,
          card_type: body.paymentCard.cardType,
          expiration_month: body.paymentCard.expirationMonth,
          expiration_year: body.paymentCard.expirationYear,
          holder: body.paymentCard.holder,
          security_code: body.paymentCard.securityCode,
        }
      };
      const b = GenericHelper.removeAttributesNull([a]);

      const retrievedCustomer = await BasketService
        .putBasketPaymentInstrumentById(currentLib, loginObject, { ...query, ...params, ...loginObject, payload: b[0] })
        .catch(async (error) => {
          console.error('Error on putting basket customer! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.putBasketPaymentInstrumentById(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf, payload: b[0] });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        const { map, data } = PaymentInstrumentConverter.getDataMapToCore(obj, retrievedCustomer.data.payment_instruments);
        const dataReplaced = Converters.replaceFields(data, map);
        return MessageService.getSuccess(dataReplaced[0], retrievedCustomer.status);
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on putting basket customer');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  /**
   * ============================================================================================
   * REMOVE
   * ============================================================================================
  */
  static async removeBasket(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params, body } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .removeBasket(currentLib, loginObject, { ...query, ...params, ...loginObject })
        .catch(async (error) => {
          console.error('Error on removing! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.removeBasket(newCurrentLib, newTokenOnBehalf, { ...query, ...params, ...newTokenOnBehalf });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        return MessageService.getSuccess({}, 200, 'basket removed successfully');
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on removing');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async removeBasketShipment(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .removeBasketShipment(currentLib, loginObject, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on removing basket shipment by id! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.removeBasketShipment(newCurrentLib, newTokenOnBehalf, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        return MessageService.getSuccess({}, 200, 'basket removed successfully');
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on removing basket shipment by id');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async removeBasketItemById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .removeBasketItemById(currentLib, loginObject, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on removing basket product item! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.removeBasketItemById(newCurrentLib, newTokenOnBehalf, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        return MessageService.getSuccess({}, 200, 'basket product item removed successfully');
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on removing basket product item');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async removeBasketPaymentInstrumentById(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const retrievedCustomer = await BasketService
        .removeBasketPaymentInstrumentById(currentLib, loginObject, { ...query, ...params })
        .catch(async (error) => {
          console.error('Error on removing basket payment instrument! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.removeBasketPaymentInstrumentById(newCurrentLib, newTokenOnBehalf, { ...query, ...params });
            },
          });

          if (result) {
            return result;
          }
          return MessageService.getError(error.response.data.fault, error.response.status, error.message);
        });

      if (GenericHelper.isSuccess(retrievedCustomer.status)) {
        return MessageService.getSuccess({}, 200, 'basket payment instrument removed successfully');
      }
      return retrievedCustomer;
    } catch (error) {
      console.error('Error on removing basket payment instrument');
      throw (MessageService.getError(error.stack, error.status, error.message));
    }
  }

  static async refreshBasketId(obj, currentLib) {
    const { MessageService, Converters, PerformRefreshToken } = obj.privateLibraries;
    const { query, params } = obj.req;
    try {
      const loginObject = { token: currentLib.credentials.BMToken };
      const removeBasketResult = await BasketService.removeBasket(
        currentLib,
        loginObject,
        params,
      ).catch(async (error) => {
        console.error('Error on removing basket! Details: ');
        console.error(error);
        const result = await GenericHelper.performRefreshToken({
          PerformRefreshToken,
          currentLib,
          tokenValueBearer: obj.req.headers.authorization,
          status: error.response.status,
          callback: async (newCurrentLib) => {
            const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
            return BasketService.removeBasket(
              newCurrentLib,
              newTokenOnBehalf,
              params,
            );
          },
        });

        if (result) {
          return result;
        }
        return MessageService
          .getError(error.response.data.fault, error.response.status, error.message);
      });
      if (GenericHelper.isSuccess(removeBasketResult.status)) {
        const newBasketResult = await BasketService.createBasket(
          currentLib,
          loginObject,
          params,
        ).catch(async (error) => {
          console.error('Error on creating a new basket! Details: ');
          console.error(error);
          const result = await GenericHelper.performRefreshToken({
            PerformRefreshToken,
            currentLib,
            tokenValueBearer: obj.req.headers.authorization,
            status: error.response.status,
            callback: async (newCurrentLib) => {
              const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
              return BasketService.createBasket(
                newCurrentLib,
                newTokenOnBehalf,
                params,
              );
            },
          });
          if (result) {
            return result;
          }
          return MessageService
            .getError(error.response.data.fault, error.response.status, error.message);
        });
        if (GenericHelper.isSuccess(newBasketResult.status)) {
          if (query.customerId) {
            const loginOnBehalf = await AuthDemandwareModel.auth(
              {
                ...obj,
                PerformRefreshToken,
                params: { ...obj.params, customerId: query.customerId },
              },
              currentLib,
            ).catch(error => ({ token: 'invalid', status: error.status }));
            if (loginOnBehalf.customerNumber) {
              const payload = {
                customer_no: loginOnBehalf.customerNumber,
                email: loginOnBehalf.customerEmail,
              };
              const updatedBasketResult = await BasketService.putBasketCustomer(
                currentLib,
                loginObject,
                {
                  basketId: newBasketResult.data.basket_id,
                  payload,
                },
              ).catch(async (error) => {
                console.error('Error on updating basket! Details: ');
                console.error(error);
                const result = await GenericHelper.performRefreshToken({
                  PerformRefreshToken,
                  currentLib,
                  tokenValueBearer: obj.req.headers.authorization,
                  status: error.response.status,
                  callback: async (newCurrentLib) => {
                    const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
                    return BasketService.putBasketCustomer(
                      newCurrentLib,
                      newTokenOnBehalf,
                      {
                        basketId: newBasketResult.data.basket_id,
                        payload,
                      },
                    );
                  },
                });
                if (result) {
                  return result;
                }
                return MessageService
                  .getError(error.response.data.fault, error.response.status, error.message);
              });
              if (GenericHelper.isSuccess(updatedBasketResult.status)) {
                const { map, data } = BasketConverter
                  .getDataMapToCore(obj, [updatedBasketResult.data]);
                const dataReplaced = Converters.replaceFields(data, map);
                return MessageService.getSuccess(dataReplaced, updatedBasketResult.status);
              }
              return updatedBasketResult;
            }
            return MessageService.getError('Unable to perform Login on Behalf!', loginOnBehalf.status);
          }
          const { map, data } = BasketConverter.getDataMapToCore(obj, [newBasketResult.data]);
          const dataReplaced = Converters.replaceFields(data, map);
          return MessageService.getSuccess(dataReplaced, newBasketResult.status);
        }
        return newBasketResult;
      }
      return removeBasketResult;
    } catch (error) {
      return MessageService.getError(
        error.fault,
        500,
        error.stack,
      );
    }
  }
}

module.exports = BasketModel;

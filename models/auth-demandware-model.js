const _ = require('lodash');
const LoginTransactionHelper = require('../services/helpers/ocapi-helpers/ocapi-login-transactions-helper');
const CustomerService = require('../services/customer/customer-service');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');

const getParamsOrQueries = (req) => {
  if (!_.isEmpty(req.query)) {
    return req.query;
  }
  if (!_.isEmpty(req.params)) {
    return req.params;
  }
  return {};
};

/**
 * Login to BM Demandware
 * @returns {string} Bearer <token>
 */
exports.loginToBM = async (obj, currentLib) => {
  try {
    const configBMTokenJson = await LoginTransactionHelper.performDemandwareBmLogin(currentLib);
    const BMTokenJson = await LoginTransactionHelper
      .retrieveAndManageBMToken(obj, configBMTokenJson);
    const tokenBearer = await LoginTransactionHelper.getTokenBearer(BMTokenJson);
    return tokenBearer;
  } catch (error) {
    throw (error);
  }
};

exports.auth = async (obj, currentLib) => {
  try {
    const params = getParamsOrQueries(obj.req);

    // Login to BM Demandware
    const tokenBearer = currentLib.credentials.BMToken;

    // Get Customer ID
    const customerId = params.customerId; // await CustomerService.getCustomerList(currentLib, tokenBearer, params);

    // Login on Behalf
    const loginOnBehalfTokenObject = await LoginTransactionHelper.performDemandwareLoginOnBehalf({
      ...obj,
      currentLib,
      customerId,
      tokenBearer,
    }).catch(async (error) => {
      console.error('Error on performing Login on Behalf! Details: ');
      console.error(error);
      const result = await GenericHelper.performRefreshToken({
        ...obj,
        currentLib,
        status: error.status,
        callback: async (newCurrentLib) => {
          const newTokenOnBehalf = { token: newCurrentLib.credentials.BMToken };
          return LoginTransactionHelper.performDemandwareLoginOnBehalf({
            ...obj,
            currentLib: newCurrentLib,
            customerId,
            tokenBearer: newTokenOnBehalf.token,
          });
        },
      });
      if (result) {
        return result;
      }
      throw (error);
    });
    return loginOnBehalfTokenObject;
  } catch (error) {
    throw (error);
  }
};

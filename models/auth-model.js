const axios = require('axios');
const qs = require('querystring');

const clientelingConstants = require('../constants/clienteling-constants');

class AuthModel {
  static formatBMToken(tokenType, accessToken) {
    return `${tokenType} ${accessToken}`;
  }

  static async doLogin(infoOrg, userId, userPassword) {
    const demandwareBmLoginInfo = clientelingConstants.DEMANDWARE_BM_LOGIN;
    const GRANT_TYPE = demandwareBmLoginInfo.GRANT_TYPE;
    const METHOD = demandwareBmLoginInfo.METHOD;
    const CONTENT_TYPE = demandwareBmLoginInfo.CONTENT_TYPE;
    let credentials = infoOrg.credentials;
    if (userId && userPassword) {
      credentials = { ...infoOrg.credentials, userId, userPassword };
    }

    const objDemandware = AuthModel.getDemandwareBMLoginObj(credentials);
    const response = await AuthModel.requestBMToken(objDemandware.finalUrl, objDemandware.authorizationHeader, METHOD, GRANT_TYPE, CONTENT_TYPE).catch(error => error);

    return {
      ...infoOrg,
      credentials: {
        ...credentials,
        BMToken: AuthModel.formatBMToken(response.data.token_type, response.data.access_token),
      }
    };
  }

  /**
   * Example Output
   * {
   *  finalUrl - https://osfglobal19-alliance-prtnr-eu05-dw.demandware.net/dw/oauth2/access_token?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
   *  authorizationHeader- Y2xpZW50ZWxsaW5nOjlyRyxHUjRzMTphYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=
   *  method - POST
   *  grantType - urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken
   *  contentType - application/x-www-form-urlencoded
   * }
  */
  static requestBMToken(finalUrl, authorizationHeader, method, grantType, contentType) {
    return axios({
      method,
      url: finalUrl,
      data: qs.stringify({ grant_type: `${grantType}` }),
      headers: {
        Authorization: `Basic ${authorizationHeader}`,
        'Content-Type': contentType,
      },
    });
  }

  static getDemandwareBMLoginObj(credentials) {
    try {
      const userId = credentials.userId;
      const userPassword = credentials.userPassword;
      const clientId = credentials.clientId;
      const clientPassword = credentials.clientPassword;
      const baseUrl = credentials.baseUrl;
      const demandwareBmLoginInfo = clientelingConstants.DEMANDWARE_BM_LOGIN;
      const finalUrl = `${baseUrl}/${demandwareBmLoginInfo.URL}=${clientId}`;

      if (userId && userPassword && clientId && clientPassword && baseUrl) {
        const BASE64_STRING = `${userId}:${userPassword}:${clientPassword}`;
        const authorizationHeader = Buffer.from(BASE64_STRING).toString('base64');
        return {
          authorizationHeader,
          finalUrl,
          demandwareBmLoginInfo,
        };
      }
      throw new Error('required.configuration.not.found');
    } catch (error) {
      throw new Error('required.configuration.not.found');
    }
  };

  static getDataApiLogin(credentials) {
    try {
      const userId = credentials.clientId;
      const userPassword = credentials.clientPassword;
      const url = clientelingConstants.DEMANDWARE_BM_LOGIN.DATA_URL;
      const contentType = clientelingConstants.DEMANDWARE_BM_LOGIN.CONTENT_TYPE;
      const authorizationToken = Buffer.from(`${userId}:${userPassword}`).toString('base64');
      const contentTypeValue = clientelingConstants.DEMANDWARE_BM_LOGIN.DATA_GRANT_TYPE;

      return axios({
        method: 'POST',
        url,
        data: qs.stringify({ grant_type: `${contentTypeValue}` }),
        headers: {
          Authorization: `Basic ${authorizationToken}`,
          'Content-Type':  contentType,
        },
      });
    } catch (error) {
      throw(error);
    }
  }

}

module.exports = AuthModel;
